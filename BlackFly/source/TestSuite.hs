import qualified Data.ByteString.UTF8
import qualified Control.Monad.State as State
import Control.Monad.Trans.Except
import Numeric.Natural
import ExampleFuchsProgramms.AsHaskellConstant.ToggleFlag
import ExampleFuchsProgramms.AsHaskellConstant.UpdateChannel
import Data.Either.Combinators
import Advance.EncodingMagic
import Advance.ProgramEncodedStateCompiler
import Compiler.Backend.Reject
import Data.List.Extra
import Debug.Trace
import Data.List
import Trifle.Rename
import Trifle.TreeShow
import CleanParser.API
import Compiler.Frontend.InputReconstruction
import Compiler.Frontend.Symbolgramme
import Compiler.Frontend.UnicodeSubsets
import Compiler.Frontend.WhiteSpace
import Compiler.Middlepart.StateAsSyntax_to_FuchsProgram
import ExampleFuchsProgramms.AsHaskellConstant.AllExamples
import Store.HexEncoding
import Store.Hash
import Store.ContentAdressableStorage
import Store.Tangle
import Store.Fuchs
import Store.OpaqueState
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt
import Evaluate.Evaluation
import Evaluate.Force
import Evaluate.Gas
import Evaluate.Environment
import Advance.ProgramEncodedProgramCompiler
import Advance.Advance
import Advance.ActualUsingAdvance
import Advance.EncodingAndDecoding.Squashing
import Advance.EncodingAndDecoding.VariableName
import Advance.AdvanceWithAdvanceIntrinsic
import RuntimeUsageBindings.ToggleExample
import RuntimeUsageBindings.ConnectivityDriverBindings




import Compiler.CompileChain
import Compiler.Frontend.VariableNameAndConstructorName
import Compiler.Frontend.LambdaAbstraction
import Compiler.Frontend.FunctionName
import Compiler.Frontend.MonospaceTree
import Compiler.Frontend.FunctionApplication
import Compiler.Frontend.Definition
import Compiler.Frontend.Parenthesis
import Compiler.Frontend.SwitchCase
import Compiler.Frontend.AppliedDefinitionBlock
import Compiler.Frontend.Expression
import Compiler.Frontend.Comment
import Compiler.Frontend.StateRecord
import Compiler.Frontend.StateAsSyntax
import Evaluate.IntrinsicSignature
import Evaluate.EvaluationTest
import Advance.AdvanceTest
import Store.HaseTest
import ExampleTestcases.UntypedUpdateChannel
import ExampleTestcases.UpdateChannel
import Trifle.Test




main :: IO ()
main = do
  print "it runs therefore it compiles"
  runTestCase test_eatVariableName
  runTestCase test_eatConstructorName
  runTestCase test_eatLambdaAbstraction
  runTestCase test_eatFunctionName
  runTestCase test_cases_tree
  runTestCase test_function_application
  runTestCase test_getting_back_all_names
  runTestCase test_definitions
  runTestCase test_population_census
  runTestCase test_parenthesis
  runTestCase test_mapping
  runTestCase test_AppliedDefinitionBlock
  runTestCase test_nested_expressions
  runTestCase test_comment
  runTestCase test_emptyLine
  runTestCase test_StateRecord
  runTestCase test_wholestate
  runTestCase test_all_examples_compile
  runTestCase test_inout_hase
  runTestCase test_evaluation_little_examples
  runTestCase test_toggle_switch
  runTestCase test_untypedupdatechannel
  runTestCase test_updatechannel
  runTestCase test_intrinsic_names
  return ()

