module Compiler.CompileChain where

import Data.Either.Combinators

import Trifle.Test

import CleanParser.API

import Compiler.Frontend.StateAsSyntax
import Compiler.Frontend.UsingTheParser
import Compiler.Middlepart.StateAsSyntax_to_FuchsProgram
import Compiler.Middlepart.Reject_StateAsSyntax
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Compiler.Backend.Reject


import ExampleFuchsProgramms.AsHaskellConstant.AllExamples


data Error_CompileChain
  = Error_InFrontend (Error_State NumberedToken)
  | Error_Rejecting_bad_StateAsSyntax Error_Rejection
  | Error_InMiddlepart (Error_translate_Expression NumberedToken)
  | Error_Rejecting_bad_FuchsProgramState Error_Rejection_FuchsProgramState
  deriving Show

compile_fuchs_file
  :: String
  -> Either Error_CompileChain FuchsProgramState
compile_fuchs_file supposed_to_be_fuchs_content = do

  stateAsSyntax
    <- mapLeft Error_InFrontend
    $ parse_to_StateAsSyntax
    $ supposed_to_be_fuchs_content

  () <- mapLeft Error_Rejecting_bad_StateAsSyntax
     $ reject_bad_StateAsSyntax
     $ stateAsSyntax

  fuchsProgramState
    <- mapLeft Error_InMiddlepart
    $ translate_StateAsSyntax_to_FuchsProgramState
    $ stateAsSyntax

  () <- mapLeft Error_Rejecting_bad_FuchsProgramState
     $ reject_bad_FuchsProgramState
     $ fuchsProgramState

  return fuchsProgramState


test_all_examples_compile :: Test
test_all_examples_compile = (,) "test_all_examples_compile"
  $ and
  $ map errorOnLeft
  $ zip allExamplesNames
  $ map compile_fuchs_file
  $ allExamples
  where
    errorOnLeft (_, (Right _)) = True
    errorOnLeft (name, (Left a)) = error $ "error while compiling "++show name++": " ++ show a

