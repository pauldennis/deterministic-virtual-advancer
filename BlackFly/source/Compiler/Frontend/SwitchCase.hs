module Compiler.Frontend.SwitchCase where

import Data.Either

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.KeywordParser
import Compiler.Frontend.WhiteSpace
import Compiler.Frontend.FunctionApplication
import Compiler.Frontend.Symbolgramme
import Compiler.Frontend.MonospaceTree

data Error_Mapping token bodyError
  = Error_Mapping (Error_FoldedExpressions token)
  | Error_MapsToArrow (Error_DefinitionRightMapsTo token)
  | Error_InBody bodyError
  deriving Show

data MappingFromTo token image
  = MappingFromTo (FoldedExpressions token) image
  deriving Show

eatSwitchCaseMapping
  :: CharacterRepresentable token
  => Parser token imageError image
  -> Parser token (Error_Mapping token imageError) (MappingFromTo token image)
eatSwitchCaseMapping bodyParser = do
  preimage
    <- fmapOnParserError Error_Mapping
    $ foldedFunctionApplication

  _ <- absurdError eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  _ <- fmapOnParserError Error_MapsToArrow eatDefinitionRightMapsTo

  _ <- absurdError eatSpaces

  image <- fmapOnParserError Error_InBody $ bodyParser

  return $ MappingFromTo preimage image


test_mapping :: Test
test_mapping = (,) "mapping" $ and [ True
  , isLeft $ runParser theParser ""
  , isRight $ runParser theParser "A↦#"
  , isRight $ runParser theParser "A|->#"
  , isRight $ runParser theParser "A |-> #"
  , isLeft $ runParser theParser "A -> #"
  , isLeft $ runParser theParser "A | -> #"
  , isRight $ runParser theParser "A a b  c A |-> #"
  , isRight $ runParser theParser "A a b\n  c A |-> #"
  , isRight $ runParser theParser "A a b\n  c\n  A |-> #"
  , isLeft $ runParser theParser "A a b\n  c\n A |-> #"
  ]
  where
    theParser = eatSwitchCaseMapping (eatExpectedCharacter '#')



---



data Error_SwitchCase token body bodyError
  = Error_SwitchCase_Switch_Keyword (Error_Keyword_Switch token)
  | Error_SwitchSubject bodyError
  | Error_SwitchCase_Cases_Keyword (Error_Keyword_Cases token)
  | Error_Function (Error_RootNode (Error_Mapping token bodyError))
  | Error_SwitchCase_OnlyOneIndentingLevel
      (MappingsTreeWithGrandChildren token body bodyError)
  deriving Show

data MappingsTreeWithGrandChildren token body bodyError
  = MappingsTreeWithGrandChildren
      (MonospaceTree
        token
        (SpacesBranch token)
        ErrorSpacesBranch
        (MappingFromTo token body)
        (Error_Mapping token bodyError)
      )
  deriving Show

data DetailedSwitchCase token body
  = DetailedSwitchCase body [MappingFromTo token body]
  deriving Show

eatSwitchCase
  :: CharacterRepresentable token
  => Parser token bodyError body
  -> Parser token (Error_SwitchCase token body bodyError) (DetailedSwitchCase token body)
eatSwitchCase bodiesParser = do
  _ <- fmapOnParserError Error_SwitchCase_Switch_Keyword $ eat_keyword_switch

  _ <- absurdError eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  objectOfInvestigation
    <- fmapOnParserError Error_SwitchSubject bodiesParser

  _ <- absurdError eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  _ <- fmapOnParserError Error_SwitchCase_Cases_Keyword $ eat_keyword_cases

  _ <- absurdError eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  rawFunction
    <- fmapOnParserError Error_Function
    $ eatRootNodeWithSubChildren
    $ eatSwitchCaseMapping
    $ bodiesParser

  let isAllSameIndentingLevel = not $ areThereGrandChildren rawFunction

  parserAssert
    isAllSameIndentingLevel
    (Error_SwitchCase_OnlyOneIndentingLevel
      $ MappingsTreeWithGrandChildren
      $ rawFunction
    )

  let mappings = extractPayloads rawFunction

  return $ DetailedSwitchCase objectOfInvestigation mappings

test_eatSwitchCase :: Test
test_eatSwitchCase = (,) "eatSwitchCase" $ and [ True
  , isLeft $ runParser theParser_switchcase ""
  , isLeft $ runParser theParser_switchcase "switch"
  , isLeft $ runParser theParser_switchcase "switch #"
  , isLeft $ runParser theParser_switchcase "switch # cases"
  , isLeft $ runParser theParser_switchcase "switch # cases A"
  , isLeft $ runParser theParser_switchcase "switch # cases A |->"
  , isRight $ runParser theParser_switchcase "switch # cases A |-> #"
  , isRight $ runParser (completeParse theParser_switchcase) $ ""
      ++ "switch # cases\n"
      ++ " A     |-> #\n"
      ++ " B c d |->    #\n"
      ++ " q     |-> #"
  , isRight $ runParser (completeParse theParser_switchcase) $ ""
      ++ "switch # cases A\n"
      ++ "  |-> #\n"
  , isLeft $ runParser theParser_switchcase $ ""
      ++ "switch # cases\n"
      ++ " A     |-> #\n"
      ++ "  B c d |->    #\n"
      ++ "   q     |-> #"
  , isRight $ runParser (completeParse theParser_switchcase) $ ""
      ++ "switch\n"
      ++ " #"
      ++ " cases\n"
      ++ " A     |-> #\n"
      ++ " B c d |->    #\n"
      ++ " q     |-> #"
  , isRight $ runParser (completeParse theParser_switchcase) $ ""
      ++ "switch # cases A     |-> #\n"
      ++ "               B c d |->    #\n"
      ++ "               q     |-> #"
  , isRight $ runParser (completeParse theParser_switchcase) $ ""
      ++ "switch # cases\n"
      ++ "  §method_toggle |-> #"
  ]
  where
    theParser_switchcase = eatSwitchCase (eatExpectedCharacter '#')
