module Compiler.Frontend.Parenthesis where

import Data.Either

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.UnicodeSubsets

data Error_Parenthesis token bodyError
  = Error_OpeneningParenthesis (EatExpectedCharacterError token)
  | Error_In_Body bodyError
  | Error_ClosingParenthesis (EatExpectedCharacterError token)
  deriving Show

data BodyInParenthesis body
  = BodyInParenthesis body
  deriving Show

-- TODO whitespace
eatWithParenthesis
  :: CharacterRepresentable token
  => Parser token bodyError body
  -> Parser
       token
       (Error_Parenthesis token bodyError)
       (BodyInParenthesis body)
eatWithParenthesis bodyParser = do
  _ <- fmapOnParserError Error_OpeneningParenthesis
    $ eatString [opening_parenthesis]

  body <- fmapOnParserError Error_In_Body bodyParser

  _ <- fmapOnParserError Error_ClosingParenthesis
    $ eatString [closing_parenthesis]

  return $ BodyInParenthesis body

test_parenthesis :: Test
test_parenthesis = (,) "'(' and ')'" $ and [ True
  , isLeft $ runParser theParser ""
  , isLeft $ runParser theParser "("
  , isLeft $ runParser theParser "()"
  , isLeft $ runParser theParser ")"
  , isLeft $ runParser theParser "( )"
  , isRight $ runParser theParser "(#)"
  , isRight $ runParser (completeParse theParser) "(#)"
  ]
  where
    theParser = eatWithParenthesis (eatExpectedCharacter '#')

