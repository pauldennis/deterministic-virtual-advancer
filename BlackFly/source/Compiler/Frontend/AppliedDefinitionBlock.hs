module Compiler.Frontend.AppliedDefinitionBlock where

import Data.Either

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.WhiteSpace
import Compiler.Frontend.KeywordParser
import Compiler.Frontend.Definition
import Compiler.Frontend.MonospaceTree


newtype Body term
  = Body term
  deriving Show

data AbstractDefinitionBlock token term
  = AbstractDefinitionBlock [Definition token term] (Body term)
  deriving Show

newtype RawDefinitionTree token term termError
  = RawDefinitionTree
      (MonospaceTree
        token
        (SpacesBranch token)
        ErrorSpacesBranch
        (Definition token term)
        (Error_Definition token termError)
      )
  deriving Show

data LetExpression token term termError
  = LetExpression
      (AbstractDefinitionBlock token term)
      (RawDefinitionTree token term termError)
  deriving Show

newtype DefinitionTreeWithGrandChildren token term termError
  = DefinitionTreeWithGrandChildren
      (MonospaceTree
        token
        (SpacesBranch token)
        ErrorSpacesBranch
        (Definition token term)
        (Error_Definition token termError)
      )
  deriving Show

data Error_LetExpression token term termError
  = Error_WhileParsing_let_Keyword
      (Error_Keyword_Let token)
  | Error_DefinitionBlockThatIsATree
      (Error_RootNode (Error_Definition token termError))
  | Error_OnlyOneIndentingLevel
      (DefinitionTreeWithGrandChildren token term termError)
  | Error_WhileParsing_in_Keyword (Error_Keyword_In token)
  | Error_WhileParsingReturnExpression termError
  deriving Show

eatLetExpression
  :: CharacterRepresentable token
  => Parser token termError term
  -> Parser
       token
       (Error_LetExpression token term termError)
       (LetExpression token term termError)
eatLetExpression expressionParser = do
  _ <- fmapOnParserError Error_WhileParsing_let_Keyword $ eat_keyword_let

  _ <- absurdError $ eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  rawDefinitions
    <- fmapOnParserError Error_DefinitionBlockThatIsATree
    $ eatRootNodeWithSubChildren
    $ eatDefinition expressionParser

  let isAllSameIndentingLevel = not $ areThereGrandChildren rawDefinitions

  parserAssert
    isAllSameIndentingLevel
    (Error_OnlyOneIndentingLevel
      $ DefinitionTreeWithGrandChildren
      $ rawDefinitions
    )

  _ <- absurdError $ eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  -- TODO
  _ <- fmapOnParserError Error_WhileParsing_in_Keyword
    $ eat_keyword_in

  -- TODO
  _ <- absurdError $ eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  returnExpression <- fmapOnParserError Error_WhileParsingReturnExpression expressionParser

  let definitions = extractPayloads rawDefinitions

  return $ LetExpression
    (AbstractDefinitionBlock definitions (Body returnExpression))
    (RawDefinitionTree rawDefinitions)


test_AppliedDefinitionBlock :: Test
test_AppliedDefinitionBlock = (,) "AppliedDefinitionBlock" $ and [ True
  , isLeft $ runParser theParser $ ""
  , isLeft $ runParser theParser $ "let"
  , isLeft $ runParser (completeParse theParser) $ "let q"
  , isLeft $ runParser (completeParse theParser) $ "let q :="
  , isLeft $ runParser (completeParse theParser) $ "let q := #"
  , isLeft $ runParser (completeParse theParser) $ "let q := # in"
  , isRight $ runParser (completeParse theParser) $ "let q := # in #"
  , isRight $ runParser (completeParse theParser) $ multiline_let_good_1
  , isRight $ runParser (completeParse theParser) $ multiline_let_good_2
  , isLeft $ runParser (completeParse theParser) $ multiline_let_bad_1
  , isLeft $ runParser (completeParse theParser) $ multiline_let_bad_2
  ]
  where
    theParser = eatLetExpression (eatExpectedCharacter '#')

multiline_let_bad_1 :: String
multiline_let_bad_1 = ""
  ++ "let q := #\n"
  ++ "    r := #\n"
  ++ "     s := #\n" -- wrong indentation
  ++ " in #\n"

multiline_let_bad_2 :: String
multiline_let_bad_2 = ""
  ++ "let q := #\n"
  ++ "    r := #\n"
  ++ "   s := #\n" -- wrong indentation
  ++ "     in #\n"

multiline_let_good_1 :: String
multiline_let_good_1 = ""
  ++ "let q := #\n"
  ++ "    q := #\n"
  ++ "    q := #\n"
  ++ "    in #"

multiline_let_good_2 :: String
multiline_let_good_2 = ""
  ++ "let \n"
  ++ "  q := #\n"
  ++ "  q := #\n"
  ++ "  q := #\n"
  ++ "    in\n"
  ++ "  #"

{-
TODO the current implementation allows the following

ww := let q1 := q1
 q2 := q2
 q3 := q3
 in q3
-}
