module Compiler.Frontend.StateRecord where

-- TODO rename to StateBinding or StateEntry

import Data.Either

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.Definition
import Compiler.Frontend.Expression
import Compiler.Frontend.Comment

data Error_StateRecord token
  = Error_IN_Definition
      (Error_Definition token (Error_NonValidExpression token))
  | Error_IN_EmptyLine
  deriving Show


data StateRecord token
  = StateRecord_Definition (Definition token (Expression token))
  | StateRecord_EmptyLine
      (EmptyLine {-token-})
  deriving Show


eatStateRecord_Definition
  :: CharacterRepresentable token
  => Parser token (Error_StateRecord token) (StateRecord token)
eatStateRecord_Definition
  = fmap StateRecord_Definition
  $ fmapOnParserError Error_IN_Definition
  $ eatDefinition eatExpression


eatStateRecord_EmptyLine
  :: CharacterRepresentable token
  => Parser token (Error_StateRecord token) (StateRecord token)
eatStateRecord_EmptyLine
  = fmap StateRecord_EmptyLine
  $ absurdError
  $ eatEmptyLine



data Error_NonValidStateRecord token
  = Error_NonValidStateRecord [Error_StateRecord token]
  deriving Show


eatStateRecord
  :: CharacterRepresentable token
  => Parser token (Error_NonValidStateRecord token) (StateRecord token)
eatStateRecord
  = fmapOnParserError Error_NonValidStateRecord
  $ firstWorkingParser
  [ eatStateRecord_Definition
  , eatStateRecord_EmptyLine --last one, because never fails
  ]



test_StateRecord :: Test
test_StateRecord = (,) "state record" $ and [ True
  , isRight $ runParser eatStateRecord $ ""
  , isLeft $ runParser (completeParse eatStateRecord) $ "q"

  , isRight $ runParser (completeParse eatStateRecord) $ "q := q"
  , isLeft $ runParser (completeParse eatStateRecord) $ "\n "

  , isRight $ runParser eatStateRecord $ ""
      ++ "kmcsl := skjd sd sd sd ds ds ds ds ds q\n"
      ++ "                   q\n"
      ++ "kmcsl := skjd sd sd sd ds ds ds ds ds q\n"
  , isRight $ runParser eatStateRecord $ ""
      ++ "kmcsl := skjd"
  ]

