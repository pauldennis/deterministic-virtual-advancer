module Compiler.Frontend.StateAsSyntax where

import Data.Void
import Data.Either

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.StateRecord


data StateAsSyntax token
  = StateAsSyntax [Record (StateRecord token) token]
  deriving Show

data Error_State token
  = Error_State
      (Error_CompleteParseError
        Void
        token
        (GreedilyRepeatParserResult
          (Record (StateRecord token) token)
          (Error_LineParser
            (Error_NonValidStateRecord token)
            (EatExpectedCharacterError token)
          )
        )
      )
  deriving Show

eatState
  = fmap StateAsSyntax
  $ fmap (\(GreedilyRepeatOutput listOfRecords)->listOfRecords)
  $ fmap (\(GreedilyRepeatParserResult x _endOfFileError)->x)
  $ fmap (\(CompleteParseResult x@_weExpextEverythingToBeEated)->x)
  $ fmapOnParserError Error_State
  $ completeParse
  $ greedilyRepeatParser
  $ eatLine
  $ eatStateRecord
eatState
  :: CharacterRepresentable token =>
     Parser
       token
       (Error_State token)
       (StateAsSyntax token)

test_wholestate :: Test
test_wholestate = (,) "whole state" $ and [ True
  , isRight $ runParser eatState $ ""
  , isRight $ runParser eatState $ "\n"
  , isRight $ runParser eatState $ "\n\n"
  , isRight $ runParser eatState $ "\n\n\n"

  , isLeft $ runParser eatState $ "~"
  , isLeft $ runParser eatState $ "q := 1"
  , isLeft $ runParser eatState $ "q := q"
  , isRight $ runParser eatState $ "q := q\n"

  , isRight $ runParser eatState $ ""
      ++ "q  := q\n"
      ++ "\n"
      ++ "\n"
      ++ "\n"
      ++ "q  := q\n"
      ++ "q  := q\n"
      ++ "q  := q\n"
      ++ "        x\n"
      ++ "\n"
      ++ "\n"
      ++ "\n"
      ++ "q  := q\n"
      ++ "\n"
  , isRight $ runParser eatState $ ""
      ++ "\n"
      ++ "q  := q\n"
      ++ "        x\n"
      ++ "\n"
  , isLeft $ runParser eatState $ ""
      ++ "\n"
      ++ "q\n"
      ++ ":= q\n"
      ++ "\n"
  ]
