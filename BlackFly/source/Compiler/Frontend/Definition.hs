module Compiler.Frontend.Definition where

import Data.Either

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.Symbolgramme
import Compiler.Frontend.WhiteSpace
import Compiler.Frontend.VariableNameAndConstructorName
import Compiler.Frontend.MonospaceTree


newtype KnownTerm expression
  = KnownTerm expression
  deriving Show

newtype NewTerm token
  = NewTerm (DetailedVariableName token)
  deriving Show

data Definition token knownTerm
  = Definition (NewTerm token) (KnownTerm knownTerm)
  deriving Show

data Error_Definition token errorCaseForKnownTerm
  = Error_NewTerm (Error_VariableName token)
  | Error_TermSeperator (Error_DefinitionColonEqual token)
  | Error_KnownTerm errorCaseForKnownTerm
  deriving Show


eatDefinition
  :: CharacterRepresentable token
  => Parser token falsity knownTerm
  -> Parser token (Error_Definition token falsity) (Definition token knownTerm)
eatDefinition expressionParser = do
  functionName <- fmapOnParserError Error_NewTerm eatVariableName

  _ <- absurdError eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  _ <- fmapOnParserError Error_TermSeperator $ eatDefinitionColonEqual

  _ <- absurdError eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  term <- fmapOnParserError Error_KnownTerm expressionParser
  return $ Definition (NewTerm functionName) (KnownTerm term)


test_definitions :: Test
test_definitions = (,) "definition tests" $ and [ True
  , isLeft $ runParser theParser $ ""
  , isLeft $ runParser theParser $ "A := #"
  , isLeft $ runParser theParser $ "Que := #"
  , isRight $ runParser theParser $ "a := #"
  , isRight $ runParser theParser $ "qww:=#"
  , isRight $ runParser theParser $ "qww := #"
  , isRight $ runParser theParser $ "qww≔#"
  , isRight $ runParser theParser $ "qww ≔ #"
  , isRight $ runParser theParser $ "qww      ≔    #"
  , isRight $ runParser theParser $ ""
      ++ "qww\n"
      ++ "  :=\n"
      ++ "      #\n"
  , isRight $ runParser theParser $ ""
      ++ "qww\n"
      ++ "  :=\n"
      ++ "  #\n"
  , isLeft $ runParser theParser $ ""
      ++ "qww\n"
      ++ "  :=\n"
      ++ "#\n"
  ]
  where
    theParser = eatDefinition (eatExpectedCharacter '#')
