module Compiler.Frontend.FunctionName where

-- TODO rename FunctionName to VariableName

import Safe
import Data.Either

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.VariableNameAndConstructorName

data FunctionName token
  = Constructor (DetailedConstructorName token (Error_EatTokenWithAttribute token Char))
  | Mapping (DetailedVariableName token)
  deriving Show

data OrWeWouldHaveNeededAnDifferentTokenInTheBeggining token
  = OrWeWouldHaveNeededAnDifferentTokenInTheBeggining
      (Error_ConstructorName token [Char])
  deriving Show

data Error_EatingFunctionNameDidNotWork token
  = Error_Function_UnexpectedEndOfFile_ButWanted_x [Char]
  | Error_FunctionNameCannotStartWith_x_because_I_Want_x_ token [Char]
  | Error_CannotStopHereBecause_x_WouldBeTheKeyword_x [token] String (VariableNameMustStopEatingHere token)
      (OrWeWouldHaveNeededAnDifferentTokenInTheBeggining token)
  deriving Show

eatFunctionName
  :: (Show token, CharacterRepresentable token)
  => Parser
      token
      (Error_EatingFunctionNameDidNotWork token)
      (FunctionName token)
eatFunctionName
  = fmapOnParserError translateError
  $ eitherAorB
      (fmap Constructor eatConstructorName)
      (fmap Mapping eatVariableName)

  where
    -- TODO missuse of
    translateError
      ( Error_ConstructorName_UnexpectedEOF_WantOneOf_x_ x1
      , Error_VariableName_UnexpectedEOF_WantOneOf_x_ x2
      )
      = Error_Function_UnexpectedEndOfFile_ButWanted_x $ x2 ++ x1
    translateError
      ( Error_ConstructorNameCannotBeginWith_x_butItCouldWith_x x1 a1
      , Error_VariableNameCannotBeginWith_x_butItCouldWith_x x2 a2
      )
      = assertNote "should not happen" (getCharacter x1 == getCharacter x2)
      $ Error_FunctionNameCannotStartWith_x_because_I_Want_x_ x1 (a2++a1)

    translateError (errorA@(Error_ConstructorNameCannotBeginWith_x_butItCouldWith_x _token _attributes), Error_KeywordIsNotAnVariableName keyword tokens info)
      = Error_CannotStopHereBecause_x_WouldBeTheKeyword_x tokens keyword info (OrWeWouldHaveNeededAnDifferentTokenInTheBeggining errorA)

    translateError _ = error $ "unconsidered case for translateError in eatFunctionName"


test_eatFunctionName :: Test
test_eatFunctionName = (,) "function name test" $ and [ True
  , isLeft $ runParser eatFunctionName ""
  , isLeft $ runParser eatFunctionName " www"
  , isRight $ runParser eatFunctionName "_"
  , isLeft $ runParser (completeParse eatFunctionName) "_ "
  , isRight $ runParser eatFunctionName "q"
  , isRight $ runParser eatFunctionName "Q"
  , isRight $ runParser eatFunctionName "_abc"
  , isRight $ runParser eatFunctionName "ABC"
  , isLeft $ runParser eatFunctionName "123"
  , isLeft $ runParser eatFunctionName "let" -- keyword
  , isRight $ runParser eatFunctionName "use abbreviation" -- still keyword but one variable can be parsed
  ]
