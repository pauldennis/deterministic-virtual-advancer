module Compiler.Frontend.UnicodeSubsets where

--- named chars
space
  , newline
  , prime
  , underscore
  , section_sign
  , opening_parenthesis
  , closing_parenthesis
  , inverted_question_mark
  :: Char
space = ' '
newline = '\n'
prime = '\''
underscore = '_'
section_sign = '§'
opening_parenthesis = '('
closing_parenthesis = ')'
inverted_question_mark = '¿'

--- sets

abc, _ABC, _0123 :: String
abc = "abcdefghijklmnopqrstuvwxyz"
_ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
_0123 = "0123456789"




--- usage in context


buildInMeaning_prefix_for_Constructor :: Char
buildInMeaning_prefix_for_Constructor = section_sign

magic_prefix_for_built_function :: Char
magic_prefix_for_built_function = inverted_question_mark

possibleFirstLettersForVariableName :: String
possibleFirstLettersForVariableName
  = [underscore]
  ++ [magic_prefix_for_built_function]
  ++ abc
possibleProceedingLettersForVariableName :: String
possibleProceedingLettersForVariableName
  = [prime]
  ++ [underscore]
  ++ abc
  ++ _ABC
  ++ _0123

possibleFirstLetttersForConstructorName :: String
possibleFirstLetttersForConstructorName
  = [buildInMeaning_prefix_for_Constructor]
  ++ _ABC

possibleProceedingLettersForConstructorName :: String
possibleProceedingLettersForConstructorName
  = [underscore]
  ++ abc
  ++ _ABC
  ++ _0123
