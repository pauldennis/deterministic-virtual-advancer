module Compiler.Frontend.Keywords where

list_of_all_keywords_that_should_not_be_variable_names :: [String]
list_of_all_keywords_that_should_not_be_variable_names = tail [ undefined
  , keyword_let_short
  , keyword_let_long

  , keyword_in_short
  , keyword_in_long

  , keyword_switch_short
  , keyword_switch_long

  , keyword_cases_short
  , keyword_cases_long
  ]

keyword_let_short :: String
keyword_let_short = "let"
keyword_let_long :: String
keyword_let_long = "use abbreviations"

keyword_in_short :: String
keyword_in_short = "in"
keyword_in_long :: String
keyword_in_long = "in the expression"

keyword_switch_short :: String
keyword_switch_short = "switch"
keyword_switch_long :: String
keyword_switch_long = "switch the expression"

keyword_cases_short :: String
keyword_cases_short = "cases"
keyword_cases_long :: String
keyword_cases_long = "with the cases"
