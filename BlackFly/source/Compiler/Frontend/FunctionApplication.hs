module Compiler.Frontend.FunctionApplication where

import Data.Either
import Data.Void

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.FunctionName
import Compiler.Frontend.WhiteSpace
import Compiler.Frontend.MonospaceTree
import Compiler.Frontend.VariableNameAndConstructorName

data Error_JuxtaposedExpressions token
  = Error_JuxtaposedExpressions
      (Error_DelimitedGreedilyRepeatParserAtLeastOnce
        (Error_EatingFunctionNameDidNotWork token)
      )
  deriving Show

newtype ListOfFunctions token
  = ListOfFunctions [FunctionName token]
  deriving Show

newtype RawSyntaxJuxtaposedExpressions token
  = RawSyntaxJuxtaposedExpressions
      (DelimitedGreedilyRepeatParserAtLeastOnceResult
        (Spaces token)
        (FunctionName token)
        Void
        (Error_EatingFunctionNameDidNotWork token)
      )
  deriving Show

data JuxtaposedExpressions token
  = JuxtaposedExpressions
      (ListOfFunctions token)
      (RawSyntaxJuxtaposedExpressions token)
  deriving Show

juxtapose_expressions
  :: (Show token, CharacterRepresentable token)
  => Parser
      token
      (Error_JuxtaposedExpressions token)
      (JuxtaposedExpressions token)
juxtapose_expressions
  = fmapOnParserError Error_JuxtaposedExpressions
  $ fmap refineOutput
  $ delimitedGreedilyRepeatParserAtLeastOnce eatFunctionName eatSpaces
  where
    refineOutput rawResult = JuxtaposedExpressions (ListOfFunctions $ extractAbstractStructure rawResult) (RawSyntaxJuxtaposedExpressions rawResult)

    extractAbstractStructure (DelimitedGreedilyRepeatParserAtLeastOnceResult
      (TheAtLeastOnceWittness firstExpression)
      (GreedilyRepeatOutput restFunctions)
      _) = firstExpression : (map (\(DelimiteredOutput _comma (OutputBetweenDelimiter function))->function) restFunctions)

data Error_FoldedExpressions token
  = Error_FoldedExpressions
      (Error_RootNode (Error_JuxtaposedExpressions token) )
  deriving Show

newtype RawSyntaxFoldedExpressions token
  = RawSyntaxFoldedExpressions
      (MonospaceTree
        token
        (SpacesBranch token)
        ErrorSpacesBranch
        (JuxtaposedExpressions token)
        (Error_JuxtaposedExpressions token)
      )
  deriving Show

data FoldedExpressions token
  = FoldedExpressions (ListOfFunctions token) (RawSyntaxFoldedExpressions token)
  deriving Show

getAbstractSyntaxTree
  :: FoldedExpressions token
  -> ListOfFunctions token
getAbstractSyntaxTree (FoldedExpressions functions _)
  = functions

foldedFunctionApplication
  :: (Show token, CharacterRepresentable token)
  => Parser token (Error_FoldedExpressions token) (FoldedExpressions token)
foldedFunctionApplication
  = fmap refineOutput
  $ fmapOnParserError Error_FoldedExpressions
  $ eatRootNodeWithSubChildren juxtapose_expressions
  where
    refineOutput rawResult = FoldedExpressions (extractAbstractStructure rawResult) (RawSyntaxFoldedExpressions rawResult)

    extractAbstractStructure
      :: MonospaceTree
            token2
            (SpacesBranch token2)
            ErrorSpacesBranch
            (JuxtaposedExpressions token2)
            (Error_JuxtaposedExpressions token2)
      -> ListOfFunctions token2
    extractAbstractStructure = ListOfFunctions . (foldMonospaceTree squash)

    squash
      = (\(JuxtaposedExpressions (ListOfFunctions a) _) bs
      -> a ++ (concat bs))


test_function_application :: Test
test_function_application = (,) "primitiv function application" $ and [ True
  , isLeft $ runParser foldedFunctionApplication ""
  , isRight $ runParser (completeParse foldedFunctionApplication) "A"
  , isRight $ runParser (completeParse foldedFunctionApplication) "a"
  , isRight $ runParser (completeParse foldedFunctionApplication) "a b"
  , isRight $ runParser (completeParse foldedFunctionApplication) "a b c"
  , isRight $ runParser (completeParse foldedFunctionApplication) "a b c d"
  , isRight $ runParser (completeParse foldedFunctionApplication) "a\n  b c\n  d e"
  , isRight $ runParser (completeParse foldedFunctionApplication) "a\n  b c\n  d e"
  , isLeft $ runParser (completeParse foldedFunctionApplication) "a #"
  , isLeft $ runParser (completeParse foldedFunctionApplication) "a \n  b #"
--   , (== "abcdef") $ (\(FoldedExpressions x _)->x) $ runParser foldedFunctionApplication "a"
  ]

test_getting_back_all_names :: Test
test_getting_back_all_names = (,) "getting all names back"
  $ ("abcdefghi" ==)
  $ concat
  $ map (\(DetailedVariableName x _ _)->x)
  $ map (\(Mapping x)->x)
  $ (\(ListOfFunctions x)->x)
  $ (\(FoldedExpressions x _)->x)
  $ getOutput
  $ fromRight undefined
  $ runParser foldedFunctionApplication
  $ "a b\n  c\n  d\n    e f g h\n  i"
