module Compiler.Frontend.UnicodeArt where

rightArrow_symbol :: String
rightArrow_symbol = "⟶"
rightArrow_art :: String
rightArrow_art = "->"

colonEqual_symbol :: String
colonEqual_symbol = "≔"
colonEqual_art :: String
colonEqual_art = ":="

rightMapsTo_symbol :: String
rightMapsTo_symbol = "↦"
rightMapsTo_art :: String
rightMapsTo_art = "|->"


two_em_dash_symbol :: String
two_em_dash_symbol = "⸺"
two_em_dash_art :: String
two_em_dash_art = "--"



---



lambdaArrows_symbol :: String
lambdaArrows_symbol = rightMapsTo_symbol
lambdaArrows_art :: String
lambdaArrows_art = rightMapsTo_art

commentPrelude_symbol :: String
commentPrelude_symbol = two_em_dash_symbol
commentPrelude_art :: String
commentPrelude_art = two_em_dash_art

