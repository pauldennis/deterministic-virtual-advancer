module Compiler.Frontend.LambdaAbstraction where

import Data.Either

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.Symbolgramme
import Compiler.Frontend.WhiteSpace
import Compiler.Frontend.FunctionApplication
import Compiler.Frontend.MonospaceTree

data Error_EatLambdaAbstraction token bodyError
  = ErrorWhileTryingToEatBindedVariablesList
      (Error_FoldedExpressions token)
  | ErrorWhileTryingToEatSeparatorBetweenVariableListAndBody
      (Error_LambdaArrow [EatExpectedCharacterError token])
  | ErrorWhileTryingToEatLambdaBody bodyError
  deriving Show

data LambdaAbstraction token body
  = LambdaAbstraction
      (FoldedExpressions token)
      body
  deriving Show

eatLambdaAbstraction
  :: CharacterRepresentable token
  => Parser token bodyError body
  -> Parser
      token
      (Error_EatLambdaAbstraction token bodyError)
      (LambdaAbstraction token body)
eatLambdaAbstraction bodyParser = do
  variableList
    <- wrapError
        ErrorWhileTryingToEatBindedVariablesList
        foldedFunctionApplication

  _spacesBetweenVariablesAndArrow
    <- absurdError eatSpaces
  _ <- absurdError $ optionalParse eatBranchingLongerThanZero

  _lambdaArrow
    <- wrapError
        ErrorWhileTryingToEatSeparatorBetweenVariableListAndBody
        eatLambdaArrow

  _spacesBetweenArrowAndBody
    <- absurdError eatSpaces

  _ <- absurdError $ optionalParse eatBranchingLongerThanZero --TODO

  body
    <- wrapError
        ErrorWhileTryingToEatLambdaBody
        bodyParser

  return $ LambdaAbstraction variableList body


test_eatLambdaAbstraction :: Test
test_eatLambdaAbstraction = (,) "lambda abstraction" $ and [ True
  , isLeft $ runParser theParser ""
  , isRight $ runParser theParser "_↦#"
  , isRight $ runParser theParser "_ ↦ #"
  , isRight $ runParser theParser "_|->#"
  , isRight $ runParser theParser "abc|->#"
  , isRight $ runParser theParser "abc\n  |->#"
  , isRight $ runParser theParser "abc a\n  |-> #"
  , isRight $ runParser theParser "_ _ |->  #"
  , isLeft $ runParser theParser "_ _ |-> _ _"
  , isLeft $ runParser theParser " _|-> #"
  , isRight $ runParser theParser
      $ (++"|-> #") $ concat $ replicate 500 "q "
  , isRight $ runParser theParser "_|->\n  #"
  , isRight $ runParser theParser "_   |->\n    #"
  , isRight $ runParser theParser "_ |->\n #"
  , isLeft $ runParser theParser "_ |->\n#"
  ]
  where
    theParser = eatLambdaAbstraction $ eatExpectedCharacter '#'

