module Compiler.Frontend.KeywordParser where

import CleanParser.API

import Compiler.Frontend.InputReconstruction
import Compiler.Frontend.Keywords


data Keyword_Let token
  = Keyword_Let_Short (InputReconstruction token)
  | Keyword_Let_Long (InputReconstruction token)
  deriving Show

data Error_Keyword_Let token
  = Error_Keyword_Let [EatExpectedCharacterError token]
  deriving Show

eat_keyword_let
  :: CharacterRepresentable token
  => Parser token (Error_Keyword_Let token) (Keyword_Let token)
eat_keyword_let
  = fmap packageOutput
  $ fmapOnParserError Error_Keyword_Let
  $ firstWorkingParser [shortVariant, longVariant]
  where
    shortVariant
      = fmap (Keyword_Let_Short . InputReconstruction)
      $ eatString keyword_let_short
    longVariant
      = fmap (Keyword_Let_Long . InputReconstruction)
      $ eatString keyword_let_long

    packageOutput string = string


---


data Keyword_In token
  = Keyword_In_Short (InputReconstruction token)
  | Keyword_In_Long (InputReconstruction token)
  deriving Show

data Error_Keyword_In token
  = Error_Keyword_In [EatExpectedCharacterError token]
  deriving Show

eat_keyword_in
  :: CharacterRepresentable token
  => Parser token (Error_Keyword_In token) (Keyword_In token)
eat_keyword_in
  = fmap packageOutput
  $ fmapOnParserError Error_Keyword_In
  $ firstWorkingParser [shortVariant, longVariant]
  where
    shortVariant
      = fmap (Keyword_In_Short . InputReconstruction)
      $ eatString keyword_in_short
    longVariant
      = fmap (Keyword_In_Long . InputReconstruction)
      $ eatString keyword_in_long

    packageOutput string = string

---



data Keyword_Switch token
  = Keyword_Switch_Short (InputReconstruction token)
  | Keyword_Switch_Long (InputReconstruction token)
  deriving Show

data Error_Keyword_Switch token
  = Error_Keyword_Switch [EatExpectedCharacterError token]
  deriving Show

eat_keyword_switch
  :: CharacterRepresentable token
  => Parser token (Error_Keyword_Switch token) (Keyword_Switch token)
eat_keyword_switch
  = fmap packageOutput
  $ fmapOnParserError Error_Keyword_Switch
  $ firstWorkingParser [shortVariant, longVariant]
  where
    shortVariant
      = fmap (Keyword_Switch_Short . InputReconstruction)
      $ eatString keyword_switch_short
    longVariant
      = fmap (Keyword_Switch_Long . InputReconstruction)
      $ eatString keyword_switch_long

    packageOutput string = string


---



data Keyword_Cases token
  = Keyword_Cases_Short (InputReconstruction token)
  | Keyword_Cases_Long (InputReconstruction token)
  deriving Show

data Error_Keyword_Cases token
  = Error_Keyword_Cases [EatExpectedCharacterError token]
  deriving Show

eat_keyword_cases
  :: CharacterRepresentable token
  => Parser token (Error_Keyword_Cases token) (Keyword_Cases token)
eat_keyword_cases
  = fmap packageOutput
  $ fmapOnParserError Error_Keyword_Cases
  $ firstWorkingParser [shortVariant, longVariant]
  where
    shortVariant
      = fmap (Keyword_Cases_Short . InputReconstruction)
      $ eatString keyword_cases_short
    longVariant
      = fmap (Keyword_Cases_Long . InputReconstruction)
      $ eatString keyword_cases_long

    packageOutput string = string



---
