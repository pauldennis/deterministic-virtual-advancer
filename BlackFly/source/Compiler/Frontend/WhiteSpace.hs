module Compiler.Frontend.WhiteSpace where

import Numeric.Natural
import Data.Void
import Data.List

import CleanParser.API

import Compiler.Frontend.UnicodeSubsets
import Compiler.Frontend.InputReconstruction

newtype NumberOfSpaces = NumberOfSpaces Natural
  deriving (Show, Eq)

data Spaces token
  = Spaces
      NumberOfSpaces
      (InputReconstruction token)
      (TheErrorThatWouldHaveHappenedWhenContinuing (EatExpectedCharacterError token))
  deriving Show

instance TokenNumerable (Spaces token) where
  getTokenSpan (Spaces (NumberOfSpaces x) _ _) = x

getNumberOfSpaces :: Spaces token -> NumberOfSpaces
getNumberOfSpaces (Spaces x _ _) = x

eatSpaces
  :: CharacterRepresentable token
  => Parser
        token
        Void
        (Spaces token)
eatSpaces
  = fmap wrapOutput
  $ greedilyRepeatParser
  $ eatExpectedCharacter space
  where
    wrapOutput
      ( GreedilyRepeatParserResult
          (GreedilyRepeatOutput spaces)
          firstNonSpace
      )
      = Spaces
          (NumberOfSpaces $ genericLength spaces)
          (InputReconstruction spaces)
          firstNonSpace



---



data NewLine token
  = NewLine (InputReconstruction token)
  deriving Show

data Error_NewLine character token
  = Hit_end_of_file_instead_of_the_newline_charactor_x character
  | Hit_token_x_but_wanted_one_with_attribute_x_ token character
  deriving Show

eatNewline
  :: CharacterRepresentable token =>
     Parser token (Error_NewLine Char token) (NewLine token)
eatNewline
  = fmap wrapOutput
  $ fmapOnParserError refineError
  $ eatExpectedCharacter newline
  where
    wrapOutput character = NewLine $ InputReconstruction [character]

    refineError (CannotEatTokenWithCharacter_x_BecauseEndOfFile character)
      = Hit_end_of_file_instead_of_the_newline_charactor_x character
    refineError (UnsuccessfulTriedToMatchChararecter_x_WithToken_x_ character token)
      = Hit_token_x_but_wanted_one_with_attribute_x_ token character

