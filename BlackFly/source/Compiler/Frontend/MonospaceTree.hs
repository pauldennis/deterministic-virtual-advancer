module Compiler.Frontend.MonospaceTree where

import Safe.Partial
import GHC.Stack

import Numeric.Natural
import Data.Either
import Data.List

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.WhiteSpace

{- |
  Tree like structure in monospace text:

  {-
    root
    ├─first-born
    │ ├─first-born grandson
    │ └─second-born grandson
    ├─second-born
    └─third-born
  -}

  here the branching is very verbose. one branch is for example:
  {-
    ´"\n│ └─"´
  -}
  it is nice for output. the same (or maybe similar) structure can be represented through only spaces and newlines:

  SpacesBranch is ment for trees like this one:
  {-
    root
      first-born
        first-born grandson
        second-born grandson
      second-born
      third-born
  -}
-}

-- TODO currently MonospaceRootLeaf can be a child of an inner node, which is not supposed to happen
data MonospaceTree token branch branchingError payload payloadError

  = MonospaceRootLeaf -- (this version has no branch compared to the leaf version)
    (WhyNoChildren token branchingError payloadError)
    payload

  | MonospaceInnerNode
      payload
      [MonospaceTree token branch branchingError payload payloadError]
      (FirstBorn token branch branchingError payload payloadError)
      (WhyNotOneChildrenMore token ErrorSpacesBranch payloadError)

  | MonospaceLeafNode
    branch
    (WhyNoChildren token branchingError payloadError)
    payload

  deriving (Show)

foldMonospaceTree
  :: (payload -> [summary] -> summary)
  -> MonospaceTree token branch branchingError payload payloadError
  -> summary
foldMonospaceTree squeezer (MonospaceRootLeaf _ payload)
  = squeezer payload []
foldMonospaceTree squeezer (MonospaceLeafNode _ _ payload)
  = squeezer payload []
foldMonospaceTree squeezer (MonospaceInnerNode payload rest (FirstBorn first) _)
  = result
  where
    result = squeezer payload recursiveSummaries

    recursiveSummaries
      = map (foldMonospaceTree squeezer)
      $ (first : rest)

extractPayloads
  :: MonospaceTree token branch branchingError payload payloadError
  -> [payload]
extractPayloads = foldMonospaceTree $ \a bs -> a : concat bs

monospaceTreeLevels
  :: MonospaceTree a b c d e
  -> [[MonospaceTree a b c d e]]
monospaceTreeLevels individual@(MonospaceRootLeaf _ _) = [[individual]]
monospaceTreeLevels individual@(MonospaceLeafNode _ _ _) = [[individual]]
monospaceTreeLevels individual@(MonospaceInnerNode _ rest (FirstBorn one) _) = result
  where
    result = [[individual]] ++ newerNumbers

    newerNumbers
      = zipListWith concat
      $ map monospaceTreeLevels
      $ (one : rest)

    --https://hackage.haskell.org/package/probability-0.2.7/docs/src/Numeric.Probability.Trace.html#zipListWith
    zipListWith :: ([a] -> b) -> [[a]] -> [b]
    zipListWith f = map f . transpose

populationCensus :: MonospaceTree a b c d e -> [Natural]
populationCensus = map (genericLength :: [a] -> Natural) . monospaceTreeLevels

test_population_census :: Test
test_population_census = (,) "population census" $ and [ True
  , ([1] ==) $ populationCensus $ (MonospaceRootLeaf u u)
  , ([1] ==) $ populationCensus last_leaf
  , ([1,1] ==) $ populationCensus (father_child)
  , ([1,3] ==) $ populationCensus father_with_tree_children
  , ([1,1,1] ==) $ populationCensus father_child_grandchild
  , ([1,1,1,1] ==) $ populationCensus father_child_grandchild_grandgrandchild
  , ([1,3,3] ==) $ populationCensus three_fathers_with_child
  , not $ areThereGrandChildren last_leaf
  , not $ areThereGrandChildren father_child
  , areThereGrandChildren father_child_grandchild
  , areThereGrandChildren father_child_grandchild_grandgrandchild
  ]
  where
    u = undefined

    last_leaf = MonospaceLeafNode u u u

    father_of leaf = MonospaceInnerNode u [] (FirstBorn leaf) u
    father_child = father_of last_leaf

    father_with_tree_children = MonospaceInnerNode u [last_leaf, last_leaf] (FirstBorn last_leaf) u

    three_fathers_with_child = MonospaceInnerNode u [father_child, father_child] (FirstBorn father_child) u

    father_child_grandchild = father_of father_child
    father_child_grandchild_grandgrandchild = father_of father_child_grandchild

areThereGrandChildren :: MonospaceTree a b c d e -> Bool
areThereGrandChildren tree
  = case monospaceTreeLevels tree of
         _:_:_:_ -> True
         _ -> False

getFirstGrandChild
  :: Partial
  => MonospaceTree a b c d e
  -> MonospaceTree a b c d e
getFirstGrandChild tree = withFrozenCallStack result
  where
    generations = monospaceTreeLevels tree
    result = case generations of
                   _:_:(firstGrandChild:_):_ -> firstGrandChild
                   _ -> error "there are no children"



newtype FirstBorn token branch branchingError payload whyStopSiblings
  = FirstBorn (MonospaceTree token branch branchingError payload whyStopSiblings)
  deriving Show

data WhyNoChildren token branchingError payloadError
  = WhyNoChildren (Error_Siblinghood token (branchingError token) payloadError)
  deriving Show

data WhyNotOneChildrenMore token branchingError payloadError
  = WhyNotOneChildrenMore (Error_OneNode (branchingError token) payloadError)
  deriving Show

allPayloads
  :: MonospaceTree token branch branchingError a payloadError
  -> [a]
allPayloads (MonospaceLeafNode _ _ payload) = [payload]
allPayloads (MonospaceInnerNode payload subPayloads (FirstBorn (subPayload)) _) = [payload] ++ (allPayloads subPayload) ++ (subPayloads >>= allPayloads)
allPayloads _ = error "payload for MonospaceRootLeaf"

data SpacesBranch token
  = SpacesBranch
      NumberOfSpaces
      (NewLine token)
      (Spaces token)
  deriving Show

instance Indentation (SpacesBranch token)
  where
    getIndentation (SpacesBranch (NumberOfSpaces numberOfMonoSpaces) _ _) = numberOfMonoSpaces

data ErrorSpacesBranch token
  = ErrorSpacesBranch_NoNewlineBeginning_x_BecauseEndOfFile Char
  | ErrorSpacesBranch_NoNewlineBeginning_x_BecauseFound_x Char token
  | IndendationNotLongEnough_needs_more_than_x_but_got_x IndendationOfParent NumberOfSpaces
  | IndendationNotExact_needs_x_but_got_x RequiredIndentation NumberOfSpaces
  deriving Show


eatSpaceBranching
  :: CharacterRepresentable token
  => Parser token (ErrorSpacesBranch token) (SpacesBranch token)
eatSpaceBranching = do
  newline <- fmapOnParserError refineNewlineError $ eatNewline
  spaces <- absurdError $ eatSpaces

  let indentation = getNumberOfSpaces spaces

  return $ SpacesBranch indentation newline spaces
  where
    refineNewlineError (Hit_end_of_file_instead_of_the_newline_charactor_x character)
      = ErrorSpacesBranch_NoNewlineBeginning_x_BecauseEndOfFile character
    refineNewlineError (Hit_token_x_but_wanted_one_with_attribute_x_ token character)
      = ErrorSpacesBranch_NoNewlineBeginning_x_BecauseFound_x character token


eatBranchingLongerThan
  :: CharacterRepresentable token
  => NumberOfSpaces
  -> Parser token (ErrorSpacesBranch token) (SpacesBranch token)
eatBranchingLongerThan (NumberOfSpaces atLeastIndentation) = do
  foundBranch <- eatSpaceBranching

  let foundIndentation = getIndentation foundBranch

  if atLeastIndentation < foundIndentation
    then return $ foundBranch
    else failWith
      $ IndendationNotLongEnough_needs_more_than_x_but_got_x (IndendationOfParent atLeastIndentation) (NumberOfSpaces foundIndentation)

eatBranchingLongerThanZero
  :: CharacterRepresentable token
  => Parser token (ErrorSpacesBranch token) (SpacesBranch token)
eatBranchingLongerThanZero = eatBranchingLongerThan (NumberOfSpaces 0)

eatBranchingWithExactLegth
  :: CharacterRepresentable token
  => NumberOfSpaces
  -> Parser token (ErrorSpacesBranch token) (SpacesBranch token)
eatBranchingWithExactLegth (NumberOfSpaces requiredIndentation) = do
  foundBranch <- eatSpaceBranching

  let foundSpaces = getIndentation foundBranch

  if requiredIndentation == foundSpaces
    then return $ foundBranch
    else failWith
      $ IndendationNotExact_needs_x_but_got_x (RequiredIndentation requiredIndentation) (NumberOfSpaces foundSpaces)




data IndendationOfParent
  = IndendationOfParent Natural
  deriving Show

data Error_RootNode a
  = RootNodeContentError a
  deriving Show

eatRootNodeWithSubChildren
  :: CharacterRepresentable token
  => Parser token errorCase output
  -> Parser
      token
      (Error_RootNode errorCase)
      (MonospaceTree token (SpacesBranch token) ErrorSpacesBranch output errorCase)
eatRootNodeWithSubChildren contentParser = do
  rootnode
    <- id
    $ wrapError RootNodeContentError
    $ contentParser

  maybeChildren
    <- absurdError
    $ optionalParse
    $ eatSiblinghood (IndendationOfParent 0)
    $ contentParser

  result <- case maybeChildren of
       ParserWorkedWith (Siblinghood firstBorn restChildren whyStop)
         -> return $ MonospaceInnerNode rootnode restChildren firstBorn whyStop
       ParserWouldNotWork whyAreThereNoChildren
         -> return $ MonospaceRootLeaf (WhyNoChildren whyAreThereNoChildren) rootnode

  return result


data Error_Siblinghood token branchingError payloadError
  = ExpectedProper_BranchingLongerThan_ButGot branchingError
  | ExpectedAtLeastOneSibling (Error_OneNode (ErrorSpacesBranch token) payloadError)
  deriving Show

data Siblinghood token branch branchingError payload payloadError whyStopSiblings
  = Siblinghood
      (FirstBorn token branch branchingError payload whyStopSiblings)
      [MonospaceTree token branch branchingError payload whyStopSiblings]
      (WhyNotOneChildrenMore token branchingError payloadError)

eatSiblinghood
  :: CharacterRepresentable token
  => IndendationOfParent
  -> Parser token payloadError output
  -> Parser
       token
       (Error_Siblinghood token (ErrorSpacesBranch token) payloadError)
       (Siblinghood token (SpacesBranch token) ErrorSpacesBranch output payloadError payloadError)
eatSiblinghood (IndendationOfParent indendationOfParent) eatContent = do
  firstBornBranch
    <- fmapOnParserError refineFirstBornBranchError
    $ positiveLookahead
    $ eatBranchingLongerThan
    $ (NumberOfSpaces indendationOfParent)

  let firstBornIndentation = getIndentation firstBornBranch
  let parseChild
        = eatOneNode
            (RequiredIndentation firstBornIndentation)
            eatContent

  firstChild <- wrapError ExpectedAtLeastOneSibling $ parseChild

  children
    <- absurdError
    $ greedilyRepeatParser
    $ parseChild

  let childNodes
        = outputsOfGreedilyRepeatParserResult children
  let whyNotOneChildMore
        = whyHadToBeGreedilyRepeatParserStopped children

  return $ Siblinghood (FirstBorn firstChild) childNodes (WhyNotOneChildrenMore whyNotOneChildMore)
  where
    refineFirstBornBranchError (PositiveLookaheadError x)
      = ExpectedProper_BranchingLongerThan_ButGot x


data RequiredIndentation
  = RequiredIndentation Natural
  deriving (Show, Eq)

data Error_OneNode branchError contentError
  = Error_OneNode_BranchingWithExactLegth branchError
  | Error_OneNode_ContentError contentError
  deriving Show

eatOneNode
  :: CharacterRepresentable token
  => RequiredIndentation
  -> Parser token payloadError payload
  -> Parser
       token
       (Error_OneNode (ErrorSpacesBranch token) payloadError)
       (MonospaceTree token (SpacesBranch token) ErrorSpacesBranch payload payloadError)
eatOneNode (RequiredIndentation requiredIndentation) eatContent = do
  branch
    <- fmapOnParserError Error_OneNode_BranchingWithExactLegth
    $ eatBranchingWithExactLegth
    $ (NumberOfSpaces requiredIndentation)

  content <- wrapError Error_OneNode_ContentError eatContent

  let indendationOfParent = requiredIndentation

  optionalChildren
    <- absurdError
    $ optionalParse
    $ eatSiblinghood (IndendationOfParent indendationOfParent)
    $ eatContent

  result <- case optionalChildren of
       ParserWorkedWith (Siblinghood firstBorn restSiblings whyStop)
         -> return $ MonospaceInnerNode content restSiblings firstBorn whyStop
       ParserWouldNotWork whyAreThereNoChildren
         -> return $ MonospaceLeafNode branch (WhyNoChildren whyAreThereNoChildren) content

  return $ result




---




test_cases_tree :: Test
test_cases_tree = (,) "test cases for tree" $ and [ True
  , isLeft $ runParser tree_parser ""
  , isRight $ runParser tree_parser "#"
  , isRight $ runParser tree_parser "#\n"
  , isLeft $ runParser (completeParse tree_parser) "#\n"
  , isRight $ runParser (completeParse tree_parser) "#\n #"
  , isRight $ runParser tree_parser "#\n #\n #"

  , (==) "\n #"
      $ (\(Remnant x)->x)
      $ getRemnant
      $ fromRight undefined
      $ runParser tree_parser
      $ "#\n"
      ++ "  #\n"
      ++ " #" -- cannot belong to tree from above! because indentation is inconsistent
  , isLeft $ runParser (completeParse tree_parser)
      $ "#\n"
      ++ "  #\n"
      ++ " #"
  , isRight $ runParser (completeParse tree_parser)
      $  "#\n"
      ++ "  #\n"
      ++ "  #"
  , isRight $ runParser (completeParse tree_parser)
      $  "#\n"
      ++ "  #\n"
      ++ "    #\n"
      ++ "  #"
  , isRight $ runParser (completeParse tree_parser)
      $  "#\n"
      ++ " #\n"
      ++ "    #\n"
      ++ " #\n"
      ++ "       #\n"
      ++ "       #\n"
      ++ " #\n"
      ++ "    #\n"
      ++ "     #\n"
      ++ "       #" --no newline
  , isRight $ runParser (completeParse $ eatLine tree_parser)
      $  "#\n"
      ++ " #\n"
      ++ "                      #\n"
      ++ " #\n"
      ++ "       #\n"
      ++ "       #\n"
      ++ " #\n"
      ++ "    #\n"
      ++ "     #\n"
      ++ "       #\n"
  , isRight $ runParser (completeParse $ eatLine tree_parser)
      $  ""
      ++ "#\n"
  , isLeft $ runParser (completeParse $ eatLine tree_parser)
      $  ""
      ++ "#"
  , isLeft $ runParser (completeParse $ eatLine tree_parser)
      $  ""
      ++ "#\n"
      ++ "#\n"

  , (== 2) $ the_length $ allPayloads $ getOutput $ fromRight undefined $ runParser tree_parser "#\n #"

  , isRight $ runParser tree_parser "#\n ~"
  , (>0) $ the_length $ show $ runParser tree_parser "#\n ~"

  , isRight $ runParser tree_parser $ concat $ map (++"#\n") $ take 100 $ map (flip replicate ' ') $ [0..]
  ]
  where
    tree_parser = eatRootNodeWithSubChildren (eatExpectedCharacter '#')

    the_length :: [a] -> Natural
    the_length = genericLength
