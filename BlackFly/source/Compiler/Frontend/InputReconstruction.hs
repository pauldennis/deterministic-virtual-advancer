module Compiler.Frontend.InputReconstruction where

import Numeric.Natural

data InputReconstruction token = InputReconstruction [token]
  deriving Show

class TokenNumerable a where
  getTokenSpan :: a -> Natural
