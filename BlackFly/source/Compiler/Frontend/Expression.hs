module Compiler.Frontend.Expression where

import CleanParser.API
import Data.Either

import Trifle.Test

import Compiler.Frontend.FunctionApplication
import Compiler.Frontend.LambdaAbstraction
import Compiler.Frontend.Parenthesis
import Compiler.Frontend.AppliedDefinitionBlock
import Compiler.Frontend.SwitchCase


data Expression token
  = Expression_FoldedApplication (FoldedExpressions token)
  | Expression_LambdaAbstraction
      (LambdaAbstraction token (Expression token))
  | Expression_Parenthesis
      (BodyInParenthesis (Expression token))
  | Expression_LetExpression
      (LetExpression token (Expression token) (Error_NonValidExpression token))
  | Expression_SwitchCase
      (DetailedSwitchCase token (Expression token))
  deriving Show


data Error_Expression token
  = Error_IN_FoldedApplication (Error_FoldedExpressions token)
  | Error_IN_LambdaAbstraction
      (Error_EatLambdaAbstraction token (Error_NonValidExpression token))
  | Error_IN_Parenthesis
      (Error_Parenthesis token (Error_NonValidExpression token))
  | Error_IN_LetExpression
      (Error_LetExpression
        token
        (Expression token)
        (Error_NonValidExpression token)
      )
  | Error_IN_SwitchCase
      (Error_SwitchCase
        token
        (Expression token)
        (Error_NonValidExpression token)
      )
  deriving Show


data Error_NonValidExpression token
  = Error_NonValidExpression [Error_Expression token]
  deriving Show

eatExpression
  :: CharacterRepresentable token
  => Parser token (Error_NonValidExpression token) (Expression token)
eatExpression
  = fmapOnParserError Error_NonValidExpression
  $ firstWorkingParser
  [ parser_eatLambdaAbstraction
  , parser_eatLetExpression
  , parser_SwitchCase
  , parser_foldedFunctionApplication
  , parser_eatWithParenthesis
  ]
  where
parser_eatLambdaAbstraction
  :: CharacterRepresentable token
  => Parser token (Error_Expression token) (Expression token)
parser_eatLambdaAbstraction = fmapOnParserError Error_IN_LambdaAbstraction $ fmap Expression_LambdaAbstraction $ eatLambdaAbstraction eatExpression

parser_SwitchCase
  :: CharacterRepresentable token
  => Parser token (Error_Expression token) (Expression token)
parser_SwitchCase = fmapOnParserError Error_IN_SwitchCase $ fmap Expression_SwitchCase $ eatSwitchCase eatExpression

parser_eatLetExpression
  :: CharacterRepresentable token
  => Parser token (Error_Expression token) (Expression token)
parser_eatLetExpression = fmapOnParserError Error_IN_LetExpression $ fmap Expression_LetExpression $ eatLetExpression eatExpression

parser_foldedFunctionApplication
  :: CharacterRepresentable token
  => Parser token (Error_Expression token) (Expression token)
parser_foldedFunctionApplication = fmapOnParserError Error_IN_FoldedApplication $ fmap Expression_FoldedApplication $ foldedFunctionApplication

parser_eatWithParenthesis
  :: CharacterRepresentable token
  => Parser token (Error_Expression token) (Expression token)
parser_eatWithParenthesis = fmapOnParserError Error_IN_Parenthesis $ fmap Expression_Parenthesis $ eatWithParenthesis eatExpression


test_nested_expressions :: Test
test_nested_expressions = (,) "test_nested_expressions" $ and [ True
  , isLeft $ runParser eatExpression ""
  , isRight $ runParser eatExpression "A ↦ (A)"
  , isLeft $ runParser eatExpression "let q = q in q" --missing :
  , isRight $ runParser eatExpression "let q := q in q"
  , isRight $ runParser (completeParse eatExpression) "q |-> q"
  , isRight $ runParser eatExpression "let q := (q |-> q) in q"
  , isRight $ runParser eatExpression "let q := q |-> q in q"
  , isRight $ runParser (completeParse eatExpression) "let q := q |-> q in switch q cases e |-> let q := q in q"
  , isRight $ runParser eatExpression "(q)"
  , isLeft $ runParser eatExpression "(switch q cases)" -- empty cases
  , isLeft $ runParser eatExpression "(switch q cases q)" -- no mapping
  , isRight $ runParser (completeParse eatExpression) $ ""
      ++ "let\n"
      ++ "     q1  :=    (w1)\n"
      ++ "     q2  := (switch object cases\n"
      ++ "               A |-> aa\n"
      ++ "               B |-> ab\n"
      ++ "               C |-> ac)\n"
      ++ "     q3    := w1\n"
      ++ " in qwwq"
  , isLeft $ runParser (completeParse eatExpression) $ ""
      ++ "let\n"
      ++ "     q1:=    (w1)\n"
      ++ "     q2  := switch object cases\n"
      ++ "     A |-> aa\n" -- not enougth indentation, (but rejected for the wrong reasons)
      ++ "     B |-> ab\n"
      ++ "     C |-> ac\n"
      ++ "     q3    := w1\n"
      ++ " in qwwq"
  , isRight $ runParser (eatExpression) $ ""
      ++ "(q |-> q q)\n"
  , isRight $ runParser (eatExpression) $ ""
      ++ "(q |-> q q\n"
      ++ "         q)\n"
  , isLeft $ runParser (completeParse eatExpression) $ ""
      ++ "let\n"
      ++ "     q1  :=    (w1)\n"
      ++ "     q2  := (q |-> q q\n"
      ++ "                       q q )\n"
      ++ "     q3    := w1\n"
      ++ " in qwwq"
  , isRight $ runParser eatExpression $ ""
      ++ "q |-> (q)"
  , isRight $ runParser eatExpression $ ""
      ++ "q |-> switch q cases q |-> (let q := q in q)"
      ++ "                     q   |-> qq"
  , isRight $ runParser eatExpression $ ""
      ++ "((((((((((((q))))))))))))"
  , isRight $ runParser eatExpression $ ""
      ++ "qwwqwqwqweewewew12323232"
  , isRight $ runParser eatExpression $ ""
      ++ "CXAE ksldlk"
  , isRight $ runParser (completeParse eatExpression) $ ""
      ++ "q |-> switch (QQQsdkj dssd ds dskkl) cases q |-> (let q := q in q)"
  , isRight $ runParser eatExpression $ ""
      ++ "switch QQQsdkj dssd ds dskkl cases Q |-> q"
  , isLeft $ runParser eatExpression $ ""
      ++ "switch A cases let q:=q in q |-> A"
  , isLeft $ runParser eatExpression $ ""
      ++ "switch A cases let (q:=q in q) |-> A"
  , isLeft $ runParser (completeParse eatExpression) $ ""
      ++ "switch A cases\n"
      ++ "  Aa ↦ B1\n"
      ++ "  Ba ↦ B2\n"
      ++ "  Ca ↦ (let\n"
      ++ "            abc ≔ number_1\n"
      ++ "            abc ≔ number_2\n"
      ++ "            abc ≔ number_3\n"
      ++ "            abc ≔ function argument)\n"
  , isRight $ runParser (completeParse eatExpression) $ ""
      ++ "switch A cases\n"
      ++ "  Aa ↦ B1\n"
      ++ "  Ba ↦ B2\n"
      ++ "  Ca ↦ (let\n"
      ++ "            abc ≔ (number_1)\n"
      ++ "            abc ≔ (number_2)\n"
      ++ "            abc ≔ (number_3)\n"
      ++ "            abc ≔ function argument\n"
      ++ "         in abd)"
  , isRight $ runParser eatExpression $ ""
      ++ "switch A cases\n"
      ++ "  Aa ↦ B1\n"
      ++ "  Ba ↦ B2\n"
      ++ "  Ca ↦ let\n"
      ++ "            abc ≔ (number_1)\n"
      ++ "            abc ≔ (number_2)\n"
      ++ "            abc ≔ (number_3)\n"
      ++ "            abc ≔ function argument\n"
  , isRight $ runParser (completeParse eatExpression) $ ""
    ++ "y ↦ let\n"
    ++ "        a := b\n"
    ++ "     in c"
  , isRight $ runParser (completeParse eatExpression) $ ""
    ++ "y ↦ let\n"
    ++ "        a := (let q := q\n"
    ++ "              in q)\n"
    ++ "     in c"
  ]

