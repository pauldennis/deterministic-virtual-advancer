module Compiler.Frontend.Comment where

import Data.Either
import Data.Void

import Trifle.Test
import CleanParser.API

import Compiler.Frontend.Symbolgramme


finishLine
  :: CharacterRepresentable token
  => Parser token (Error_LineParser Void (EatExpectedCharacterError token)) (Record (GreedilyRepeatParserResult token (EatNotExpectedCharacterError token)) token)
finishLine
  = eatLine
  $ greedilyRepeatParser
  $ eatExpectedNotCharacter
  $ '\n'

data Error_Comment
  = Error_Comment
  deriving Show

data Comment
  = Comment
  deriving Show

eatOneLineComment
  :: CharacterRepresentable token
  => Parser token Error_Comment Comment
eatOneLineComment = do
  _ <- fmapOnParserError (const Error_Comment) eatCommentPrelude
  _ <- fmapOnParserError (const Error_Comment) finishLine
  return Comment


test_comment :: Test
test_comment = (,) "comments test" $ and [ True
  , isLeft $ runParser eatOneLineComment $ ""
  , isRight $ runParser eatOneLineComment $ "-- sdkjks#+-+-{23\n"
  , isRight $ runParser eatOneLineComment $ "--\n"
  , isLeft $ runParser eatOneLineComment $ "-- "
  , isLeft $ runParser eatOneLineComment $ "--"
  , isRight $ runParser eatOneLineComment $ "⸺\n"
  ]




---

data EmptyLine
  = EmptyLine
  deriving Show

eatEmptyLine
  :: CharacterRepresentable token
  => Parser token Void EmptyLine
eatEmptyLine = return EmptyLine


test_emptyLine :: Test
test_emptyLine = (,) "comments test" $ and [ True
  , isRight $ runParser eatEmptyLine $ ""
  , isRight $ runParser eatEmptyLine $ "~"

  , isRight $ runParser (completeParse eatEmptyLine) $ ""
  , isLeft $ runParser (completeParse eatEmptyLine) $ "\n\n"

  , isLeft $ runParser (completeParse eatEmptyLine) $ "~"
  ]


