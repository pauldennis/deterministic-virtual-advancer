module Compiler.Frontend.VariableNameAndConstructorName where

import Data.Either

import Trifle.Test

import CleanParser.API

import Compiler.Frontend.UnicodeSubsets
import Compiler.Frontend.InputReconstruction
import Compiler.Frontend.Keywords

newtype VariableNameMustStopEatingHere token
  = VariableNameMustStopEatingHere (Error_EatTokenWithAttribute token Char)
  deriving Show

-- TODO rename to DetailedFunctionName
-- or maybe LowerCase token
-- or LowerCase name
data DetailedVariableName token
  = DetailedVariableName
      String
      (InputReconstruction token)
      (VariableNameMustStopEatingHere token)
  deriving Show

data Error_VariableName token
  = Error_VariableName_UnexpectedEOF_WantOneOf_x_ [Char]
  | Error_VariableNameCannotBeginWith_x_butItCouldWith_x token [Char]
  | Error_KeywordIsNotAnVariableName String [token] (VariableNameMustStopEatingHere token)
  deriving Show


eatVariableName
  :: CharacterRepresentable token
  => Parser token (Error_VariableName token) (DetailedVariableName token)
eatVariableName = do
  firstLetter
    <- fmapOnParserError refineError
      $ eatOneCharOfString possibleFirstLettersForVariableName

  GreedilyRepeatParserResult
    (GreedilyRepeatOutput proceedingLetters)
    (TheErrorThatWouldHaveHappenedWhenContinuing variableNameMustStopHere)
    <- manyOfOneOfString possibleProceedingLettersForVariableName

  let variableName = firstLetter : proceedingLetters
  let abstractVariableName = map getCharacter variableName

  let isItKeyword = elem abstractVariableName list_of_all_keywords_that_should_not_be_variable_names

  failIfWith isItKeyword $ Error_KeywordIsNotAnVariableName abstractVariableName variableName (VariableNameMustStopEatingHere variableNameMustStopHere)

  return
    $ DetailedVariableName
        abstractVariableName
        (InputReconstruction variableName)
        (VariableNameMustStopEatingHere variableNameMustStopHere)
  where
    manyOfOneOfString string
      = absurdError $ greedilyRepeatParser $ eatOneCharOfString string

    refineError (CannotFindTokenWithOneOfTheAttributes_x_BecauseEndOfFile x)
      = Error_VariableName_UnexpectedEOF_WantOneOf_x_ x
    refineError (TheFoundToken_x_DoesFullfillAnyOfTheAllowedProperties_x_ token properties)
      = Error_VariableNameCannotBeginWith_x_butItCouldWith_x token properties

    refineError _ = error "this error is new, what to do now?"


test_eatVariableName :: Test
test_eatVariableName = (,) "variable names" $ and
  [ isRight $ runParser eatVariableName "lowerCaseCamelCase"
  , isLeft $ runParser eatVariableName "UpperCaseCamelCase"
  , isRight $ runParser eatVariableName "snake_case"
  , isRight $ runParser eatVariableName "_Upper_Case_Snake_Case"
  , isRight $ runParser eatVariableName "_"
  , isRight $ runParser eatVariableName "_123"
  , isRight $ runParser eatVariableName "_abc"
  , isLeft $ runParser eatVariableName "9dontStartWithNumber"
  , isLeft $ runParser eatVariableName "'"
  , isRight $ runParser eatVariableName "_'"
  , isRight $ runParser eatVariableName "abc'def'ghi"

  , isLeft $ runParser eatVariableName "in"
  , isLeft $ runParser eatVariableName "let"
  ]


--


newtype ConstructorNameMustStopEatingHere a
  = ConstructorNameMustStopEatingHere a
  deriving Show

data DetailedConstructorName token constructorStopsHereWith
  = DetailedConstructorName
      String
      (InputReconstruction token)
      (ConstructorNameMustStopEatingHere constructorStopsHereWith)
  deriving Show

data Error_ConstructorName token attributes
  = Error_ConstructorName_UnexpectedEOF_WantOneOf_x_ attributes
  | Error_ConstructorNameCannotBeginWith_x_butItCouldWith_x token attributes
  deriving Show

eatConstructorName
  :: CharacterRepresentable a
  => Parser a (Error_ConstructorName a [Char]) (DetailedConstructorName a (Error_EatTokenWithAttribute a Char))
eatConstructorName
  = fmapOnParserError refineError
  $ do
      firstLetter
        <- eatOneCharOfString possibleFirstLetttersForConstructorName

      GreedilyRepeatParserResult
        (GreedilyRepeatOutput proceedingLetters)
        (TheErrorThatWouldHaveHappenedWhenContinuing constructorNameMustStopHere)
        <- manyOfOneOfString possibleProceedingLettersForConstructorName

      let variableName = firstLetter : proceedingLetters

      return
        $ DetailedConstructorName
            (map getCharacter variableName)
            (InputReconstruction variableName)
            (ConstructorNameMustStopEatingHere constructorNameMustStopHere)
  where
    manyOfOneOfString string
      = absurdError $ greedilyRepeatParser $ eatOneCharOfString string

    refineError (CannotFindTokenWithOneOfTheAttributes_x_BecauseEndOfFile x)
      = Error_ConstructorName_UnexpectedEOF_WantOneOf_x_ x
    refineError (TheFoundToken_x_DoesFullfillAnyOfTheAllowedProperties_x_ token properties)
      = Error_ConstructorNameCannotBeginWith_x_butItCouldWith_x token properties

    refineError _ = error "unknown error should not happen in eatConstructorName"

test_eatConstructorName :: Test
test_eatConstructorName = (,) "constructor names" $ and
  [ isLeft $ runParser eatConstructorName "lowerCaseCamelCase"
  , isRight $ runParser eatConstructorName "UpperCaseCamelCase"
  , isLeft $ runParser eatConstructorName "snake_case"
  , isRight $ runParser eatConstructorName "Upper_Case_Snake_Case"
  , isLeft $ runParser eatConstructorName "_"
  , isLeft $ runParser eatConstructorName "_123"
  , isRight $ runParser eatConstructorName "Module_123"
  , isLeft $ runParser eatConstructorName "_abc"
  , isLeft $ runParser eatConstructorName "9DontStartWithNumber"
  , isLeft $ runParser eatConstructorName "'"
--   , isLeft $ runParser eatConstructorName "Module'" --TODO
  , isRight $ runParser eatConstructorName "Abc_def_ghi"

  , isRight $ runParser eatConstructorName "§"
  , isRight $ runParser eatConstructorName "§_"
  , isRight $ runParser eatConstructorName "§_A"
  , isRight $ runParser eatConstructorName "§AA"
  , isRight $ runParser (completeParse eatConstructorName) "§AAsdkjdskj"
  , isRight $ runParser (completeParse eatConstructorName) "§_AAsdkjdskjsdAKJDKJ_21349"
  , isLeft $ runParser (completeParse eatConstructorName) "§AA§"
  , isLeft $ runParser (completeParse eatConstructorName) "§§"
  , isRight $ runParser eatConstructorName "§§"
  ]

