module Compiler.Frontend.Symbolgramme where

import CleanParser.API

import Compiler.Frontend.UnicodeArt
import Compiler.Frontend.InputReconstruction

-- TODO template haskell?


data LambdaArrow token
  = PrettyLambdaArrow (InputReconstruction token)
  | AsciiInputLambdaArrow (InputReconstruction token)
  deriving Show

data Error_LambdaArrow errorCase
  = Error_LambdaArrow errorCase
  deriving Show

eatLambdaArrow
  :: CharacterRepresentable token
  => Parser token (Error_LambdaArrow ([EatExpectedCharacterError token])) (LambdaArrow token)
eatLambdaArrow
  = fmapOnParserError Error_LambdaArrow
  $ firstWorkingParser [prettyVariant, inputVariant]
  where
    prettyVariant
      = fmap (PrettyLambdaArrow . InputReconstruction)
      $ eatString lambdaArrows_symbol
    inputVariant
      = fmap (AsciiInputLambdaArrow . InputReconstruction)
      $ eatString lambdaArrows_art


--



data DefinitionColonEqual token
  = PrettyDefinitionColonEqual (InputReconstruction token)
  | AsciiInputDefinitionColonEqual (InputReconstruction token)
  deriving Show

data Error_DefinitionColonEqual token
  = Error_DefinitionColonEqual [EatExpectedCharacterError token]
  deriving Show

eatDefinitionColonEqual
  :: CharacterRepresentable token
  => Parser token (Error_DefinitionColonEqual token) (DefinitionColonEqual token)
eatDefinitionColonEqual
  = fmapOnParserError Error_DefinitionColonEqual
  $ firstWorkingParser [prettyVariant, inputVariant]
  where
    prettyVariant
      = fmap (PrettyDefinitionColonEqual . InputReconstruction)
      $ eatString colonEqual_symbol
    inputVariant
      = fmap (AsciiInputDefinitionColonEqual . InputReconstruction)
      $ eatString colonEqual_art


---




data DefinitionRightMapsTo token
  = PrettyDefinitionRightMapsTo (InputReconstruction token)
  | AsciiInputDefinitionRightMapsTo (InputReconstruction token)
  deriving Show

data Error_DefinitionRightMapsTo token
  = Error_DefinitionRightMapsTo [EatExpectedCharacterError token]
  deriving Show

eatDefinitionRightMapsTo
  :: CharacterRepresentable token
  => Parser token (Error_DefinitionRightMapsTo token) (DefinitionRightMapsTo token)
eatDefinitionRightMapsTo
  = fmapOnParserError Error_DefinitionRightMapsTo
  $ firstWorkingParser [prettyVariant, inputVariant]
  where
    prettyVariant
      = fmap (PrettyDefinitionRightMapsTo . InputReconstruction)
      $ eatString rightMapsTo_symbol
    inputVariant
      = fmap (AsciiInputDefinitionRightMapsTo . InputReconstruction)
      $ eatString rightMapsTo_art





---



data CommentPrelude token
  = Pretty_CommentPrelude (InputReconstruction token)
  | Ascii_CommentPrelude (InputReconstruction token)
  deriving Show

data Error_CommentPrelude token
  = Error_CommentPrelude [EatExpectedCharacterError token]
  deriving Show

eatCommentPrelude
  :: CharacterRepresentable token
  => Parser token (Error_CommentPrelude token) (CommentPrelude token)
eatCommentPrelude
  = fmapOnParserError Error_CommentPrelude
  $ firstWorkingParser [prettyVariant, inputVariant]
  where
    prettyVariant
      = fmap (Pretty_CommentPrelude . InputReconstruction)
      $ eatString
      $ commentPrelude_symbol
    inputVariant
      = fmap (Ascii_CommentPrelude . InputReconstruction)
      $ eatString
      $ commentPrelude_art
