module Compiler.Frontend.UsingTheParser where

import CleanParser.API

import Compiler.Frontend.StateAsSyntax
import Compiler.Frontend.FunctionName

parse_to_StateAsSyntax
  :: String
  -> Either
      (Error_State NumberedToken)
      (StateAsSyntax NumberedToken)
parse_to_StateAsSyntax = parserWithNumberedTokensComplete eatState

parseStateAsUnixTool :: IO ()
parseStateAsUnixTool = interact $ errorOnError . parse_to_StateAsSyntax
  where
    errorOnError (Right output) = show output
    errorOnError (Left errorCase) = error $ show errorCase


parse_to_FunctionNameAsSyntax
  :: String
  -> Either
      (Error_EatingFunctionNameDidNotWork NumberedToken)
      (FunctionName NumberedToken)
parse_to_FunctionNameAsSyntax
  = parserWithNumberedTokensComplete
  $ eatFunctionName
