module Compiler.Backend.Reject where

import Data.Either.Combinators
import Data.List.Extra
import qualified Data.ByteString as Bytes
import Trifle.Missing

import Trifle.Patchwork
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Store.Fuchs

data Error_Rejection_FuchsProgramState
  = Error_TwoTimesTheSameBinding (Bytes.ByteString) [[(VariableName, FuchsProgram)]]
  | Error_Expression Error_Rejection_FuchsProgram
  deriving Show

reject_bad_FuchsProgramState
  :: FuchsProgramState
  -> Either Error_Rejection_FuchsProgramState ()
reject_bad_FuchsProgramState (FuchsProgramState bindings) = do

  let groupedBindings
        = groupSortOn ((\(VariableName x)->x) . fst)
        $ bindings
  let offenders = filter (not . isSingleton) groupedBindings
  let convenience
        = (\(VariableName x) ->x)
        $ fst
        $ head -- one of the same variables
        $ head -- fist duplicate new variable
        $ offenders

  if null offenders
     then return ()
     else Left $ Error_TwoTimesTheSameBinding convenience offenders

  mapM_ (mapLeft Error_Expression . recjections_in_expressions) $ map snd bindings

  return ()


---

data Error_Rejection_FuchsProgram
  = Error_Ambiguous_Binding_in_Let_binding
  | Error_Ambivalent_pattern_match_to_new_variable DuplicatVariable
  | Error_Ambivalent_Arguments_in_lambda DuplicatVariable
  | Error_Ambivalent_Bindings_in_let_expression DuplicatVariable
  deriving Show

-- TODO catamorphism?
recjections_in_expressions
  :: FuchsProgram
  -> Either Error_Rejection_FuchsProgram ()

recjections_in_expressions (Apply function arguments) = do
  recjections_in_expressions function
  mapM_ recjections_in_expressions arguments
  return ()

recjections_in_expressions (SaturatedConstruction _ arguments) = do
  mapM_ recjections_in_expressions arguments
  return ()

recjections_in_expressions (Case scrutinee alternatives defaultPath) = do
  mapLeft Error_Ambivalent_pattern_match_to_new_variable
    $ mapM_ no_puplications_in_variable_list
    $ map (\(_,x,_)->x)
    $ alternatives

  -- recursion part
  recjections_in_expressions scrutinee
  mapM_ recjections_in_expressions $ map (\(_,_,x)->x) alternatives
  recjections_in_expressions defaultPath
  return ()

recjections_in_expressions (Lambda variableNames _globalVariables body) = do
  mapLeft Error_Ambivalent_Arguments_in_lambda
    $ no_puplications_in_variable_list
    $ variableNames

  recjections_in_expressions body

recjections_in_expressions (Variable _) = return ()

recjections_in_expressions (RecursiveLet bindings body) = do
  mapLeft Error_Ambivalent_Bindings_in_let_expression
    $ no_puplications_in_variable_list
    $ map fst bindings

  mapM_ recjections_in_expressions $ map snd bindings
  recjections_in_expressions body

recjections_in_expressions (Trap body) = do
  recjections_in_expressions body

recjections_in_expressions new_case = error $ "missing case in rejction of FuchsProgram: " ++ show new_case


--


data DuplicatVariable
  = DuplicatVariable VariableName
  deriving Show

--TODO negative test case is missing
no_puplications_in_variable_list
  :: [VariableName]
  -> Either DuplicatVariable ()
no_puplications_in_variable_list variable_list = do
  if isNubbed variable_list
    then return ()
    else Left $ DuplicatVariable (duplicateExample variable_list)
