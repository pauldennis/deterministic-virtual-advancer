module Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt where

-- TODO wrong place

import Control.Monad.Trans.Except
import Control.Monad.Trans.Class

import Data.List

import qualified Control.Monad.State as State

import Trifle.Rename

import Store.Fuchs
import Store.OpaqueState
import Store.Tangle
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State



storeProgram :: FuchsProgram -> State.State TangleStore FuchsReceipt

storeProgram (SaturatedConstruction constructorName arguments) = do
  compiledArguments <- mapM storeProgram arguments
  pawn__PrimitiveDataConstructor_x constructorName compiledArguments

storeProgram (Case program alternatives defaultPath) = do
  examinee <- storeProgram program
  alternativePaths <- mapM storeProgram $ map (\(_,_,p)->p) alternatives
  defaultProgram <-storeProgram defaultPath

  -- TODO performance?
  let alternativeMappings = zipWith (\(a,b,_) c->(a,b,c)) alternatives alternativePaths

  pawn__SwitchCase examinee alternativeMappings defaultProgram

storeProgram (Lambda free_variables variables body) = do
  compiledBody <- storeProgram body
  pawn__ExpresionWithPlaceHolder free_variables variables compiledBody

storeProgram (Variable variableName) = do
  pawn__PlaceHolder variableName

storeProgram (RecursiveLet bindings aimResult) = do
  bindingsReceipts <- mapM pawnBinding bindings
  aimResultReceipt <- storeProgram aimResult
  pawn__RecursiveLetBindings bindingsReceipts aimResultReceipt
  where
    pawnBinding
      :: (VariableName, FuchsProgram)
      -> State.State
          TangleStore
          (VariableName, FuchsReceipt)
    pawnBinding (variableName, package) = do
      packageReceipt <- storeProgram package
      return $ (variableName, packageReceipt)

storeProgram (Apply function arguments) = do
  functionProgram <- storeProgram function
  argumentPrograms <- mapM storeProgram arguments
  pawn__FunctionApplication functionProgram argumentPrograms

storeProgram (Trap errorThing) = do
  errorPackage <- storeProgram errorThing
  pawn__CustomUserError errorPackage

-- TODO should there be a seperated storeState function?
storeProgram (OpaqueNativeObject fuchsProgramStateBindings) = do
  StateReceipt state <- storeFuchsProgramState fuchsProgramStateBindings
  return state

storeProgram program = error $ "unimplemented case: " ++ show program



----



destoreProgram :: FuchsReceipt -> ExceptT Error_FuchsStore (State.State TangleStore) FuchsProgram
destoreProgram fuchsReceipt = do
  fuchs <- redeemFuchs_FuchsStore fuchsReceipt
  case fuchs of

      PrimitiveDataConstructor constructorName children -> do
        childProgramms <- mapM destoreProgram children
        return $ SaturatedConstruction constructorName childProgramms

      SwitchCase innerFuchsReceipt alternatives defaultPath -> do
        examinee <- destoreProgram innerFuchsReceipt
        defaultProgram <- destoreProgram defaultPath
        alternativePrograms <- mapM decompileAlternative alternatives
        return $ Case examinee alternativePrograms defaultProgram
        where
          decompileAlternative (constructorName, variableNames, path) = do
            alternativePath <- destoreProgram path
            return (constructorName, variableNames, alternativePath)

      ExpresionWithPlaceHolder free_variables variables bodyReceipt
        -> do
            bodyProgramm <- destoreProgram bodyReceipt
            return $ Lambda free_variables variables bodyProgramm

      PlaceHolder variableName -> do
        return $ Variable variableName

      RecursiveLetBinding bindings aimResult -> do
        bindedPrograms <- mapM decompileBinding bindings
        aim <- destoreProgram aimResult
        return $ RecursiveLet bindedPrograms aim
        where
          decompileBinding (variableName, innerFuchsReceipt) = do
            program <- destoreProgram innerFuchsReceipt
            return (variableName, program)

      FunctionApplication function argumentReceiptss -> do
        functionProgram <- destoreProgram function
        arguments <- mapM destoreProgram argumentReceiptss
        return $ Apply functionProgram arguments

      CustomUserError errorThing -> do
        errorPackage <- destoreProgram errorThing
        return $ Trap $ errorPackage

      OpaqueBunchOfBindings bindings -> do
        destored_bindings <- mapM destoreBinding bindings
        return
          $ OpaqueNativeObject
          $ FuchsProgramState --TODO two indirections?
          $ destored_bindings
        where
          destoreBinding (variableName, function) = do
            program <- destoreProgram function
            return (variableName, program)

      the_case -> error $ "missing case in destoreProgram: " ++ show the_case


---


uglyPrintProgram :: FuchsProgram -> String
uglyPrintProgram = unlines . uglyLineShowProgram

uglyLineShowProgram :: FuchsProgram -> [String]

uglyLineShowProgram (SaturatedConstruction (ConstructorName constructorName) arguments)
  = []
  ++ [show constructorName]
  ++ recursion
  where
    recursion = map ("  "++) $ arguments >>= uglyLineShowProgram

uglyLineShowProgram (Case program alternatives defaultPath)
  = []
  ++ ["case"]
  ++ map ("  "++) (uglyLineShowProgram program)
  ++ ["of"]
  ++ map ("  "++) (alternatives >>= printAlternative)
  ++ ["default -> "]
  ++ map ("  "++) (uglyLineShowProgram defaultPath)
  where
    printAlternative (ConstructorName constructorName, variableNames, path)
      = []
      ++ [(intercalate " " $ show constructorName : rawVariableNames) ++ " ->"]
      ++ map ("  "++) (uglyLineShowProgram path)
      where
        rawVariableNames = map (\(VariableName x)->show x) variableNames

uglyLineShowProgram (Lambda free_variables variables body)
  = []
  ++ [raw_free_variables ++ "\\" ++ rawVariableStrings ++ " ->"]
  ++ map ("  "++) (uglyLineShowProgram body)
  where
    rawVariableStrings :: String
    rawVariableStrings
      = intercalate " "
      $ map (\(VariableName y)->show y) variables

    raw_free_variables :: String
    raw_free_variables
      = intercalate " "
      $ map (\(VariableName y)->show y) free_variables


uglyLineShowProgram (Variable (VariableName name))
  = []
  ++ [show name]

uglyLineShowProgram (RecursiveLet bindedPrograms aimResult)
  = []
  ++ ["let"]
  ++ map ("  "++) (bindedPrograms >>= printBinding)
  ++ ["in"]
  ++ map ("  "++) (uglyLineShowProgram aimResult)

uglyLineShowProgram (Apply function arguments)
  = []
  ++ ["("]
  ++ map ("  "++) (uglyLineShowProgram function)
  ++ [")"]
  ++ [ "(" ++ (intercalate ") (" $ uglyLineShowProgram =<< arguments) ++ ")"]

uglyLineShowProgram (Trap errorThing)
  = []
  ++ ["Trap"]
  ++ map ("  "++) (uglyLineShowProgram errorThing)

uglyLineShowProgram (OpaqueNativeObject (FuchsProgramState bindedPrograms))
  = []
  ++ ["┌"]
  ++ ["│OpaqueNativeObject"]
  ++ ["│let"]
  ++ map ("│ "++) (bindedPrograms >>= printBinding)
  ++ ["└ "]

uglyLineShowProgram x = error $ "ugly print unimplemented for: " ++ show x

printBinding
  :: (VariableName, FuchsProgram)
  -> [String]
printBinding (VariableName constructorName, path)
  = []
  ++ [show constructorName ++ " = "]
  ++ map ("  "++) (uglyLineShowProgram path)


---


uglyPrintState :: FuchsProgramState -> String
uglyPrintState (FuchsProgramState bindings)
  = (bindings >>= printEnvironment)
  where
    printEnvironment (VariableName bytes, programm) = ""
      ++ show bytes ++ " := " ++ "\n"
      ++ unlines (map ("  "++) (uglyLineShowProgram programm))
      ++ "\n"




---






inAndOutProgramTest
  :: FuchsProgram
  -> ExceptT Error_FuchsStore (State.State TangleStore) (Bool, FuchsProgram, FuchsProgram)
inAndOutProgramTest program = do
  receipt <- lift $ storeProgram program
  fuchs <- destoreProgram receipt
  return $ (program == fuchs, program, fuchs)

actualDoTest :: (Either Error_FuchsStore (Bool, FuchsProgram, FuchsProgram), TangleStore)
actualDoTest = ($ emptyTangleStore) $ State.runState $ runExceptT $ inAndOutProgramTest
  programExample

actualPrettyPrint :: IO ()
actualPrettyPrint = putStr $ uglyPrintProgram
--   fibonacciFunction
  programExample





---




-- TODO performance, not first unzipping and zipping the list again
storeFuchsProgramState
  :: FuchsProgramState
  -> State.State TangleStore StateReceipt
storeFuchsProgramState (FuchsProgramState bindings) = do
  let variables = map fst bindings
  let rawBodies = map snd bindings

  storedProgramms <- mapM storeProgram rawBodies
  let storableBindings = zip variables storedProgramms

  rawStateReceipt <- pawn__OpaqueBunchOfBindings storableBindings

  return $ StateReceipt rawStateReceipt




data Error_WhileDestoringFuchsProgramState
  = Error_WhileDestoringFuchsProgramState Error_RedeemOpaqueState
  | Error_WhileDestoringFuchProgram Error_FuchsStore
  deriving Show

destoreFuchsProgramState
  :: StateReceipt
  -> ExceptT
      Error_WhileDestoringFuchsProgramState
      (State.State TangleStore)
      FuchsProgramState
destoreFuchsProgramState the_new_state = do
  OpaqueState binding_of_new_state
    <- mapException Error_WhileDestoringFuchsProgramState
    $ redeem_as_State
    $ the_new_state

  bindings <- mapM destoreRightPart binding_of_new_state

  let fuchsProgramState = FuchsProgramState bindings

  return fuchsProgramState
  where
    destoreRightPart (variableName, receipt)
      = mapException Error_WhileDestoringFuchProgram
      $ do
          program <- destoreProgram receipt
          return (variableName, program)
