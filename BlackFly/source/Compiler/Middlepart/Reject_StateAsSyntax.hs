module Compiler.Middlepart.Reject_StateAsSyntax where

import CleanParser.API

import Compiler.Frontend.StateAsSyntax


data Error_Rejection
  = Error_Rejection
  deriving Show

reject_bad_StateAsSyntax
  :: (StateAsSyntax NumberedToken)
  -> Either Error_Rejection ()
reject_bad_StateAsSyntax (StateAsSyntax _stateAsSyntax) = do
  return ()
