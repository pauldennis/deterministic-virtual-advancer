module Compiler.Middlepart.StateAsSyntax_to_FuchsProgram where

{- |
throw away all detailed information and just leave the assembly and convert it to an type that the backend understands
-}

import qualified Data.ByteString.UTF8 as UTF8
import Store.Fuchs

import Data.List
import Data.Either
import Data.Either.Combinators

import CleanParser.API

import Compiler.Frontend.FunctionName
import Compiler.Frontend.FunctionApplication
import Compiler.Frontend.Expression
import Compiler.Frontend.Definition
import Compiler.Frontend.VariableNameAndConstructorName
import Compiler.Frontend.StateRecord
import Compiler.Frontend.SwitchCase
import Compiler.Frontend.StateAsSyntax
import Compiler.Frontend.Parenthesis
import Compiler.Frontend.LambdaAbstraction
import Compiler.Frontend.AppliedDefinitionBlock

import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State


translate_StateAsSyntax_to_FuchsProgramState
  :: StateAsSyntax newlineSymbol
  -> Either
      (Error_translate_Expression newlineSymbol)
      FuchsProgramState
translate_StateAsSyntax_to_FuchsProgramState (StateAsSyntax stateRecords)
  = translate_listOfRecords stateRecords



translate_listOfRecords
  :: [Record (StateRecord token) newlineSymbol]
  -> Either (Error_translate_Expression token) FuchsProgramState
translate_listOfRecords records
  = fmap FuchsProgramState
  $ fmap concat
  $ mapM translate_Record records



translate_Record
  :: Record (StateRecord token) newlineSymbol
  -> Either
      (Error_translate_Expression token)
      [(VariableName, FuchsProgram)]
translate_Record (Record record _)
  = translate_StateRecord record



translate_StateRecord
  :: StateRecord token
  -> Either
      (Error_translate_Expression token)
      [(VariableName, FuchsProgram)]
translate_StateRecord (StateRecord_Definition definition)
  = fmap (\x -> [x])
  $ translate_Definition
  $ definition
translate_StateRecord (StateRecord_EmptyLine _)
  = Right []



translate_Definition
  :: Definition token (Expression token)
  -> Either
      (Error_translate_Expression token)
      (VariableName, FuchsProgram)
translate_Definition
  (Definition
    (NewTerm
      (DetailedVariableName string _ _)
    )
    (KnownTerm knownTerm)
  )
  = do
    translated_body <- translate_Expression knownTerm
    return $ (newVariable, translated_body)
    where
      newVariable = VariableName $ UTF8.fromString $ string







---



data Error_translate_Expression token
  = Error_while_translate_SwitchCase (Error_translate_SwitchCase token)
  | Error_while_translate_Parenthesis (Error_translate_Expression token)
  | Error_while_translate_FoldedApplication (Error_AssumeVariableIsFunction token)
  | Error_while_translate_LambdaAbstraction (Error_translate_LambdaAbstraction token)
  | Error_while_translate_LetExpression (Error_translate_Expression token)
  deriving Show

translate_Expression
  :: Expression token
  -> Either (Error_translate_Expression token) FuchsProgram
translate_Expression (Expression_SwitchCase switchedCase)
  = mapLeft Error_while_translate_SwitchCase
  $ translate_SwitchCase switchedCase
translate_Expression (Expression_Parenthesis body)
  = mapLeft Error_while_translate_Parenthesis
  $ translate_Parenthesis body
translate_Expression (Expression_FoldedApplication body)
  = mapLeft Error_while_translate_FoldedApplication
  $ translate_FoldedApplication body
translate_Expression (Expression_LambdaAbstraction body)
  = mapLeft Error_while_translate_LambdaAbstraction
  $ translate_LambdaAbstraction body
translate_Expression (Expression_LetExpression body)
  = mapLeft Error_while_translate_LetExpression
  $ translate_LetExpression body




---




data Error_translate_SwitchCase token
  = Error_recursive_translate_Expression (Error_translate_Expression token)
  | Error_translateCaseMapping (Error_translateCaseMapping token)
  deriving Show

translate_SwitchCase
  :: DetailedSwitchCase token (Expression token)
  -> Either (Error_translate_SwitchCase token) (FuchsProgram)
translate_SwitchCase (DetailedSwitchCase scrutinee mappings) = do
  translated_scrutinee
    <- mapLeft Error_recursive_translate_Expression
    $ translate_Expression scrutinee
  translated_mappings
    <- mapM (mapLeft Error_translateCaseMapping . translateCaseMapping) mappings
  return $ Case translated_scrutinee translated_mappings (Trap (Variable (VariableName (UTF8.fromString $ "todo: no default case implemented: " ++ show translated_scrutinee))))


data Error_translateCaseMapping token
  = Error_recursive_translateCaseMapping (Error_translate_Expression token)
  | Error_Preimage (Error_UnexpectedVariableNamesInPatterns token)
  deriving Show

-- TODO use monad transformer to combine either with writer?
translateCaseMapping
  :: MappingFromTo token (Expression token)
  -> Either
      (Error_translateCaseMapping token)
      (ConstructorName, [VariableName], FuchsProgram)
translateCaseMapping (MappingFromTo foldedExpressions image)
  = do
    body
      <- mapLeft Error_recursive_translateCaseMapping
      $ translate_Expression image
    (constructor, variables)
      <- mapLeft Error_Preimage
      $ translate_preimage_to_pattern_match foldedExpressions
    return $ (constructor, variables, body)


data Error_UnexpectedVariableNamesInPatterns token
  = Error_ConstructorPart (Error_AssumeVariableIsConstructor token)
  | Error_VariableNamePart [Error_AssumeVariableIsFunction token]
  | Error_ConstructorPart_and_VariableNamePart (Error_AssumeVariableIsConstructor token) [Error_AssumeVariableIsFunction token]
  deriving Show

translate_preimage_to_pattern_match
  :: FoldedExpressions token
  -> Either (Error_UnexpectedVariableNamesInPatterns token) (ConstructorName, [VariableName])
translate_preimage_to_pattern_match
  (FoldedExpressions (ListOfFunctions listOfFunctions) _)
  = result
  where
    result = case maybeConstructor of
                  Right goodConstructor -> case badNames of
                                  [] -> Right $ (goodConstructor, goodNames)
                                  xs -> Left $ Error_VariableNamePart xs
                  Left badConstructor -> case badNames of
                                              [] -> Left $ Error_ConstructorPart badConstructor
                                              xs -> Left $ Error_ConstructorPart_and_VariableNamePart badConstructor xs
    the_head = head $ listOfFunctions --TODO change type to Data.List.NonEmpty

    the_tail = tail listOfFunctions

    maybeConstructor = assumeFunctionIsConstructor the_head
    maybeVariableNames = map assumeFunctionIsVariableName the_tail

    badNames = lefts maybeVariableNames
    goodNames = rights maybeVariableNames



data Error_AssumeVariableIsConstructor token
  = Error_ExpectedConstructorButGot String (FunctionName token)
  deriving Show

assumeFunctionIsConstructor
  :: FunctionName token
  -> Either (Error_AssumeVariableIsConstructor token) ConstructorName
assumeFunctionIsConstructor
  (Constructor (DetailedConstructorName string _ _))
  = Right $ ConstructorName $ UTF8.fromString $ string
assumeFunctionIsConstructor
  input@(Mapping (DetailedVariableName string _ _))
  = Left $ Error_ExpectedConstructorButGot string input


data Error_AssumeVariableIsFunction token
  = Error_ExpectedVariableButGot String (FunctionName token)
  deriving Show

-- TODO this probably makes no sense or is a hack:
--  VariableName vs Mapping
assumeFunctionIsVariableName
  :: FunctionName token
  -> Either (Error_AssumeVariableIsFunction token) VariableName
assumeFunctionIsVariableName
  (Mapping (DetailedVariableName string _ _))
  = Right $ VariableName $ UTF8.fromString $ string
assumeFunctionIsVariableName
  input@(Constructor (DetailedConstructorName string _ _))
  = Left $ Error_ExpectedVariableButGot string input




---





translate_Parenthesis
  :: BodyInParenthesis (Expression token)
  -> Either (Error_translate_Expression token) FuchsProgram
translate_Parenthesis (BodyInParenthesis expression)
  = translate_Expression expression




---




translate_FoldedApplication
  :: FoldedExpressions token
  -> Either (Error_AssumeVariableIsFunction token) FuchsProgram
translate_FoldedApplication (FoldedExpressions (ListOfFunctions listOfFunctions) _) = do
  appendages_translation
    <- mapM assumeFunctionIsVariableName the_tail

  let appendages = map Variable appendages_translation --TODO ???

  let head_translation
        = translate_to_VariableName_or_Constructor the_head

  return
    $ optimizeEmptyApplication
    $ case head_translation of
        Left constructorName
          -> SaturatedConstruction constructorName appendages
        Right variableName
          -> Apply (Variable variableName) appendages
  where
    the_head = head listOfFunctions
    the_tail = tail listOfFunctions

optimizeEmptyApplication :: FuchsProgram -> FuchsProgram
optimizeEmptyApplication (Apply function []) = function
optimizeEmptyApplication optimizationDoesNotApply = optimizationDoesNotApply


translate_to_VariableName_or_Constructor
  :: FunctionName token
  -> Either ConstructorName VariableName
translate_to_VariableName_or_Constructor
  (Mapping (DetailedVariableName string _ _))
  = Right $ VariableName $ UTF8.fromString $ string
translate_to_VariableName_or_Constructor
  (Constructor (DetailedConstructorName string _ _))
  = Left $ ConstructorName $ UTF8.fromString $ string



---




data Error_translate_LambdaAbstraction token
  = Error_recursive_translate_LambdaAbstraction (Error_translate_Expression token)
  | Error_VariableList (Error_AssumeVariableIsFunction token)
  deriving Show

translate_LambdaAbstraction
  :: LambdaAbstraction token (Expression token)
  -> Either
      (Error_translate_LambdaAbstraction token)
      FuchsProgram
translate_LambdaAbstraction
  (LambdaAbstraction (FoldedExpressions (ListOfFunctions arguments) _) body)
  = do
    translatedBody
      <- mapLeft Error_recursive_translate_LambdaAbstraction
      $ translate_Expression body
    argumentsAsVariables
      <- mapLeft Error_VariableList
      $ mapM assumeFunctionIsVariableName arguments

    let freeArgumentsAsVariables
         = extractFreeVariables translatedBody
           \\ argumentsAsVariables

    return $ Lambda freeArgumentsAsVariables argumentsAsVariables translatedBody

extractFreeVariables :: FuchsProgram -> [VariableName]
extractFreeVariables (Apply function arguments)
  = (function : arguments) >>= extractFreeVariables
extractFreeVariables (Case scrutinee cases defaultCase)
  = union free_in_scrutinee (union free_in_mappings free_in_defaultcase)
  where
    free_in_scrutinee = extractFreeVariables scrutinee
    free_in_defaultcase = extractFreeVariables defaultCase
    free_in_mappings = foldr union [] $ map find_free_in_mapping cases
    --TODO nub Eq vs Ord

    find_free_in_mapping :: (ConstructorName, [VariableName], FuchsProgram) -> [VariableName]
    find_free_in_mapping (_, boundings, image)
      = (extractFreeVariables image) \\ boundings --TODO Ord vs Eq
extractFreeVariables (Variable variableName) = [variableName]
extractFreeVariables (SaturatedConstruction _ programs)
  = foldr union []
  $ map extractFreeVariables programs

extractFreeVariables (Trap _) = []

-- TODO this function is all untested, and currently
extractFreeVariables (RecursiveLet bindings body) = result
  where
    free_in_body = extractFreeVariables body
    free_in_bindings = (map snd bindings) >>= extractFreeVariables
    bounded_in_bindings = map fst bindings

    free_in_bindings_without_recursive_bindings = free_in_bindings \\ bounded_in_bindings

    result = free_in_body \\ free_in_bindings_without_recursive_bindings

extractFreeVariables program
  = error
  $ "this case for free variables not implemented: "
  ++ show program



---




translate_LetExpression
  :: LetExpression token (Expression token) termError
  -> Either (Error_translate_Expression token) FuchsProgram
translate_LetExpression (LetExpression (AbstractDefinitionBlock definitions (Body returnExpression)) _)
  = do
    translated_body <- translate_Expression returnExpression
    definings <- mapM translate_Definition definitions
    return $ RecursiveLet definings translated_body
