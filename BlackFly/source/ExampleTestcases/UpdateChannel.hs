module ExampleTestcases.UpdateChannel where

import qualified Control.Monad.State as State
import Data.Either.Combinators
import Control.Monad.Trans.Except

import Trifle.Rename

import Compiler.CompileChain
import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Store.Tangle
import Store.Fuchs
import Evaluate.Gas
import Advance.Advance
import Advance.ActualUsingAdvance
import Advance.ProgramEncodedProgramCompiler
import ExampleFuchsProgramms.AsHaskellConstant.UpdateChannel
import ExampleFuchsProgramms.AsHaskellConstant.ToggleFlag
import Advance.ProgramEncodedStateCompiler
import Advance.AdvanceWithAdvanceIntrinsic


test_updatechannel :: ([Char], Bool)
test_updatechannel
  = (,) "test_updatechannel"
  $ and
  $ case maybe_result of
      Right bools -> bools
      Left errorCase -> error $ show errorCase
  where
    InterpretationResult maybe_result _
      = actuallyRunTheThing_withEmptyStore updateChannelTest
      :: InterpretationResult Error_UpdateChannelTest TangleStore [Bool]





---


foreign_button :: String -> BlockProgram
foreign_button methodName
  = BlockProgram
  $ Variable
  $ constructVariableName
  $ methodName

foreign_method_call :: String -> [FuchsProgram] -> BlockProgram
foreign_method_call methodName arguments
  = BlockProgram
  $ Apply method_of_state
  $ arguments
  where
    method_of_state
      = Variable
      $ constructVariableName
      $ methodName

foreign_call_initializeTheUpdateChannel
  :: OpaqueObject
  -> BlockProgram
foreign_call_initializeTheUpdateChannel (OpaqueObject genesis_state)
  = foreign_method_call "method_initializeTheUpdateChannel"
  $ [genesis_state]

foreign_call_commit :: ProgramEncodedBlockProgram -> BlockProgram
foreign_call_commit
  (ProgramEncodedBlockProgram (ProgramEncodedProgram diff))
  = foreign_method_call "method_commit"
  $ [diff]

foreign_event_toggle :: ProgramEncodedBlockProgram
foreign_event_toggle
  = encodeBlockProgramAsProgramEncodedBlockProgram
  $ foreign_button
  $ "method_toggle"


---


newtype ProgramEncodedBlockProgram
  = ProgramEncodedBlockProgram ProgramEncodedProgram

encodeBlockProgramAsProgramEncodedBlockProgram
  :: BlockProgram
  -> ProgramEncodedBlockProgram
encodeBlockProgramAsProgramEncodedBlockProgram
  (BlockProgram program)
  = ProgramEncodedBlockProgram
  $ programEncodeProgram
  $ program



---




data Error_UpdateChannelTest
  = Error_Initialize Error_Advance
  | Error_Commit Error_Advance
  | Error_DebugGetBack Error_WhileDestoringFuchsProgramState
  deriving Show

updateChannelTest :: ExceptT Error_UpdateChannelTest (State.State TangleStore) [Bool]
updateChannelTest = do
  uninitialized_updatechannel
    <- State.lift
    $ storeFuchsProgramState -- TODO storing is only nessessary when given to genie!?
    $ fromRight (error "updatechannel example does not compile") --TODO have generic compile method for the examples with automativ error message
    $ compile_fuchs_file
    $ updatechannel

  let toggle_genesis_state = id
        $ mark_state_as_opaque_object_state
        $ fromRight (error "toggleflag example does not compile")
        $ compile_fuchs_file
        $ toggleflag

  {- initial state -}
  AdvanceResult initialized_updateChannel _
    <- mapException Error_Initialize
    $ makeAdvance uninitialized_updatechannel
    $ foreign_call_initializeTheUpdateChannel
    $ toggle_genesis_state

  let turn_on_the_light_command
        = foreign_call_commit
        $ foreign_event_toggle

  {- first commit -}
  AdvanceResult first_commit_receipt _
    <- mapException Error_Commit
    $ makeAdvance initialized_updateChannel
    $ turn_on_the_light_command

  _thingToBeShown
    <- mapException Error_DebugGetBack
    $ destoreFuchsProgramState
    $ first_commit_receipt

--   _ <- error $ "state after first commit: \n" ++ uglyPrintState thingToBeShown

  return [True]
  where
    gas = Gas $ 22
    gases = Gases (GasForChecking gas) (GasForAdvancing gas)
    makeAdvance = advance current_standard_intrinsics gases
