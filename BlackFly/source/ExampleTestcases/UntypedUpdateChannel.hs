module ExampleTestcases.UntypedUpdateChannel where

import qualified Control.Monad.State as State
import Data.Either.Combinators
import Control.Monad.Trans.Except

import Trifle.Rename

import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Store.Tangle
import Store.Fuchs
import Evaluate.Gas
import Evaluate.Evaluation
import Advance.Advance
import Advance.ActualUsingAdvance
import Compiler.CompileChain
import ExampleFuchsProgramms.AsHaskellConstant.UntypedUpdateChannel


test_untypedupdatechannel :: ([Char], Bool)
test_untypedupdatechannel
  = (,) "test_untypedupdatechannel"
  $ and
  $ case maybeOutput of
      Right bools -> bools
      Left errorCase -> error $ show errorCase
  where
    InterpretationResult maybeOutput _
      = actuallyRunTheThing_withEmptyStore untypedUpdateChannelTest
      :: InterpretationResult
            Error_UpdateChannelTest
            TangleStore
            [Bool]





--

method_initializeState :: FuchsProgram
method_initializeState
  = Variable
  $ constructVariableName
  $ "method_initializeTheUpdateChannel"

initialStateExample :: ConstructorName
initialStateExample
  = constructConstructorName
  $ "InitialStateExample"

attachment :: FuchsProgram
attachment = SaturatedConstruction (constructConstructorName $ "InitialCommitExample attachment") []

block_initialize :: BlockProgram
block_initialize
  = BlockProgram
  $ Apply method_initializeState [SaturatedConstruction initialStateExample [attachment]]



--


method_commit :: FuchsProgram
method_commit
  = Variable
  $ constructVariableName
  $ "method_commit"

firstCommitExample :: ConstructorName
firstCommitExample
  = constructConstructorName
  $ "FirstCommitExample"

block_commit1 :: BlockProgram
block_commit1
  = BlockProgram
  $ Apply method_commit [SaturatedConstruction firstCommitExample []]

block_commit2 :: BlockProgram
block_commit2
  = BlockProgram
  $ Apply
      method_commit
      [SaturatedConstruction firstCommitExample [block_commit1', block_commit1', block_commit1']]
  where
    BlockProgram block_commit1' = block_commit1


--





data Error_UpdateChannelTest
  = Error_while_initializing Error_Advance
  | Error_CouldInitializeTwice (Error_FlipedCondition AdvanceResult)
  | Error_WasSupposedToFailWhenCommitWithoutInitializiation (Error_FlipedCondition AdvanceResult)
  | Error_while_first_commit Error_Advance
  | Error_while_second_commit Error_Advance
  deriving Show

untypedUpdateChannelTest :: ExceptT Error_UpdateChannelTest (State.State TangleStore) [Bool]
untypedUpdateChannelTest = do
  uninitialized_updatechannel_state
    <- State.lift
    $ storeFuchsProgramState
    $ fromRight'
    $ compile_fuchs_file
    $ untypedupdatechannel

  AdvanceResult initialized_updatechannel _
    <- mapException Error_while_initializing
    $ makeAdvance uninitialized_updatechannel_state
    $ block_initialize

  _cannot_initialize_twice
    <- mapException Error_CouldInitializeTwice
    $ flipErrorCondition
    $ makeAdvance initialized_updatechannel
    $ block_initialize

  AdvanceResult updatechannel_with_one_commits _systemCall_or_ReturnValue
    <- mapException Error_while_first_commit
    $ makeAdvance initialized_updatechannel
    $ block_commit1

  _cannot_commit_without_initialize
    <- mapException Error_WasSupposedToFailWhenCommitWithoutInitializiation
    $ flipErrorCondition
    $ makeAdvance uninitialized_updatechannel_state
    $ block_commit1

  _updatechannel_with_two_commits
    <- mapException Error_while_second_commit
    $ makeAdvance updatechannel_with_one_commits
    $ block_commit2

  return [True]
  where
    gases = Gases (GasForChecking $ Gas 10) (GasForAdvancing $ Gas 10)
    makeAdvance before_state block = advance emptyIntrinsicEnvironment gases before_state block








---





data Error_FlipedCondition erroneousSuccess
  = Error_FlipedCondition erroneousSuccess
  deriving Show

flipErrorCondition
  :: ExceptT
        errorCase
        (State.State TangleStore)
        erroneousSuccess
  -> ExceptT
        (Error_FlipedCondition erroneousSuccess)
        (State.State TangleStore)
        errorCase
flipErrorCondition supposedToFail = do
  errorMessage <- tryExceptT supposedToFail
  case errorMessage of
       Left errorCase
         -> return errorCase
       Right erroneousState
         -> throwException $ Error_FlipedCondition $ erroneousState
