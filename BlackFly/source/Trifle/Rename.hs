module Trifle.Rename where

import Control.Monad

import Control.Monad.Trans.Except


mapException :: Functor m => (e -> e') -> ExceptT e m a -> ExceptT e' m a
mapException = withExceptT

throwException :: Monad m => e -> ExceptT e m a
throwException = throwE

-- TODO library?
tryExceptT :: Monad m => ExceptT e m a -> ExceptT e2 m (Either e a)
tryExceptT m = catchE (liftM Right m) (return . Left)

-- tryOut :: Monad m => ExceptT e m a -> ExceptT e m (Either e a)
-- tryOut = tryE
