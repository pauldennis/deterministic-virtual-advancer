module Trifle.Missing where

import Data.Containers.ListUtils
import Data.List.Extra hiding (nubOrd)
import Safe.Partial
import GHC.Stack

import Trifle.Patchwork

isNubbed :: Ord a => [a] -> Bool
isNubbed list_with_maybe_duplicates = result
  where
    result
      = (==) (trivialize nubbedList)
      (trivialize list_with_maybe_duplicates)

    nubbedList = nubOrd list_with_maybe_duplicates

    trivialize = fmap $ const ()

duplicateExample :: (Partial, Ord a) => [a] -> a
duplicateExample list_with_duplicates
  = withFrozenCallStack
  $ head
  $ head
  $ filter (not . isSingleton)
  $ group
  $ sort
  $ list_with_duplicates

