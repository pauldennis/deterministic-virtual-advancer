module Trifle.Test where

import System.Exit

-- TODO use or not using?
type Test = (String, Bool)

runTestCase :: (String, Bool) -> IO ()
runTestCase (name, testresult)
  = do
    putStr name
    putStrLn ": ..."
    if testresult
     then putStrLn $ "  TEST SUCCESS"
     else do
       putStrLn $ "  TEST FAILURE"
       exitWith (ExitFailure 1)

