module Trifle.Patchwork where

--https://hackage.haskell.org/package/ghc-lib-parser-9.2.3.20220527/docs/GHC-Utils-Misc.html#v:isSingleton
isSingleton :: [a] -> Bool
isSingleton [_] = True
isSingleton _   = False

