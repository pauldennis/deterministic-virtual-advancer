module RuntimeUsageBindings.ConnectivityDriverBindings where

import qualified Control.Monad.State as State

import ExampleFuchsProgramms.AsHaskellConstant.ConnectivityDriver
import RuntimeUsageBindings.OutmostApplicationState
import Store.Fuchs
import Store.Tangle
import Compiler.CompileChain
import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt


initial_state_connectivitydriver_example :: OutmostApplicationState
initial_state_connectivitydriver_example
  = uncurry OutmostApplicationState
  $ State.runState initial_store_connectivitydriver_example emptyTangleStore


initial_store_connectivitydriver_example :: State.State TangleStore StateReceipt
initial_store_connectivitydriver_example = do
  the_genesis_state
    <- storeFuchsProgramState
    $ either (\x -> error $ "program does not compile: " ++ show x) id
    $ compile_fuchs_file
    $ connectivitydriver

  return the_genesis_state
