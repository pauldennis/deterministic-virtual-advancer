module RuntimeUsageBindings.OutmostApplicationState where

import Store.Tangle
import Store.Fuchs

data OutmostApplicationState
  = OutmostApplicationState StateReceipt TangleStore
