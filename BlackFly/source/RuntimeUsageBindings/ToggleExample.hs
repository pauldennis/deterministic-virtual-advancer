module RuntimeUsageBindings.ToggleExample where

import qualified Control.Monad.State as State

import Data.Either.Combinators
import Control.Monad.Trans.Except
import Data.IORef


import Trifle.Rename
import ExampleFuchsProgramms.AsHaskellConstant.ToggleFlag
import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Store.Tangle
import Store.Fuchs
import Evaluate.Gas
import Evaluate.Evaluation
import Advance.Advance
import Advance.ActualUsingAdvance
import Compiler.CompileChain

import RuntimeUsageBindings.OutmostApplicationState





initial_state_toggle_example :: OutmostApplicationState
initial_state_toggle_example
  = uncurry OutmostApplicationState
  $ State.runState initial_store_toggle_example emptyTangleStore

initial_store_toggle_example :: State.State TangleStore StateReceipt
initial_store_toggle_example = do
  the_genesis_state
    <- storeFuchsProgramState
    $ fromRight'
    $ compile_fuchs_file
    $ toggleflag

  return the_genesis_state



---



block_toggle :: BlockProgram
block_toggle
  = BlockProgram
  $ Variable
  $ constructVariableName
  $ "method_toggle"

max_gas_toggle_example :: Gas
max_gas_toggle_example = Gas 20

max_gases_toggle_example :: Gases
max_gases_toggle_example
  = Gases
      (GasForChecking max_gas_toggle_example)
      (GasForAdvancing max_gas_toggle_example)


data ProperRuntimeError_ToggleExample
  = Error_WhileToggle Error_Advance
  deriving Show

advanceStateWithBlock
  :: FuchsProgram
  -> StateReceipt
  -> ExceptT
        ProperRuntimeError_ToggleExample
        (State.State TangleStore)
        AdvanceResult
advanceStateWithBlock block before_state
  = mapException Error_WhileToggle
  $ advance emptyIntrinsicEnvironment max_gases_toggle_example before_state
  $ (BlockProgram block)

event_toggle
  :: StateReceipt
  -> ExceptT Error_Advance (State.State TangleStore) AdvanceResult
event_toggle before_state
  = advance
      emptyIntrinsicEnvironment
      max_gases_toggle_example
      before_state
      block_toggle



data EventResult
  = EventResult OutmostApplicationState SystemCall_or_ReturnValue

data Error_ToggleEvent
  = Error_ToggleEvent Error_Advance
  deriving Show

toggle
  :: OutmostApplicationState
  -> Either Error_ToggleEvent EventResult
toggle (OutmostApplicationState root tangleStore)
  = result
  where
    result
      = case advanceResult of
          Right (AdvanceResult newRoot systemCall_or_ReturnValue)
            -> Right $ EventResult (OutmostApplicationState newRoot newTangleStore) systemCall_or_ReturnValue
          Left errorCase -> Left $ Error_ToggleEvent errorCase

    InterpretationResult advanceResult newTangleStore
      = actuallyRunTheThing (event_toggle root) tangleStore
      :: InterpretationResult Error_Advance TangleStore AdvanceResult



---



data ToggleSystemCall
  = WantsToSet
  | WantsToClear

--TODO where to properly put the constants
bitIsSet :: String
bitIsSet = "BitIsSet"
bitIsCleared :: String
bitIsCleared = "BitIsCleared"


data Error_ParsingSystemCall_Toggle
  = Error_notNullaryConstructor FuchsProgram
  | Error_isNeither_x_not_y String String ConstructorName
  deriving Show

parseSystemcall_2
  :: FuchsProgram
  -> Either Error_ParsingSystemCall_Toggle ToggleSystemCall
parseSystemcall_2 (SaturatedConstruction constructorName [])
  = parseSystemcall_3 constructorName
parseSystemcall_2 x = Left $ Error_notNullaryConstructor x

parseSystemcall_3
  :: ConstructorName
  -> Either Error_ParsingSystemCall_Toggle ToggleSystemCall
parseSystemcall_3 constructorName
  = if constructorName == constructConstructorName bitIsSet
      then Right WantsToSet
      else if constructorName == constructConstructorName bitIsCleared
              then Right WantsToClear
              else Left $ Error_isNeither_x_not_y bitIsSet bitIsCleared constructorName




-----



data GlobalAdvanceResult
  = GlobalAdvanceResult
      TangleStore
      SystemCall_or_ReturnValue

advance_app_state_by_event
  :: IORef OutmostApplicationState
  -> IO GlobalAdvanceResult
advance_app_state_by_event rolling_state
  = atomicModifyIORef' rolling_state modify
  where
    modify
      :: OutmostApplicationState
      -> (OutmostApplicationState, GlobalAdvanceResult)
    modify state
      = id
      $ tupleUp
      $ failOnLeft
      $ toggle
      $ state

    tupleUp
      :: EventResult
      -> (OutmostApplicationState, GlobalAdvanceResult)
    tupleUp (EventResult x@(OutmostApplicationState _stateReceipt tangleStore) y) = (x, GlobalAdvanceResult tangleStore y)

    failOnLeft
      :: Either Error_ToggleEvent EventResult
      -> EventResult
    failOnLeft = either errorOut id

    errorOut
      :: Error_ToggleEvent
      -> EventResult
    errorOut errorCase
      = error
      $ "too bad, runtime error: "
      ++ show errorCase




interpretSystemCall
  :: (ToggleSystemCall -> IO ())
  -> (FuchsProgram -> Either Error_ParsingSystemCall_Toggle ToggleSystemCall)
  -> TangleStore
  -> SystemCall_or_ReturnValue
  -> IO ()
interpretSystemCall
  executionOfSystemCallRequest
  systemCallParser
  tangleStore
  (SystemCall_or_ReturnValue systemCall_or_ReturnValue)
  = executionOfSystemCallRequest
  $ either (\x -> error $ "systemcall attempt not recognised: " ++ show x) id
  $ systemCallParser
  $ either (\x -> error $ "error while destoring systemcall: " ++ show x) id
  $ getInterpretationResultOutcome
  $ actuallyRunTheThing (destoreProgram systemCall_or_ReturnValue)
  $ tangleStore



callback_toggle
  :: (ToggleSystemCall -> IO ())
  -> IORef OutmostApplicationState
  -> IO ()
callback_toggle executeSystemCall rolling_state
  = do

  GlobalAdvanceResult tangleStore systemCall_or_ReturnValue
    <- advance_app_state_by_event rolling_state

  interpretSystemCall
    executeSystemCall
    parseSystemcall_2
    tangleStore
    systemCall_or_ReturnValue
