module LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State where

import Store.Fuchs

-- TODO place?
data FuchsProgram
  = REMOVEMELATEr --TODO

  | Apply
      FuchsProgram
      [FuchsProgram]

  | SaturatedConstruction
      ConstructorName
      [FuchsProgram]

  | Case
      FuchsProgram
      [(ConstructorName, [VariableName], FuchsProgram)]
      FuchsProgram
      -- TODO wrap last one that is default case

  | Lambda
      [VariableName]
      [VariableName]
      FuchsProgram
      -- free_variables, introduced_variables, body

  | StrictIntrinsic
      VariableName

  | Variable
      VariableName

  | RecursiveLet
      [(VariableName, FuchsProgram)]
      FuchsProgram
      -- TODO replace Tuple with named type: "data EnvironmentBind = EnvironmentBind VariableName FuchsProgram"

  | Trap
      FuchsProgram

  | OpaqueNativeObject
      FuchsProgramState

  deriving (Eq, Show)

nullaryConstructor :: String -> FuchsProgram
nullaryConstructor string = SaturatedConstruction (constructConstructorName string) []

naryConstructor :: String -> [FuchsProgram] -> FuchsProgram
naryConstructor string children = SaturatedConstruction  (constructConstructorName string) children

variable_FuchsProgram :: String -> FuchsProgram
variable_FuchsProgram string = Variable $ constructVariableName string

trap_FuchsProgram :: String -> FuchsProgram
trap_FuchsProgram string = Trap $ nullaryConstructor string



---


-- TODO collect all names and choose some more
data FuchsProgramState
  = FuchsProgramState [(VariableName, FuchsProgram)]
  deriving (Show, Eq)

lookupInFuchsProgramState
  :: VariableName
  -> FuchsProgramState
  -> Maybe FuchsProgram

lookupInFuchsProgramState preimage (FuchsProgramState mapping) = lookup preimage mapping




---




programExample :: FuchsProgram
programExample = Case examinee alternatives alternativeCase
  where
    examinee = SaturatedConstruction (constructConstructorName "AAA") []
    alternatives = tail [ undefined
      , (constructConstructorName "AAA", [], nullaryConstructor "Option_1")
      , (constructConstructorName "BBB", [constructVariableName "x", constructVariableName "y"], option_2)
      , (constructConstructorName "CCC", [], option_3)
      , (constructConstructorName "DDD", [], trap_FuchsProgram "error DDD")
      ]
    alternativeCase = SaturatedConstruction (constructConstructorName "default_1") []

    option_2 = Lambda [] [constructVariableName "x"] (SaturatedConstruction (constructConstructorName "begin of lambda body") [Variable (constructVariableName "x")])

    fibonacciName = "fibonacci"
    option_3 = RecursiveLet [(constructVariableName fibonacciName, fibonacci)] (Variable $ constructVariableName fibonacciName)
    fibonacci = Lambda [] [constructVariableName "x"] $ Case (Variable $ constructVariableName "x")
      [ (constructConstructorName "0", [], nullaryConstructor "0")
      , (constructConstructorName "1", [], nullaryConstructor "1")
      ]
      (Apply function arguments)
      where
        function = Lambda [] [constructVariableName "x"] (Variable $ constructVariableName "f")
        arguments = [nullaryConstructor "DefaultValue", nullaryConstructor "Second Argument"]

fibonacciFunction :: FuchsProgram
fibonacciFunction = RecursiveLet [(constructVariableName "fibonacci", actualFunction)] (Variable $ constructVariableName "fibonacci")
  where
    actualFunction = Lambda [] [constructVariableName "x"] $ Case (Variable $ constructVariableName "x")
      [ (constructConstructorName "0", [], nullaryConstructor "0")
      , (constructConstructorName "1", [], nullaryConstructor "1")
      ]
      (nullaryConstructor "f(n-2)+f(n-1)")

