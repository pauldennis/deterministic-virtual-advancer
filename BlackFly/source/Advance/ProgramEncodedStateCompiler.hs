module Advance.ProgramEncodedStateCompiler where

import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State



newtype OpaqueObject = OpaqueObject FuchsProgram
  deriving Show

mark_state_as_opaque_object_state
  :: FuchsProgramState
  -> OpaqueObject
mark_state_as_opaque_object_state
  (FuchsProgramState bindings)
  = result
  where
    result
      = OpaqueObject
      $ OpaqueNativeObject
      $ FuchsProgramState
      $ bindings

