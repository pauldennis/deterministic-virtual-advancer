module Advance.EncodingAndDecoding.Squashing where

import Data.Either.Combinators

import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Store.Fuchs
import Advance.EncodingAndDecoding.VariableName

-- TODO use same definition for decoding and encoding
magic__apply :: [Char]
magic__apply = "§Apply"

magic__variable :: [Char]
magic__variable = "§Variable"


-- from the outside i only want simple but extensible events. For example whole lambdas can be submitted. From the inside I have to be able to do the same thing. Therefore there is

data Error_SquashingFuchsProgram
  = Error_UnrecognisedIndicator_x_expected_one_of_x_
      FuchsProgram
      [String]
  | Error_ExpectedSaturatedConstruction_but_got_x_ FuchsProgram
  | Error_WhileDecodingApplyChildren Error_ParseApplyChildren
  | Error_WhileDecodingVariableChildren Error_ParseChildrenOfVariable
  deriving Show

squashProgramEncodedProgram
  :: FuchsProgram
  -> Either Error_SquashingFuchsProgram FuchsProgram
squashProgramEncodedProgram
  input@(SaturatedConstruction constructorName children)
  = result
  where
    indication__apply = constructConstructorName magic__apply
    indication__variable = constructConstructorName magic__variable

    -- TODO how to make sure we got all cases here?
    all_indicators
      = Prelude.tail
      $ [ undefined
      , magic__apply
      , magic__variable
      ]

    --TODO refactor out children?
    result
      | constructorName == indication__apply = squash_children_apply
      | constructorName == indication__variable = squash_children__variable
      | otherwise = Left $ Error_UnrecognisedIndicator_x_expected_one_of_x_ input all_indicators

    squash_children_apply
      :: Either Error_SquashingFuchsProgram FuchsProgram
    squash_children_apply
      = mapLeft Error_WhileDecodingApplyChildren
      $ fmap (\variable -> Apply variable [])
      $ squashEncodedProgramTerm__Apply_Children
      $ children

    squash_children__variable
      :: Either Error_SquashingFuchsProgram FuchsProgram
    squash_children__variable
      = fmap Variable
      $ mapLeft Error_WhileDecodingVariableChildren
      $ squashEncodedProgramTerm__Children_of_Variable
      $ children


squashProgramEncodedProgram unmatched_program
  = Left $ Error_ExpectedSaturatedConstruction_but_got_x_ unmatched_program



---

data Error_ParseChildrenOfVariable
  = Error_VariableNameNotSupposedToHaveChildren FuchsProgram [FuchsProgram]
  | Error_NotSupposedToHaveMoreOrLessOneChild [FuchsProgram]
  | Error_WhileParsingVariableName Error_EncodedVariableName
  deriving Show

squashEncodedProgramTerm__Children_of_Variable
  :: [FuchsProgram]
  -> Either Error_ParseChildrenOfVariable VariableName
squashEncodedProgramTerm__Children_of_Variable
  [SaturatedConstruction encodedVariableName []]
  = mapLeft Error_WhileParsingVariableName
  $ parseProgramEncodedVariableName
  $ encodedVariableName
squashEncodedProgramTerm__Children_of_Variable
  [only_child@(SaturatedConstruction _encodedVariableName children)]
  = Left $ Error_VariableNameNotSupposedToHaveChildren only_child children
squashEncodedProgramTerm__Children_of_Variable
  children
  = Left $ Error_NotSupposedToHaveMoreOrLessOneChild children





-- TODO Error_WhileParsingVariableName2
data Error_ParseApplyChildren
  = Error_ExpectedExactlyOneChild [FuchsProgram]
  | Error_ExpectedConstructor_but_got_x_ FuchsProgram
  | Error_ExpectedMagicIndicator_x_but_got_x ConstructorName ConstructorName
  | Error_WhileParsingVariableName2 Error_EncodedVariableName
  | Error_ExpectedSingleNullaryConstructorButGot_x [FuchsProgram]
  deriving Show

squashEncodedProgramTerm__Apply_Children
  :: [FuchsProgram]
  -> Either Error_ParseApplyChildren FuchsProgram
squashEncodedProgramTerm__Apply_Children
  [_only_child@(SaturatedConstruction constructorName [SaturatedConstruction child []])]
  = result
  where
    variable_indication = constructConstructorName magic__variable

    result
      = if variable_indication == constructorName
           then mapRight Variable
            $ mapLeft Error_WhileParsingVariableName2
            $ parseProgramEncodedVariableName
            $ child
           else Left
             $ Error_ExpectedMagicIndicator_x_but_got_x variable_indication constructorName
squashEncodedProgramTerm__Apply_Children
  [_only_child@(SaturatedConstruction _constructorName children)]
  = Left $ Error_ExpectedSingleNullaryConstructorButGot_x children
squashEncodedProgramTerm__Apply_Children [unrecognized_only_child]
  = Left $ Error_ExpectedConstructor_but_got_x_ unrecognized_only_child
squashEncodedProgramTerm__Apply_Children unmatched_children
  = Left $ Error_ExpectedExactlyOneChild unmatched_children

