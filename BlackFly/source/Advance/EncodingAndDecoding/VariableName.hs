module Advance.EncodingAndDecoding.VariableName where

import Data.Word
import Data.ByteString

import Advance.EncodingMagic
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Store.Fuchs



magicallyEncodeVariableNameAsMagiciallyEncodedConstructor
  :: VariableName
  -> FuchsProgram
magicallyEncodeVariableNameAsMagiciallyEncodedConstructor (VariableName byteString) = result
  where
    result = SaturatedConstruction magically_encoded_Constructor []
    magically_encoded_Constructor = ConstructorName magic_bytes
    magic_bytes = makeByteSequenceMagicForConstructor byteString




data Error_EncodedVariableName
  = Error_MagixPrefix_x_Expected_but_got_x_with_rest_x (Word8, Word8) (Word8, Word8) ByteString
  | Error_ExpectedAtLeastTwoBytes ByteString
  deriving Show

-- TODO type unessesary complicated?
-- TODO central utility to add and remove the magic prefix
parseProgramEncodedVariableName
  :: ConstructorName
  -> Either Error_EncodedVariableName VariableName
parseProgramEncodedVariableName (ConstructorName userVariableName)
  = do
      let maybeFirstTwoBytes = do
            (first, second_rest) <- uncons userVariableName
            (second, rest) <- uncons second_rest
            return (first, second, rest)
      case maybeFirstTwoBytes of
        Just (first, second, rest) -> do
          if (first, second) == buildInMeaning_prefix_as_twobytes
             then return $ VariableName rest
             else Left $ Error_MagixPrefix_x_Expected_but_got_x_with_rest_x (first, second) buildInMeaning_prefix_as_twobytes rest
        Nothing -> Left $ Error_ExpectedAtLeastTwoBytes userVariableName
