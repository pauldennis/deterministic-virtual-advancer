module Advance.AdvanceTest where

import Data.Either.Combinators
import Control.Monad.Trans.Except

import qualified Control.Monad.State as State

import Trifle.Rename

import ExampleFuchsProgramms.AsHaskellConstant.ToggleFlag
import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Store.Tangle
import Store.Fuchs
import Evaluate.Gas
import Evaluate.Evaluation
import Advance.Advance
import Advance.ActualUsingAdvance
import Compiler.CompileChain


data AdvancedTest_Result
  = AdvancedTest_Result
  deriving Show

data Error_AdvancedTest
  = Error_in_underlying_FuchsStore Error_FuchsStore
  | Error_whileAdvancing Error_Advance
  | Error_WhileDestore Error_WhileDestoringFuchsProgramState
  deriving Show

advance_test :: ExceptT Error_AdvancedTest (State.State TangleStore) [Bool]
advance_test = do
  the_genesis_state
    <- State.lift
    $ storeFuchsProgramState
    $ fromRight'
    $ compile_fuchs_file
    $ toggleflag

  AdvanceResult the_new_state _ <- toggleSwitch the_genesis_state
  AdvanceResult the_same_state_as_the_genesis_state _ <- toggleSwitch the_new_state

  let repetitions = 1
        * 4 -- currently takes quite a long time
        * 2 -- always switch back to inital state
        :: Integer

  flickered_state <- nTimes repetitions toggleSwitch the_genesis_state

  supposedToFail
    <- tryExceptT
    $ fail_to_call_method
    $ the_genesis_state

  return $
    [ True
    , the_genesis_state /= the_new_state
    , the_genesis_state == the_same_state_as_the_genesis_state
    , flickered_state == the_genesis_state
    , flickered_state /= the_new_state
    , isLeft supposedToFail
    ]
  where
    the_block = Variable $ constructVariableName $ "method_toggle"
    invalid_block = Variable $ constructVariableName $ "not existing variable name"
    gas = Gas 15
    gases = Gases (GasForChecking gas) (GasForAdvancing gas)

    advanceStateWithBlock block before_state
      = mapException Error_whileAdvancing
      $ advance emptyIntrinsicEnvironment gases before_state
      $ (BlockProgram block)

    toggleSwitch
      :: StateReceipt
      -> ExceptT Error_AdvancedTest (State.State TangleStore) AdvanceResult
    toggleSwitch = advanceStateWithBlock the_block

    fail_to_call_method = advanceStateWithBlock invalid_block

    nTimes 0 _ x = return x
    nTimes n f x = do
      AdvanceResult a _ <- f x
      (nTimes (n-1) f) a


run_advance_test
  :: InterpretationResult
      Error_AdvancedTest
      TangleStore
      [Bool]
run_advance_test
  = actuallyRunTheThing advance_test emptyTangleStore

test_toggle_switch :: ([Char], Bool)
test_toggle_switch
  = (,) "test_toggle_switch"
  $ and
  $ case getInterpretationResultOutcome run_advance_test of
      Right bools -> bools
      Left errorCase -> error $ show errorCase
