module Advance.ProgramEncodedProgramCompiler where

import Data.Either.Combinators
import qualified Control.Monad.State as State
import qualified Data.ByteString.UTF8 as UTF8
import Data.ByteString
import Control.Monad.Trans.Except

import Trifle.Rename
import Advance.EncodingMagic
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt
import Store.Tangle
import Store.Fuchs
import Evaluate.Environment

import Advance.EncodingAndDecoding.VariableName


newtype FuchsEncodedFuchs_Receipt
  = FuchsEncodedFuchs_Receipt FuchsReceipt

-- TODO somehow encodeProgramAsSaturatedConstruction and squashProgramEncodedProgram seem to belong to eachother and somehow they are of quite different concerns, does this have some meaning?

-- used for isValid
compileProgramEncodedProgram
  :: FuchsProgram
  -> State.State TangleStore FuchsEncodedFuchs_Receipt
compileProgramEncodedProgram program
  = do
  programEncodedProgram <- storeProgram saturatedConstruction
  return
    $ FuchsEncodedFuchs_Receipt
    $ programEncodedProgram
  where
    ProgramEncodedProgram saturatedConstruction
      = programEncodeProgram program



encodedCase :: FuchsProgram -> ConstructorName
encodedCase = constructConstructorName . magicEncodingString_of_case_of_FuchsProgram

newtype ProgramEncodedProgram
  = ProgramEncodedProgram FuchsProgram

programEncodeProgram
  :: FuchsProgram
  -> ProgramEncodedProgram
programEncodeProgram
  = ProgramEncodedProgram
  . encodeProgramAsSaturatedConstruction

encodeProgramAsSaturatedConstruction
  :: FuchsProgram
  -> FuchsProgram
encodeProgramAsSaturatedConstruction
  variable@(Variable (VariableName variableName))
  = result
  where
    magicEncodingIndicator = encodedCase variable

    adhoc_encoded_ByteString
      = (\x -> SaturatedConstruction x [])
      $ ConstructorName
      $ makeByteSequenceMagicForConstructor
      $ variableName

    result = SaturatedConstruction magicEncodingIndicator [adhoc_encoded_ByteString]


encodeProgramAsSaturatedConstruction apply@(Apply function arguments)
  = result
  where
    magicEncodingIndicator = encodedCase apply

    recursivly_encoded_function = encodeProgramAsSaturatedConstruction function
    recursivly_encoded_arguments = Prelude.map encodeProgramAsSaturatedConstruction arguments

    result
      = SaturatedConstruction magicEncodingIndicator
      $ [ recursivly_encoded_function]
      ++ recursivly_encoded_arguments


encodeProgramAsSaturatedConstruction lambda@(Lambda _globals arguments body)
  = result
  where
    magicEncodingIndicator = encodedCase lambda

    encodedArguments = id
      $ SaturatedConstruction argumentsIndicator
      $ Prelude.map magicallyEncodeVariableNameAsMagiciallyEncodedConstructor
      $ arguments

    argumentsIndicator
      = promoteConstructorToMagicConstructor
      $ constructConstructorName
      $ magicStringForLambdaArguments

    encodedBody = encodeProgramAsSaturatedConstruction body

    result = SaturatedConstruction magicEncodingIndicator [encodedArguments, encodedBody]


encodeProgramAsSaturatedConstruction casecase@(Case scrutinee alternatives defaultCase)
  = result
  where
    magicEncodingIndicator = encodedCase casecase

    encodedScrutinee = encodeProgramAsSaturatedConstruction scrutinee
    encodedAlternatives = Prelude.map encodeAlternative alternatives
    encodedDefaultCase = encodeProgramAsSaturatedConstruction defaultCase

    result
      = SaturatedConstruction magicEncodingIndicator
      $ [encodedScrutinee]
      ++ encodedAlternatives
      ++ [encodedDefaultCase]

    encodeAlternative
      :: (ConstructorName, [VariableName], FuchsProgram)
      -> FuchsProgram
    encodeAlternative (constructorName, variableNames, path) = alternative_result
      where
        argumentsIndicator
          = promoteConstructorToMagicConstructor
          $ constructConstructorName
          $ magicStringForCaseAlternative

        encodedConstructorName
          = (\x -> SaturatedConstruction x [])
          $ promoteConstructorToMagicConstructor
          $ constructorName

        encodedVariableNames = Prelude.map magicallyEncodeVariableNameAsMagiciallyEncodedConstructor variableNames

        encodedPath = encodeProgramAsSaturatedConstruction path

        alternative_result
          = SaturatedConstruction argumentsIndicator
          $ [encodedConstructorName]
          ++ encodedVariableNames
          ++ [encodedPath]


encodeProgramAsSaturatedConstruction saturatedConstruction@(SaturatedConstruction constructorName attachments)
  = result
  where
    magicEncodingIndicator = encodedCase saturatedConstruction

    encodedConstructor
      = (\x -> SaturatedConstruction x []) --TODO should refactor out?
      $ promoteConstructorToMagicConstructor
      $ constructorName

    recursiveEncodedThings
      = Prelude.map encodeProgramAsSaturatedConstruction
      $ attachments

    result = SaturatedConstruction magicEncodingIndicator
      $ [encodedConstructor]
      ++ recursiveEncodedThings


encodeProgramAsSaturatedConstruction trap@(Trap body)
  = result
  where
    magicEncodingIndicator = encodedCase trap

    encodedBody = encodeProgramAsSaturatedConstruction body

    result = SaturatedConstruction magicEncodingIndicator
      $ [encodedBody]


encodeProgramAsSaturatedConstruction recursiveLet@(RecursiveLet bindings body)
  = result
  where
    magicEncodingIndicator = encodedCase recursiveLet

    encodedBody = encodeProgramAsSaturatedConstruction body

    encodedVariableNames
      = Prelude.map magicallyEncodeVariableNameAsMagiciallyEncodedConstructor
      $ Prelude.map fst
      $ bindings

    encodedProgramms
      = Prelude.map encodeProgramAsSaturatedConstruction
      $ Prelude.map snd
      $ bindings

    encodedBindings = Prelude.zipWith bind encodedVariableNames encodedProgramms

    bind
      :: FuchsProgram
      -> FuchsProgram
      -> FuchsProgram
    bind left right
      = (\magic_Bind -> SaturatedConstruction magic_Bind [left, right])
      $ promoteConstructorToMagicConstructor
      $ constructConstructorName
      $ magicStringForEncodedBind

    result
      = SaturatedConstruction magicEncodingIndicator
      $ encodedBindings
      ++ [encodedBody]


encodeProgramAsSaturatedConstruction
  opaqueNativeObject@(OpaqueNativeObject _bindings)
  = result
  where
    result = SaturatedConstruction magicEncodingIndicator []

    magicEncodingIndicator = encodedCase opaqueNativeObject



encodeProgramAsSaturatedConstruction unimplementedCase = error $ "the encoding of this case is not implemented: " ++ show unimplementedCase



---





---


---








data Error_WhileSquashingProgramEncodedState
  = Error_WhileDestoringSupposedlyProgramEncodedBindings Error_FuchsStore
  | Error_ExpectedMagicToken_x_ForAdvanceResultButGot_x_ ByteString ByteString
  | Error_ExpectedConstructorName_x_but_got_x_ ByteString FuchsProgram
  | Error_ExpectedMagicToken_x_ForBind_ButGot_x_ ByteString ByteString
  | Error_BindIsSupposedToHaveCertainFormatButWeGot [FuchsProgram]
  | Error_WhileSuashingEncodedVariableName Error_EncodedVariableName
  | Error_WhileSquashingEncodedTerm Error_SquashingTerm
  | Error_WhileDestoringOldBindings Error_FuchsStore
  | Error_ExpectedProgramEncodedBindToHaveACertainFormatButGot FuchsProgram
  | Error_WhileDecodingAdvanceResultChildren Error_DecodingAdvanceResultChildren
  deriving Show

-- TODO where to put withMagicSerialization? Are there alternative aproaches?
-- NOTE: the return type is not a State. It is the raw bindings
squashProgramEncodedAdvanceResult_withMagicSerialization
  :: [(VariableName, FuchsReceipt)] --TODO should wrap in something?
  -> FuchsReceipt
  -> ExceptT
        Error_WhileSquashingProgramEncodedState
        (State.State TangleStore)
        ([(VariableName, FuchsReceipt)], FuchsReceipt)
squashProgramEncodedAdvanceResult_withMagicSerialization
  old_bindings_stored
  fuchsReceipt
  = do
  programEncodedBindings
    <- mapException Error_WhileDestoringSupposedlyProgramEncodedBindings
    $ destoreProgram
    $ fuchsReceipt

  -- TODO here we can recognise that a State and an Environment are kind of the same
  -- TODO this is kind of inefficient because we destore the program just to store it maybe again later?
  old_bindings
    <- mapException Error_WhileDestoringOldBindings
    $ mapM destoreRightPart
    $ old_bindings_stored

  -- TODO do not pattern match ConstructorName but compare at higher level
  (squashedBindings, returnValue) <- case programEncodedBindings of
    SaturatedConstruction (ConstructorName user_brandish) user_payload ->
      if magic_AdvanceResult == user_brandish
         then id
           $ mapException Error_WhileDecodingAdvanceResultChildren
           $ ExceptT
           $ return
           $ squashProgramEncodedAdvanceResult (Environment old_bindings)
           $ user_payload
         else throwException $ Error_ExpectedMagicToken_x_ForAdvanceResultButGot_x_ magic_AdvanceResult user_brandish
    garbage -> throwException $ Error_ExpectedConstructorName_x_but_got_x_ magic_AdvanceResult garbage

  storedBindings <- mapM storeRightPart squashedBindings

  storedSystemCall
    <- State.lift
    $ storeProgram
    $ returnValue

  return (storedBindings, storedSystemCall)
  where
    magic_AdvanceResult = UTF8.fromString magicStringForEncodedAdvanceResult

    storeRightPart (variableName, program)
      = State.lift $ do
          receipt <- storeProgram program
          return (variableName, receipt)

    destoreRightPart (variableName, receipt)
      = do
          program <- destoreProgram receipt
          return (variableName, program)




---


data Error_DecodingAdvanceResultChildren
  = Error_ExpextedExactlyTwoChildrenButGot [FuchsProgram]
  | Error_WhileSquasingState Error_DecodingState
  deriving Show

squashProgramEncodedAdvanceResult
  :: Environment
  -> [FuchsProgram]
  -> Either
      Error_DecodingAdvanceResultChildren
      ([(VariableName, FuchsProgram)], FuchsProgram)
squashProgramEncodedAdvanceResult
  old_bindings
  _programs@[encodedBindings, systemCall]
  = do
    state
      <- mapLeft Error_WhileSquasingState
      $ squashEncodedState old_bindings encodedBindings
    return (state, systemCall)
squashProgramEncodedAdvanceResult _old_bindings programs
  = Left
  $ Error_ExpextedExactlyTwoChildrenButGot programs

---


data Error_DecodingState
  = Error_DidExpectSaturatedConstructorForBindButGot FuchsProgram
  | Error_ExpectedMagic_x_but_got_x ByteString FuchsProgram
  | Error_WhileDecodingBindings Error_WhileSquashingProgramEncodedState
  deriving Show

-- TODO split up Error_WhileSquashingProgramEncodedState into two errors
squashEncodedState
  :: Environment
  -> FuchsProgram
  -> Either
        Error_DecodingState
        [(VariableName, FuchsProgram)]
squashEncodedState
  former_bindings
  object@(SaturatedConstruction (ConstructorName user_brandish) programs)
  = result
  where
    result
      = if user_brandish == magic_State
          then mapLeft Error_WhileDecodingBindings
            $ mapM (squashProgramEncodedBind former_bindings) programs
          else Left $ Error_ExpectedMagic_x_but_got_x magic_State object

    magic_State = UTF8.fromString magicStringForEncodedState


squashEncodedState
  _environment
  garbage
  = Left
  $ Error_DidExpectSaturatedConstructorForBindButGot garbage






---




squashProgramEncodedBind
  :: Environment
  -> FuchsProgram
  -> Either
        Error_WhileSquashingProgramEncodedState
        (VariableName, FuchsProgram)
squashProgramEncodedBind
  old_bindings
  (SaturatedConstruction (ConstructorName user_brandish) left_and_right)
  = do
      definition
        <- if magic_Bind == user_brandish
              then squashProgramEncodedDefinition old_bindings left_and_right
              else Left $ Error_ExpectedMagicToken_x_ForBind_ButGot_x_ magic_Bind user_brandish

      return definition
  where
    magic_Bind = UTF8.fromString magicStringForEncodedBind

squashProgramEncodedBind _old_bindings object
  = Left $ Error_ExpectedProgramEncodedBindToHaveACertainFormatButGot object



---


-- TODO what is a better name for Definition?
-- TODO maybe Binding?
squashProgramEncodedDefinition
  :: Environment
  -> [FuchsProgram]
  -> Either
        Error_WhileSquashingProgramEncodedState
        (VariableName, FuchsProgram)
squashProgramEncodedDefinition
  old_bindings
  [SaturatedConstruction variableName [], term]
  = do
      squashed_variableName <- mapLeft Error_WhileSuashingEncodedVariableName
        $ parseProgramEncodedVariableName variableName

      squashed_term <- mapLeft Error_WhileSquashingEncodedTerm
        $ squashEncodedProgramTerm old_bindings term

      return (squashed_variableName, squashed_term)

squashProgramEncodedDefinition _old_bindings arguments
  = Left
  $ Error_BindIsSupposedToHaveCertainFormatButWeGot arguments


---



---


data Error_SquashingTerm
  = Error_CannotFindMagicName_x_in_Environment_x VariableName Environment
  | Error_UnrecognizedMagic_Construction_did_not_expect_attachments FuchsProgram
  deriving Show

-- TODO allow magic recursively?
-- Probably not? If not why at all?
squashEncodedProgramTerm
  :: Environment
  -> FuchsProgram
  -> Either Error_SquashingTerm FuchsProgram

squashEncodedProgramTerm
  old_bindings
  construction@(SaturatedConstruction constructor@(ConstructorName _magic_name) recursiveAttachments)
  = case parseProgramEncodedVariableName constructor of
           Left _ -> return construction --TODO no recursive magic!?
           Right brandish_variableName -> do

              -- the magic that is recognised here accepts no recursiveAttachments
              -- this is for not spoiling the magic namespace
              if Prelude.null recursiveAttachments
                 then return ()
                 else Left $ Error_UnrecognizedMagic_Construction_did_not_expect_attachments construction

              magic_term <- case lookupInEnvironment old_bindings brandish_variableName of
                    Just fuchsProgram -> return fuchsProgram
                    Nothing -> Left $ Error_CannotFindMagicName_x_in_Environment_x brandish_variableName old_bindings
              return magic_term


-- TODO Hm. This feels weird since
squashEncodedProgramTerm
  _old_bindings
  program@(Lambda _globals _bounded _body)
  = return program


squashEncodedProgramTerm old_bindings term
  = error
  $ "not considered case in term squashing: "
  ++ show (term, "isolator", old_bindings)



