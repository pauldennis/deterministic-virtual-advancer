module Advance.AdvanceWithAdvanceIntrinsic where

import Store.Fuchs
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Advance.EncodingAndDecoding.Squashing
import Advance.IntrinsicAdvanceSignature

import Evaluate.Gas
import Evaluate.Evaluation
import Evaluate.IntrinsicSignature
import Evaluate.Environment
import Advance.Advance
import Advance.ActualUsingAdvance

intrinsic_advance_binding :: IntrinsicBind
intrinsic_advance_binding
  = IntrinsicBind intrinsic_advance_signature (MagicSpell recursive_advance_call)

current_standard_intrinsics :: Intrinsics
current_standard_intrinsics = prepareIntrinsics [intrinsic_advance_binding]


---



data IntrinsicBind
  = IntrinsicBind IntrinsicSignature MagicSpell


prepareIntrinsics
  :: [IntrinsicBind]
  -> Intrinsics
prepareIntrinsics list
  = Intrinsics (Environment environmentBindings) magicCalcualtion
  where
    environmentBindings = map prepareEnvironment list
    magicCalcualtion = map prepareMagic list



prepareEnvironment
  :: IntrinsicBind
  -> (VariableName, FuchsProgram)
prepareEnvironment
  (IntrinsicBind intrinsicSignature _magic_spell)
  = (getExpectedMagicVariableName intrinsicSignature, StrictIntrinsic variableName)
  where
    IntrinsicSignature variableName _ = intrinsicSignature

prepareMagic
  :: IntrinsicBind
  -> (VariableName, Intrinsic)
prepareMagic
  (IntrinsicBind intrinsicSignature magicSpell) =
  ( getExpectedMagicVariableName intrinsicSignature
  , Intrinsic (error "no arity") magicSpell
  )


---




-- TODO this should actually be a TangleStore-State manipulating function?
recursive_advance_call
  :: Gas
  -> [FuchsProgram]
  -> FuchsProgram
recursive_advance_call
  remaining_gas_of_parent_caller
  (gas:timeout:intrinsics:state:encoded_block:[])
  = fromEither $ do
      if gas == nullaryConstructor "UnlimitedGas" --TODO magic API calls in ints own file. TODO auto compile these information into the signature hash
         then Right ()
         else Left $ trap_FuchsProgram "unrecognised timelimit, expected"

      if timeout == nullaryConstructor "NoTimeout"
         then Right ()
         else Left $ trap_FuchsProgram "unrecognised timelimit"

      if intrinsics == nullaryConstructor "NoIntrinsics"
         then Right ()
         else Left $ trap_FuchsProgram "unrecognised intrinsics"

      nativeState <- case state of
        OpaqueNativeObject nativeState -> return $ nativeState
        not_an_object -> Left $ trap_FuchsProgram $ "not an OpaqueNativeObject: " ++ show not_an_object

      let spendable_gas = sameGasLimits remaining_gas_of_parent_caller

      block
        <- case squashProgramEncodedProgram encoded_block of
              Right squashed_block -> return squashed_block
              Left errorCase -> Left $ Trap (nullaryConstructor $ "TODO tell that squasing program did not work: " ++ show errorCase)

      let squashed_block = BlockProgram block

      -- now the recursive call to advance
      let InterpretationResult maybe_new_state _
            = advance_no_bother_version
                emptyIntrinsicEnvironment
                spendable_gas
                nativeState
                squashed_block

      case maybe_new_state of
        Right fuchsProgramState
          -> return $ result
          where
            result = result1
            result1 = Trap (nullaryConstructor $ "TODO return new state: " ++ show (OpaqueNativeObject fuchsProgramState))
            _result2 = OpaqueNativeObject fuchsProgramState
        Left errorCase
          -> Left $ Trap (nullaryConstructor $ "TODO return that advance did not work: " ++ show errorCase)
  where
    fromEither (Right x) = x
    fromEither (Left x) = x

recursive_advance_call _gas unmatched_arguments_numbers
  = Trap
  $ naryConstructor
    "unmatched number of arguments. -- TODO make this an runtime error, not just a trap"
    unmatched_arguments_numbers

