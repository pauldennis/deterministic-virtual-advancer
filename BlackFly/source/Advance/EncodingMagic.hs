module Advance.EncodingMagic where

import qualified Data.ByteString.UTF8 as UTF8
import Data.Word

import Data.ByteString
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Store.Fuchs
import Compiler.Frontend.UnicodeSubsets

encode_as_magic :: String -> String
encode_as_magic = ([buildInMeaning_prefix_for_Constructor] ++)


magicEncodingString_of_case_of_FuchsProgram :: FuchsProgram -> String
magicEncodingString_of_case_of_FuchsProgram (Variable _) = encode_as_magic "Variable"
magicEncodingString_of_case_of_FuchsProgram (Apply _ _) = encode_as_magic "Apply"
magicEncodingString_of_case_of_FuchsProgram (Lambda _ _ _) = encode_as_magic "Lambda"
magicEncodingString_of_case_of_FuchsProgram (Case _ _ _) = encode_as_magic "Case"
magicEncodingString_of_case_of_FuchsProgram (SaturatedConstruction _ _) = encode_as_magic "SaturatedConstruction"
magicEncodingString_of_case_of_FuchsProgram (Trap _) = encode_as_magic "Trap"
magicEncodingString_of_case_of_FuchsProgram (RecursiveLet _ _) = encode_as_magic "RecursiveLet"
magicEncodingString_of_case_of_FuchsProgram (OpaqueNativeObject _) = encode_as_magic "OpaqueNativeObject"
magicEncodingString_of_case_of_FuchsProgram x = error $ "no encoding case provided for: " ++ show x


magicStringForEncodedAdvanceResult :: String
magicStringForEncodedAdvanceResult
  = encode_as_magic "AdvanceResult"


magicStringForEncodedState :: String
magicStringForEncodedState
  = encode_as_magic "State"


magicStringForEncodedBind :: String
magicStringForEncodedBind
  = encode_as_magic "Bind"


magicStringForLambdaArguments :: String
magicStringForLambdaArguments
  = encode_as_magic "LambdaArguments"

magicStringForCaseAlternative :: String
magicStringForCaseAlternative
  = encode_as_magic "CaseAlternative"


buildInMeaning_prefix_as_twobytes :: (Word8, Word8)
buildInMeaning_prefix_as_twobytes = id
  $ (\[first, second]->(first, second))
  $ unpack
  $ UTF8.fromString
  $ [buildInMeaning_prefix_for_Constructor]


-- TODO this encoding is actually wrong
-- correctly encoded would be when every byte is encoded in a magic list. Right now a check function can only pattern match previously known variable names and Constructors
magicPrefixIndicator :: UTF8.ByteString
magicPrefixIndicator = UTF8.fromString [buildInMeaning_prefix_for_Constructor]


-- an indicator that something is wrong can be seen here: append is actually some kind of string concatenation which it actually is not
makeByteSequenceMagicForConstructor
  :: UTF8.ByteString
  -> UTF8.ByteString
makeByteSequenceMagicForConstructor = append magicPrefixIndicator


promoteConstructorToMagicConstructor
  :: ConstructorName
  -> ConstructorName
promoteConstructorToMagicConstructor (ConstructorName byteString)
  = ConstructorName $ makeByteSequenceMagicForConstructor byteString

