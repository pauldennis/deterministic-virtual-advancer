module Advance.Advance where
{- |
Many have tried similar things with similar intentions for more then several decades. May this hopefully be at least a little improvement of some somewhere probably already existing technology.
-}


import Trifle.Rename

import qualified Data.ByteString as Bytes
import qualified Control.Monad.State as State

import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Data.Either.Combinators
import Data.ByteString.UTF8

import Store.Tangle
import Store.Fuchs
import Store.OpaqueState
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Advance.ProgramEncodedProgramCompiler
import Advance.ActualUsingAdvance

import Evaluate.Gas
import Evaluate.Evaluation
import Evaluate.Force
import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt

magicNameOfValidationFunction :: String
magicNameOfValidationFunction = "isValid"
magicNameOfBlockIsValidNullaryConstructor :: String
magicNameOfBlockIsValidNullaryConstructor = "BlockIsValid"
magicNameOfNonvalidBlockNullaryConstructor :: String
magicNameOfNonvalidBlockNullaryConstructor = "InvalidBlock"


newtype CheckFunctionReceipt = CheckFunctionReceipt FuchsReceipt

data Error_ExtractFunction
  = RedeemingStateDidNotWork Error_RedeemOpaqueState
  | StateDoesNotContainAnBindingToValidationFunction_expected_0_in_1 Bytes.ByteString StateReceipt
  deriving Show


exractCheckFunction
  :: StateReceipt
  -> ExceptT Error_ExtractFunction (State.State TangleStore) CheckFunctionReceipt
exractCheckFunction stateReceipt = do
  OpaqueState state_bindings
    <- rethrow
    $ redeem_as_State
    $ stateReceipt

  checkFunctionReceipt
    <- except
    $ maybeToRight errorCase
    $ lookup (VariableName magicString)
    $ state_bindings

  return $ CheckFunctionReceipt checkFunctionReceipt
  where
    rethrow = mapException RedeemingStateDidNotWork

    errorCase = StateDoesNotContainAnBindingToValidationFunction_expected_0_in_1 magicString stateReceipt

    magicString = fromString magicNameOfValidationFunction




generateCheckingSatiation
  :: CheckFunctionReceipt
  -> FuchsEncodedFuchs_Receipt
  -> State.State TangleStore FuchsReceipt
generateCheckingSatiation
  (CheckFunctionReceipt checkFunction)
  (FuchsEncodedFuchs_Receipt oneArgument)
  = pawn__FunctionApplication checkFunction [oneArgument]


generateNextStateTermThatIsToBeEvaluated
  :: [(VariableName, FuchsReceipt)]
  -> FuchsReceipt
  -> State.State TangleStore FuchsReceipt
generateNextStateTermThatIsToBeEvaluated
  bindings
  bodyExpression
  = pawn__RecursiveLetBindings bindings bodyExpression


data BlockProgram
  = BlockProgram FuchsProgram

newtype GasForChecking = GasForChecking Gas
newtype GasForAdvancing = GasForAdvancing Gas

data Gases
  = Gases GasForChecking GasForAdvancing

sameGasLimits :: Gas -> Gases
sameGasLimits gas = Gases (GasForChecking gas) (GasForAdvancing gas)

data Error_Advance
  = Error_in_ExtractCheckFunction Error_ExtractFunction
  | Error_in_EvaluationForCheck Error_Evaluating_FuchsReceipt
  | Error_in_EvaluationOfNewState Error_Evaluating_FuchsReceipt
  | Error_UnrecognisedCheckingResult FuchsReceipt FuchsProgram
  | Error_in_ExtractBindingsFunction Error_RedeemOpaqueState
  | Error_TheBlockIsNotValid
  | Error_in_SquashingAttempt Error_WhileSquashingProgramEncodedState
  | Error_in_Destoring_result_of_destoring_isBlockValid_Fuchs Error_FuchsStore
  | Error_in_getting_easy_to_handle
  deriving Show

data SystemCall_or_ReturnValue
  = SystemCall_or_ReturnValue FuchsReceipt
  deriving Show

data AdvanceResult
  = AdvanceResult
      StateReceipt
      SystemCall_or_ReturnValue
  deriving Show

-- | take a State and apply a Block to it. The result is a new State. There is a check function in the State that is used to verify the Block. The check function can check the syntax of the block. Only if the Block is valid it may be applied. The check function of a State may be constrained all the way back to the genesis State. The check function gives the State some kind of type. The State can be thought of as an object from an object oriented language but the objects are immutable. If you want to have some mutating like behaviour you generate a new Block that can reuse things from the internals of the state. The advance can be an system call from within or an library call from outside. After doing this you gained an updated object.
advance
  :: Intrinsics
  -> Gases
  -> StateReceipt
  -> BlockProgram
  -> ExceptT Error_Advance (State.State TangleStore) AdvanceResult
advance
  intrinsics
  (Gases
    (GasForChecking gasForChecking)
    (GasForAdvancing gasForEvaluation)
  )
  former_stateReceipt
  (BlockProgram program)
  = do
  checkFunction
    <- mapException Error_in_ExtractCheckFunction
    $ exractCheckFunction
    $ former_stateReceipt

  programEncodedProgram
    <- lift
    $ compileProgramEncodedProgram
    $ program

  -- TODO bring other definitions in state with let clause into scope
  computationOfValidity
    <- lift
    $ generateCheckingSatiation checkFunction programEncodedProgram

  isBlockValid_Fuchs
    <- mapException Error_in_EvaluationForCheck
    $ strict_evaluation_of_FuchsReceipt intrinsics gasForChecking
    $ computationOfValidity

  -- it appears to be enough to only compare the hash of the result
  let receiptOfNullaryConstructor string
        = (\(x,_y)->x)
        $ ($ emptyTangleStore)
        $ State.runState
        $ storeProgram
        $ nullaryConstructor
        $ string
  let receiptOfSuccess
        = receiptOfNullaryConstructor magicNameOfBlockIsValidNullaryConstructor
  let receiptOfFailure
        = receiptOfNullaryConstructor magicNameOfNonvalidBlockNullaryConstructor

  prettyErrorMessagePayload
    <- mapException Error_in_Destoring_result_of_destoring_isBlockValid_Fuchs
    $ destoreProgram
    $ isBlockValid_Fuchs

  let examineCheckingResult
        | isBlockValid_Fuchs == receiptOfSuccess = return True
        | isBlockValid_Fuchs == receiptOfFailure = return False
        | otherwise = throwException $ Error_UnrecognisedCheckingResult isBlockValid_Fuchs prettyErrorMessagePayload

  isBlockValid <- examineCheckingResult

  if isBlockValid
    then return ()
    else throwException Error_TheBlockIsNotValid

  nativeProgramBlock
    <- lift
    $ storeProgram
    $ program

  OpaqueState former_bindings
    <- mapException Error_in_ExtractBindingsFunction
    $ redeem_as_State
    $ former_stateReceipt

  computationOfResultState
    <- lift
    $ generateNextStateTermThatIsToBeEvaluated former_bindings nativeProgramBlock

  newState_supposedlyFuchsInFuchsEncoded
    <- mapException Error_in_EvaluationOfNewState
    $ force_evaluation_of_FuchsReceipt intrinsics gasForEvaluation
    $ computationOfResultState

  (newNativeBindings, new_systemCall_or_ReturnValue)
    <- mapException Error_in_SquashingAttempt
    $ squashProgramEncodedAdvanceResult_withMagicSerialization former_bindings
    $ newState_supposedlyFuchsInFuchsEncoded

  latter_stateReceipt
    <- lift
    $ pawn__State
    $ newNativeBindings

  _latter_systemCall_or_ReturnValue_Receipt
    <- lift
    $ pawn__State
    $ newNativeBindings

  return $ AdvanceResult latter_stateReceipt (SystemCall_or_ReturnValue new_systemCall_or_ReturnValue)

data Error_Advance_Algebraic_Version
  = Error_WhileCallingActualAdvance Error_Advance
  | Error_WhileDestoring Error_WhileDestoringFuchsProgramState
  deriving Show

-- TODO what version is the right one?
advance_algebraic_version
  :: Intrinsics
  -> Gases
  -> FuchsProgramState
  -> BlockProgram
  -> ExceptT Error_Advance_Algebraic_Version (State.State TangleStore) FuchsProgramState
advance_algebraic_version
  intrinsics
  gases
  state
  blockProgram
  = do
      stateReceipt
        <- lift
        $ storeFuchsProgramState
        $ state

      AdvanceResult newState_Receipt _systemCall_or_ReturnValue
        <- mapException Error_WhileCallingActualAdvance
        $ advance intrinsics gases stateReceipt
        $ blockProgram

      newState
        <- mapException Error_WhileDestoring
        $ destoreFuchsProgramState
        $ newState_Receipt

      return newState

-- TODO very inefficient things are happening here?
advance_no_bother_version
  :: Intrinsics
  -> Gases
  -> FuchsProgramState
  -> BlockProgram
  -> InterpretationResult
      Error_Advance_Algebraic_Version
      TangleStore
      FuchsProgramState
advance_no_bother_version
  intrinsics
  gases
  state
  blockProgram
  = actuallyRunTheThing_withEmptyStore
  $ advance_algebraic_version intrinsics gases state blockProgram




---




exractCheckFunction_test :: ExceptT Error_ExtractFunction (State.State TangleStore) Bool
exractCheckFunction_test = do
  fuchsReceipt <- lift $ storeProgram programExample
  let stateComponents = [(constructVariableName "variable", fuchsReceipt)]
  stateReceipt <- lift $ pawn__State stateComponents

  CheckFunctionReceipt validationProgram <- exractCheckFunction stateReceipt

  return $ validationProgram == fuchsReceipt

exractCheckFunction_test2 :: ExceptT Error_ExtractFunction (State.State TangleStore) Bool
exractCheckFunction_test2 = do
  fuchsReceipt <- lift $ storeProgram programExample
  let stateComponents = [(constructVariableName "isValid", fuchsReceipt)]
  stateReceipt <- lift $ pawn__State stateComponents

  CheckFunctionReceipt validationProgram <- exractCheckFunction stateReceipt

  return $ validationProgram == fuchsReceipt

runtestcase_HaseStore
  :: ExceptT errorCase (State.State TangleStore) output
  -> Either errorCase output
runtestcase_HaseStore testcase = State.evalState (runExceptT testcase) emptyTangleStore

extract_test_1 :: Either Error_ExtractFunction Bool
extract_test_1 = runtestcase_HaseStore exractCheckFunction_test

extract_test_2 :: Either Error_ExtractFunction Bool
extract_test_2 = runtestcase_HaseStore exractCheckFunction_test2

