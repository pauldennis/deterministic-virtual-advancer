module Advance.ActualUsingAdvance where

import Control.Monad.Trans.Except
import qualified Control.Monad.State as State

import Store.Tangle




data InterpretationResult errorCase store output
  = InterpretationResult
      (Either errorCase output)
      store

getInterpretationResultOutcome
  :: InterpretationResult errorCase store output
  -> Either errorCase output
getInterpretationResultOutcome (InterpretationResult x _)
  = x


actuallyRunTheThing
  :: ExceptT errorCase (State.State store) output
  -> store
  -> InterpretationResult errorCase store output
actuallyRunTheThing interpreter store
  = uncurry InterpretationResult
  $ unpack_twice interpreter store
  where
    unpack_twice = State.runState . runExceptT



actuallyRunTheThing_withEmptyStore
  :: ExceptT errorCase (State.State TangleStore) output
  -> InterpretationResult errorCase TangleStore output
actuallyRunTheThing_withEmptyStore script
  = actuallyRunTheThing script emptyTangleStore

