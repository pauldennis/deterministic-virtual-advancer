module Advance.IntrinsicAdvanceSignature where


import Evaluate.IntrinsicSignature


intrinsic_advance_signature :: IntrinsicSignature
intrinsic_advance_signature
  = rightOrShowError
  $ makeIntrinsicSignature
      name
      (ExplanatoryText explanation)
      (Arity arity)
  where
    name = "advance"
    explanation = ""
      ++ "Dear Intrinsic User," ++ "\n"
      ++ "" ++ "\n"
      ++ "The intrinsic `advance` is the first intrinsic function I am currently adding to the runtime. The idea is to allow to have advance functionionality be recursivly called with better performance by the runtime itself. Theoretically one could implement advance themself within a program run by the runtime. The usage idea is that a program executed by the runtime is itself some kind of runtime that executes programs." ++ "\n"
      ++ "" ++ "\n"
      ++ "Later I might allow some features like just-in-time compiling the code that is to be executed by the advance function. Also it might me possible to have some completely external module/library be used by code in the runtime" ++ "\n"
      ++ "" ++ "\n"
      ++ "The arguments (as of time of writing) are: 1. Gas (determinisitc), 2. Timeout (temporary forgetting data and resyncing from the \"internet\" over Genie) 3. List of passed down intrinsics. 4. State (some bindings of some functions), 5. Block (some kind of main() that gets called with the environment provided by the State (4.). I is supposed to output not () but rather the new State. This is like and Object that gets a method of itself called, but copy on write)" ++ "\n"
    arity = 3


rightOrShowError
  :: Show a
  => Either a b
  -> b
rightOrShowError (Right result) = result
rightOrShowError (Left errorCase) = error $ show $ errorCase


test_intrinsic_advance_signature_is_valid :: (String, Bool)
test_intrinsic_advance_signature_is_valid
  = (,) "test_intrinsic_advance_signature_is_valid"
  $ seq intrinsic_advance_signature True
