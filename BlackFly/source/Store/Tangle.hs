module Store.Tangle where

import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Data.List.Split
import Data.Either.Combinators

import qualified Control.Monad.State as State
import qualified Data.ByteString as Bytes

import Store.ContentAdressableStorage
import Store.Hash

newtype TangleStore = TangleStore ContentAdressableStorage
  deriving (Show)

newtype TangleReceipt = TangleReceipt HashReceipt deriving (Eq, Show)

data Tangle = Tangle Bytes.ByteString [TangleReceipt] deriving (Show)

emptyTangleStore :: TangleStore
emptyTangleStore = TangleStore emptyStorage

data Error_TangleStore = UnderlyingContentAdressableStorage_Error Error_ContentAdressableStorage
  deriving (Show)

-- TODO find the appropriate library function
liftComputation_ContentAdressableStorage
  :: State.State ContentAdressableStorage o
  -> State.State TangleStore o
liftComputation_ContentAdressableStorage innerExecution = do
  (TangleStore store) <- State.get
  let (output, newStore) = State.runState innerExecution store
  () <- State.put (TangleStore newStore)
  return output

innerPawn :: Bytes.ByteString -> State.State TangleStore HashReceipt
innerPawn content = liftComputation_ContentAdressableStorage $ pawn content


-- TODO find the appropriate library function
liftEitherComputation_ContentAdressableStorage
  :: State.State ContentAdressableStorage o
  -> State.State TangleStore o
liftEitherComputation_ContentAdressableStorage innerExecution = do
  (TangleStore store) <- State.get
  let (output, newStore) = State.runState innerExecution store
  () <- State.put (TangleStore newStore)
  return output


innerRedeem_TangleStore :: HashReceipt -> State.State TangleStore (Either Error_TangleStore Bytes.ByteString)
innerRedeem_TangleStore receipt = id
  $ fmap (mapLeft UnderlyingContentAdressableStorage_Error)
  $ liftEitherComputation_ContentAdressableStorage
  $ redeem receipt


serializeInnerNode :: HashReceipt -> [TangleReceipt] -> Bytes.ByteString
serializeInnerNode (HashReceipt (Hash digest)) tangleReceipts = result
  where
    result = Bytes.concat allhashData

    links = fmap (\(TangleReceipt (HashReceipt (Hash rawHash))) -> rawHash) tangleReceipts

    -- an inner node consits of one raw stream and a list of innner nodes
    allhashData = digest : links

pawnWithLinks
  :: Bytes.ByteString
  -> [TangleReceipt]
  -> State.State TangleStore TangleReceipt
pawnWithLinks tag tangleReceipts = do
  tagReceipt <- innerPawn tag
  rawTangleReceipt <- innerPawn $ serializeInnerNode tagReceipt tangleReceipts
  return $ TangleReceipt rawTangleReceipt

pawnInitial :: Bytes.ByteString -> State.State TangleStore TangleReceipt
pawnInitial tag = pawnWithLinks tag []

-- TODO find library function for it
chunksOf_ByteStream :: Int -> Bytes.ByteString -> [Bytes.ByteString]
chunksOf_ByteStream n string = id
  $ map Bytes.pack
  $ chunksOf n
  $ Bytes.unpack
  $ string

deserializeInnerNode :: Bytes.ByteString -> (HashReceipt, [TangleReceipt])
deserializeInnerNode byteString = result
  where
    result = (tag_hash, map TangleReceipt links)
    rawhashs = chunksOf_ByteStream hashLengthInBytes byteString
    hashs = map (HashReceipt . Hash) rawhashs
    (tag_hash : links) = hashs

redeemTangle
  :: TangleReceipt
  -> ExceptT Error_TangleStore (State.State TangleStore) Tangle
redeemTangle (TangleReceipt hashReceipt) = do
  bytes <- ExceptT $ innerRedeem_TangleStore hashReceipt
  let (tagHash, links) = deserializeInnerNode bytes
  tagStream <- ExceptT $ innerRedeem_TangleStore tagHash
  return $ Tangle tagStream links


test_inAndOut2 :: ExceptT Error_TangleStore (State.State TangleStore) String
test_inAndOut2 = do
  initialReceipt <- lift $ pawnInitial (Bytes.pack [])
  rootReceipt <- lift $ pawnWithLinks (Bytes.pack [68,69,65,68,66,69,69,70]) [initialReceipt]
  tangle <- redeemTangle rootReceipt
  case tangle of
       (Tangle deadbeef [child]) -> do
          subtangle <- redeemTangle child
          case subtangle of
               (Tangle emptyString []) -> return $ show (deadbeef, emptyString)
               _ -> error "expected different tangle"
       _ -> error "should not happen"

dooo2 :: ExceptT e (State.State TangleStore) a -> Either e a
dooo2 f = State.evalState (runExceptT f) emptyTangleStore

dddo2 :: Either Error_TangleStore String
dddo2 = dooo2 test_inAndOut2
