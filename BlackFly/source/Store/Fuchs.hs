-- | may this be the last time I have to deal with serialization and deserialization

module Store.Fuchs where


import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Data.ByteString.UTF8

import qualified Data.ByteString as Bytes
import qualified Control.Monad.State as State

import Trifle.Rename

import Store.Tangle

-- TODO find right place for these guys:
newtype ConstructorName = ConstructorName Bytes.ByteString deriving (Eq{-, Show-})
newtype VariableName = VariableName Bytes.ByteString deriving (Eq, Ord{-, Show-})

-- TODO toString has the wrong type signature. there is no way of finding out if bytes are valid utf8
instance Show ConstructorName where
  show (ConstructorName variableName) = ""
    ++ "(ConstructorName (Data.ByteString.UTF8.fromString "
    ++ show (toString variableName)
    ++ "))"

instance Show VariableName where
  show (VariableName variableName) = ""
    ++ "(VariableName (Data.ByteString.UTF8.fromString "
    ++ show (toString variableName)
    ++ "))"

constructConstructorName :: String -> ConstructorName
constructConstructorName = ConstructorName . fromString

constructVariableName :: String -> VariableName
constructVariableName = VariableName . fromString


--


newtype FuchsReceipt = FuchsReceipt TangleReceipt deriving (Eq, Show)


-- TODO better types for arguments in ExpresionWithPlaceHolder
data Fuchs
  = DoNotuse_RemoveME --TODO
  | PrimitiveDataConstructor ConstructorName [FuchsReceipt]
  | SwitchCase FuchsReceipt [(ConstructorName, [VariableName], FuchsReceipt)] FuchsReceipt
  | ExpresionWithPlaceHolder [VariableName] [VariableName] FuchsReceipt --[free variables] [bound variables] body
  | PlaceHolder VariableName
  | RecursiveLetBinding [(VariableName, FuchsReceipt)] FuchsReceipt
  | FunctionApplication FuchsReceipt [FuchsReceipt]
  | CustomUserError FuchsReceipt --TODO probably irrelevant?
  | OpaqueBunchOfBindings [(VariableName, FuchsReceipt)]
  deriving (Show)

-- TODO give a lengthy explanation for the thing behind this
magicFuchsConstructor :: Fuchs -> Bytes.ByteString
magicFuchsConstructor = fromString . magicFuchsConstructorString

magicFuchsConstructorString :: Fuchs -> String
magicFuchsConstructorString (PrimitiveDataConstructor _ _) = "PrimitiveDataConstructor"
magicFuchsConstructorString (SwitchCase _ _ _) = "SwitchCase"
magicFuchsConstructorString (ExpresionWithPlaceHolder _ _ _) = "ExpresionWithPlaceHolder"
magicFuchsConstructorString (PlaceHolder _) = "PlaceHolder"
magicFuchsConstructorString (RecursiveLetBinding _ _) = "RecursiveLetBinding"
magicFuchsConstructorString (FunctionApplication _ _) = "FunctionApplication"
magicFuchsConstructorString (CustomUserError _) = "CustomUserError"
magicFuchsConstructorString (OpaqueBunchOfBindings _) = "Object. Huuiie, Paul Dennis was here :-)"
magicFuchsConstructorString x  = error $ "unkonw magicFuchsConstructorString: " ++ show x


data Error_FuchsStore
  = UnderlyingTangleStoreError Error_TangleStore
  | UnknownDataConstructor_x_has_x TangleReceipt Bytes.ByteString
  | ThereIsNotExactlyOneChildIn_x_ItIsSupposedToBeAPrimitiveDataConstructor TangleReceipt
  | SwitchCaseTangleContainsLessThenTwoChildren TangleReceipt
  | AlternativeDoesNotContainExactlyOnePath TangleReceipt
  | ExpresionWithPlaceHolder_DoesNotHoldExactlyTwoChildren TangleReceipt
  | ExpresionWithPlaceHolder_VariableArrayHasNonEmptyTag TangleReceipt
  | ExpresionWithPlaceHolder_VariableArrayEntryNotSupposedToHaveChildren TangleReceipt
  | PlaceholderTangleDoesNotContainExactlyOneChild TangleReceipt
  | VariableNameNotSupposedToContainChildren TangleReceipt
  | RecursiveLetBindingTangleContainsLessThanTwoChildren TangleReceipt
  | RecursiveLetBindingsBindingInstanceContainsNotExactlyOnePackage TangleReceipt
  | OpaqueBunchOfBindings_BindingInstanceContainsNotExactlyOnePackage TangleReceipt
  | VariablesHelperError Error_VariablesHelper
  | TangleThatIsSupposedTOBeAnFunctionApplicationDoesNotContainAtLeastOneChild TangleReceipt
  | BottomIsOnlyAllowedToContainOneChild TangleReceipt
  deriving (Show)




-- TODO remove me or better not?
innerRedeem_FuchsStore
  :: TangleReceipt
  -> ExceptT Error_FuchsStore (State.State TangleStore) Tangle
innerRedeem_FuchsStore receipt = id
  $ mapException UnderlyingTangleStoreError
  $ ExceptT
  $ runExceptT
  $ redeemTangle
  $ receipt


---


pawn_helper__Variables
  :: [VariableName]
  -> State.State TangleStore TangleReceipt
pawn_helper__Variables variables = do
  pawnedVariables <- mapM pawnVariable variables
  pawnWithLinks Bytes.empty pawnedVariables
  where
    pawnVariable (VariableName variable) = pawnInitial variable

data Error_VariablesHelper
  = FuchsStore_InternalError Error_FuchsStore
  | TagIsNotEmpty TangleReceipt
  | VariableNameSerializationNotSupposedToContainChildren
  deriving (Show)

redeem_helper__Variables
  :: TangleReceipt
  -> ExceptT Error_VariablesHelper (State.State TangleStore) [VariableName]
redeem_helper__Variables variableArray = do
  Tangle empty serializedVariables <- rethrow $ innerRedeem_FuchsStore variableArray
  if not $ Bytes.null empty
     then throwException $ TagIsNotEmpty variableArray
     else do
       variablesTangles <- mapM extractVariablename serializedVariables
       return variablesTangles
  where
    rethrow = mapException FuchsStore_InternalError

    extractVariablename serializedVariable = do
      Tangle tag empty <- rethrow $ innerRedeem_FuchsStore serializedVariable
      case empty of
           [] -> return $ VariableName tag
           _ -> throwException VariableNameSerializationNotSupposedToContainChildren


---



pawn__PrimitiveDataConstructor_x
  :: ConstructorName
  -> [FuchsReceipt]
  -> State.State TangleStore FuchsReceipt
pawn__PrimitiveDataConstructor_x (ConstructorName constructorName) arguments = do
  let rawArguments = map (\(FuchsReceipt x) -> x) arguments
  rawTerm <- pawnWithLinks constructorName rawArguments

  fmap FuchsReceipt $ pawnWithLinks magic_indicator [rawTerm]
  where
    magic_indicator = magicFuchsConstructor (PrimitiveDataConstructor undefined undefined)

pawn__nullaryConstructor
  :: ConstructorName
  -> State.State TangleStore FuchsReceipt
pawn__nullaryConstructor constructorName
  = pawn__PrimitiveDataConstructor_x constructorName []

pawn__SwitchCase
  :: FuchsReceipt
  -> [(ConstructorName, [VariableName], FuchsReceipt)]
  -> FuchsReceipt
  -> State.State TangleStore FuchsReceipt
pawn__SwitchCase (FuchsReceipt examinee) alternatives (FuchsReceipt defaultPath) = do
  serializedAlternatives <- mapM serializeOneAlternative alternatives
  serializedSwitchCase <- pawnWithLinks magic_indicator $ examinee : defaultPath : serializedAlternatives
  return $ FuchsReceipt serializedSwitchCase
  where
    serializeOneAlternative (ConstructorName constructorName, variableNames, FuchsReceipt path) = do
      serializedVariables <- pawn_helper__Variables variableNames
      pawnWithLinks constructorName [serializedVariables, path]
    magic_indicator = magicFuchsConstructor (SwitchCase undefined undefined undefined)


pawn__ExpresionWithPlaceHolder
  :: [VariableName]
  -> [VariableName]
  -> FuchsReceipt
  -> State.State TangleStore FuchsReceipt
pawn__ExpresionWithPlaceHolder free_variables variables (FuchsReceipt body) = do
  arrayOfFreeVariableNames <- pawn_helper__Variables free_variables
  arrayOfVariableNames <- pawn_helper__Variables variables
  resultReceipt <- pawnWithLinks magic_indicator [arrayOfFreeVariableNames, arrayOfVariableNames, body]
  return $ FuchsReceipt resultReceipt
  where
    magic_indicator = magicFuchsConstructor (ExpresionWithPlaceHolder  undefined undefined undefined)

pawn__PlaceHolder
  :: VariableName
  -> State.State TangleStore FuchsReceipt
pawn__PlaceHolder (VariableName variable) = do
  variableReceipt <- pawnInitial variable
  result <- pawnWithLinks magic_indicator [variableReceipt]
  return $ FuchsReceipt result
  where
    magic_indicator = magicFuchsConstructor (PlaceHolder undefined)

pawn__FunctionApplication
  :: FuchsReceipt
  -> [FuchsReceipt]
  -> State.State TangleStore FuchsReceipt
pawn__FunctionApplication (FuchsReceipt function) arguments = do
  result <- pawnWithLinks magic_indicator $ applicationRepresentation
  return $ FuchsReceipt result
  where
    applicationRepresentation = function : (map (\(FuchsReceipt x)->x) arguments)
    magic_indicator = magicFuchsConstructor (FunctionApplication undefined undefined)

pawn__CustomUserError
  :: FuchsReceipt
  -> State.State TangleStore FuchsReceipt
pawn__CustomUserError (FuchsReceipt fuchsReceipt) = do
  fmap FuchsReceipt $ pawnWithLinks magic_indicator [fuchsReceipt]
  where
    magic_indicator = magicFuchsConstructor (CustomUserError undefined)

pawn__RecursiveLetBindings
  :: [(VariableName, FuchsReceipt)]
  -> FuchsReceipt
  -> State.State TangleStore FuchsReceipt
pawn__RecursiveLetBindings bindings (FuchsReceipt aimExpression) = do
  bindingsList <- mapM serializeOneBinding bindings
  result <- pawnWithLinks magic_indicator (aimExpression : bindingsList)
  return $ FuchsReceipt result
  where
    serializeOneBinding (VariableName variableName, FuchsReceipt package) = pawnWithLinks variableName [package]
    magic_indicator = magicFuchsConstructor (RecursiveLetBinding undefined undefined)

pawn__OpaqueBunchOfBindings
  :: [(VariableName, FuchsReceipt)]
  -> State.State TangleStore FuchsReceipt
pawn__OpaqueBunchOfBindings bindings = do
  bindingsList <- mapM serializeOneBinding bindings
  result <- pawnWithLinks magic_indicator bindingsList
  return $ FuchsReceipt result
  where
    serializeOneBinding (VariableName variableName, FuchsReceipt package) = pawnWithLinks variableName [package]
    magic_indicator = magicFuchsConstructor (OpaqueBunchOfBindings undefined)

---


-- TODO rename this module to a better name
newtype StateReceipt
  = StateReceipt FuchsReceipt
  deriving (Show, Eq)

pawn__State
  :: [(VariableName, FuchsReceipt)]
  -> State.State TangleStore StateReceipt
pawn__State bindings = do
  fuchsReceipt <- pawn__OpaqueBunchOfBindings bindings
  return $ StateReceipt fuchsReceipt






---




redeem__PrimitiveDataConstructor_x
  :: TangleReceipt
  -> [TangleReceipt]
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeem__PrimitiveDataConstructor_x _fuchsReceipt [children] = do
  Tangle constructorName arguments <- innerRedeem_FuchsStore children
  return $ PrimitiveDataConstructor (ConstructorName constructorName) (map FuchsReceipt arguments)
redeem__PrimitiveDataConstructor_x fuchsReceipt _
  = throwException $ ThereIsNotExactlyOneChildIn_x_ItIsSupposedToBeAPrimitiveDataConstructor fuchsReceipt


redeem__SwitchCase
  :: TangleReceipt
  -> [TangleReceipt]
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeem__SwitchCase fuchsReceipt (examinee : defaultPath : alternatives) = do
  alternativeFuchses <- mapM redeem__Alternative alternatives
  return $ SwitchCase (FuchsReceipt examinee) alternativeFuchses (FuchsReceipt defaultPath)
  where
    redeem__Alternative alternativeTangleReceipt = do
      alternative <- innerRedeem_FuchsStore alternativeTangleReceipt
      case alternative of
           Tangle constructorName [variables, path] -> do
             variableNames <- mapException VariablesHelperError $ redeem_helper__Variables variables
             return (ConstructorName constructorName, variableNames, FuchsReceipt path)
           _ -> throwException $ AlternativeDoesNotContainExactlyOnePath fuchsReceipt
redeem__SwitchCase fuchsReceipt _ = throwException $ SwitchCaseTangleContainsLessThenTwoChildren fuchsReceipt

redeem__ExpresionWithPlaceHolder
  :: TangleReceipt
  -> [TangleReceipt]
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeem__ExpresionWithPlaceHolder fuchsReceipt [freeVariableArray, variableArray, termWithPlaceHolders] = do
  Tangle free_empty free_serializedVariables <- innerRedeem_FuchsStore freeVariableArray
  Tangle empty serializedVariables <- innerRedeem_FuchsStore variableArray
  if (not $ Bytes.null empty) && (not $ Bytes.null free_empty)
     then throwException $ ExpresionWithPlaceHolder_VariableArrayHasNonEmptyTag fuchsReceipt
     else do
       free_variablesTangles <- mapM innerRedeem_FuchsStore free_serializedVariables
       variablesTangles <- mapM innerRedeem_FuchsStore serializedVariables
       free_variables <- mapM (extractVariablename fuchsReceipt) free_variablesTangles
       variables <- mapM (extractVariablename fuchsReceipt) variablesTangles
       return $ ExpresionWithPlaceHolder free_variables variables (FuchsReceipt termWithPlaceHolders)
  where
    extractVariablename _fuchsReceipt (Tangle tag []) = return $ VariableName tag
    extractVariablename fuchsReceiptForErrorCase _ = throwException $ ExpresionWithPlaceHolder_VariableArrayEntryNotSupposedToHaveChildren fuchsReceiptForErrorCase
redeem__ExpresionWithPlaceHolder fuchsReceipt _ = throwException $ ExpresionWithPlaceHolder_DoesNotHoldExactlyTwoChildren fuchsReceipt

redeem__PlaceHolder
  :: TangleReceipt
  -> [TangleReceipt]
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeem__PlaceHolder fuchsReceipt [variableName] = do
  Tangle tag noChildren <- innerRedeem_FuchsStore variableName
  case noChildren of
       [] -> return $ PlaceHolder (VariableName tag)
       _ -> throwException $ VariableNameNotSupposedToContainChildren fuchsReceipt
redeem__PlaceHolder fuchsReceipt _ = throwException $ PlaceholderTangleDoesNotContainExactlyOneChild fuchsReceipt

redeem__FunctionApplication
  :: TangleReceipt
  -> [TangleReceipt]
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeem__FunctionApplication _fuchsReceipt (function : arguments) = do
  return $ FunctionApplication (FuchsReceipt function) (map FuchsReceipt arguments)
redeem__FunctionApplication fuchsReceipt [] = throwException $ TangleThatIsSupposedTOBeAnFunctionApplicationDoesNotContainAtLeastOneChild $ fuchsReceipt

redeem__CustomUserError
  :: TangleReceipt
  -> [TangleReceipt]
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeem__CustomUserError _fuchsReceipt [child] = do
  return $ CustomUserError (FuchsReceipt child)
redeem__CustomUserError fuchsReceipt _ = throwException $ BottomIsOnlyAllowedToContainOneChild fuchsReceipt

redeem__RecursiveLetBinding
  :: TangleReceipt
  -> [TangleReceipt]
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeem__RecursiveLetBinding fuchsReceipt (bodyExpression : children@(_firstborn : _agnatus)) = do -- TODO why the things after @? Is this wrong?
  bindings <- mapM (extractBinding fuchsReceipt) children
  return $ RecursiveLetBinding bindings (FuchsReceipt bodyExpression)
  where
    extractBinding fuchsReceiptForErrorCase child = do
      Tangle variableName packages <- innerRedeem_FuchsStore child
      case packages of
           [package] -> return $ (VariableName variableName, FuchsReceipt package)
           _ -> throwException $ RecursiveLetBindingsBindingInstanceContainsNotExactlyOnePackage fuchsReceiptForErrorCase
redeem__RecursiveLetBinding fuchsReceipt _ = throwException $ RecursiveLetBindingTangleContainsLessThanTwoChildren fuchsReceipt

redeem__OpaqueBunchOfBindings
  :: TangleReceipt
  -> [TangleReceipt]
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeem__OpaqueBunchOfBindings fuchsReceipt encodedBindings = do
  bindings <- mapM (extractBinding fuchsReceipt) encodedBindings
  return $ OpaqueBunchOfBindings bindings
  where
    extractBinding fuchsReceiptForErrorCase child = do
      Tangle variableName packages <- innerRedeem_FuchsStore child
      case packages of
           [package] -> return $ (VariableName variableName, FuchsReceipt package)
           _ -> throwException $ OpaqueBunchOfBindings_BindingInstanceContainsNotExactlyOnePackage fuchsReceiptForErrorCase



redeemFuchs_FuchsStore
  :: FuchsReceipt
  -> ExceptT Error_FuchsStore (State.State TangleStore) Fuchs
redeemFuchs_FuchsStore (FuchsReceipt fuchsReceipt) = do
  Tangle tag children <- innerRedeem_FuchsStore fuchsReceipt

  let distinguish
        | tag == primitiveData     = redeem__PrimitiveDataConstructor_x fuchsReceipt children
        | tag == switchCase        = redeem__SwitchCase fuchsReceipt children
        | tag == lambdaAbstraction = redeem__ExpresionWithPlaceHolder fuchsReceipt children
        | tag == variable          = redeem__PlaceHolder fuchsReceipt children
        | tag == let_recursive     = redeem__RecursiveLetBinding fuchsReceipt children
        | tag == apply             = redeem__FunctionApplication fuchsReceipt children
        | tag == object            = redeem__OpaqueBunchOfBindings fuchsReceipt children
        | tag == trap              = redeem__CustomUserError fuchsReceipt children

        | otherwise = throwException $ UnknownDataConstructor_x_has_x fuchsReceipt tag

  distinguish

  where
    primitiveData     = magicFuchsConstructor (PrimitiveDataConstructor undefined undefined)
    switchCase        = magicFuchsConstructor (SwitchCase undefined undefined undefined)
    lambdaAbstraction = magicFuchsConstructor (ExpresionWithPlaceHolder undefined undefined undefined)
    variable          = magicFuchsConstructor (PlaceHolder undefined)
    let_recursive     = magicFuchsConstructor (RecursiveLetBinding undefined undefined)
    apply             = magicFuchsConstructor (FunctionApplication undefined undefined)
    object            = magicFuchsConstructor (OpaqueBunchOfBindings undefined)
    trap              = magicFuchsConstructor (CustomUserError undefined)





---




test_inAndOut__PrimitiveDataConstructor_x :: ExceptT Error_FuchsStore (State.State TangleStore) String
test_inAndOut__PrimitiveDataConstructor_x = do
  primitiveDataBlob <- lift $ pawn__nullaryConstructor $ constructConstructorName "öüä"
  fuchs <- redeemFuchs_FuchsStore primitiveDataBlob
  return $ show (primitiveDataBlob, fuchs)

test_inAndOut__SwitchCase :: ExceptT Error_FuchsStore (State.State TangleStore) String
test_inAndOut__SwitchCase = do
  c1 <- lift $ pawn__nullaryConstructor customConstructorName_1
  _c2 <- lift $ pawn__nullaryConstructor customConstructorName_2
  a1 <- lift $ pawn__nullaryConstructor alternative_name_1
  a2 <- lift $ pawn__nullaryConstructor alternative_name_2

  a3 <- lift $ pawn__nullaryConstructor alternative_name_3

  caseExpression <- lift $ pawn__SwitchCase c1
    [ (customConstructorName_1, [], a1)
    , (customConstructorName_2, [], a2)
    ]
    a3

  fuchs <- redeemFuchs_FuchsStore caseExpression
  return $ show (caseExpression, fuchs)
  where
    customConstructorName_1 = constructConstructorName "aeaeae"
    customConstructorName_2 = constructConstructorName "ssssss"
    alternative_name_1 = constructConstructorName "1111111"
    alternative_name_2 = constructConstructorName "2222222"
    alternative_name_3 = constructConstructorName "default"

test_inAndOut__ExpresionWithPlaceHolder :: ExceptT Error_FuchsStore (State.State TangleStore) String
test_inAndOut__ExpresionWithPlaceHolder = do
  body <- lift $ pawn__nullaryConstructor $ constructConstructorName "a constant"
  lambda <- lift $ pawn__ExpresionWithPlaceHolder [] [constructVariableName "x", constructVariableName "y"] body
  fuchs <- redeemFuchs_FuchsStore lambda
  return $ show fuchs

test_inAndOut__OpaqueBunchOfBindings :: ExceptT Error_FuchsStore (State.State TangleStore) String
test_inAndOut__OpaqueBunchOfBindings = do
  constant_function <- lift $ pawn__nullaryConstructor $ constructConstructorName "program stub"
  opaqueBunchOfBindings <- lift $ pawn__OpaqueBunchOfBindings [(constructVariableName "part of state", constant_function)]
  bunch_of_bindings <- redeemFuchs_FuchsStore opaqueBunchOfBindings
  return $ show bunch_of_bindings

runtestcase :: ExceptT e (State.State TangleStore) a -> Either e a
runtestcase testcase = State.evalState (runExceptT testcase) emptyTangleStore

testcase_1 :: Either Error_FuchsStore String
testcase_1 = runtestcase test_inAndOut__PrimitiveDataConstructor_x
testcase_2 :: Either Error_FuchsStore String
testcase_2 = runtestcase test_inAndOut__SwitchCase
testcase_3 :: Either Error_FuchsStore String
testcase_3 = runtestcase test_inAndOut__ExpresionWithPlaceHolder
testcase_4 :: Either Error_FuchsStore String
testcase_4 = runtestcase test_inAndOut__OpaqueBunchOfBindings
