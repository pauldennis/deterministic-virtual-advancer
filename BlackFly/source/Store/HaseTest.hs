module Store.HaseTest where

import qualified Control.Monad.State as State

import Data.Either.Combinators

import Trifle.Test

import Control.Monad.Trans.Except
import Control.Monad.Trans.Class

import Advance.Advance
import Store.Fuchs
import Store.Tangle

import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt




inoutExample :: ExceptT Error_FuchsStore (State.State TangleStore) Bool
inoutExample = do
  fuchsReceipt <- lift $ storeProgram programExample
  let stateComponents = [(constructVariableName "variable", fuchsReceipt)]
  stateReceipt <- lift $ pawn__OpaqueBunchOfBindings stateComponents
  wannebe__bindings <- redeemFuchs_FuchsStore stateReceipt
  let OpaqueBunchOfBindings bindings = wannebe__bindings
  return $ stateComponents == bindings

hase_example :: Either Error_FuchsStore Bool
hase_example = runtestcase_HaseStore inoutExample

test_inout_hase :: Test
test_inout_hase = (,) "test_inout_hase" $ and [ True
  , fromRight' hase_example
  ]
