module Store.HexEncoding where

import GHC.Word
import Data.Tuple
import GHC.Stack
import Safe.Partial

import qualified Data.ByteString as Bytes


hexToBytes :: Partial => Bytes.ByteString -> Either String Bytes.ByteString
hexToBytes certainBytes = withFrozenCallStack
  $ Right --TODO proper upwards
  $ Bytes.pack
  $ extractOnlyRights
  $ map interpretDyad
  $ dyads
  $ Bytes.unpack
  $ certainBytes
  where
    dyads [] = []
    dyads (x:y:ys) = (x,y) : dyads ys
    dyads _ = error "not an even umber of base16 characters"

    extractOnlyRights :: [Either String Word8] -> [Word8]
    extractOnlyRights = extractOnlyRights' 0

--     extractOnlyRights' :: Integer
    extractOnlyRights' n ((Right x):xs) = x : extractOnlyRights' ((n::Integer)+2) xs
    extractOnlyRights' _ [] = []
    extractOnlyRights' n ((Left x):_) = error $ "dyad at position " ++ show n ++ " is faulty" ++ x

bytesToHex :: Bytes.ByteString -> Bytes.ByteString
bytesToHex arbitrayBytes = id
  $ Bytes.pack
  $ (>>= encodeDyad)
  $ Bytes.unpack
  $ arbitrayBytes

encodeDyad :: Word8 -> [Word8]
encodeDyad byte = (\(x,y) -> [x,y]) $ snd $ (allMappings !! (fromIntegral byte))


interpretDyad :: (Word8, Word8) -> Either String Word8
interpretDyad dyad@(x1,x2) = result
  where
    errorCase
      = "invalid characters in dyad: "
      ++ show dyad
      ++ " "
      ++ show (toEnumChar $ fromIntegral x1, toEnumChar $ fromIntegral x2)

    toEnumChar :: Int -> Char
    toEnumChar = toEnum

    result = annodateErrorMessage
      $ lookup dyad
      $ map swap
      $ allMappings

    annodateErrorMessage (Just a) = Right a
    annodateErrorMessage Nothing = Left errorCase

allowedcharacters :: [GHC.Word.Word8]
allowedcharacters = id
  $ map fromIntegral
  $ map fromEnum
  $ "0123456789abcdef"

allCombinations :: [(Word8, Word8)]
allCombinations = do
  left <- allowedcharacters
  right <- allowedcharacters
  return (left,right)

allMappings :: [(Word8, (Word8, Word8))]
allMappings = zip [(0::GHC.Word.Word8)..] allCombinations

