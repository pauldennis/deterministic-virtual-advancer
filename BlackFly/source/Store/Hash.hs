module Store.Hash where

import Data.ByteString.UTF8 as BSU
import Crypto.Hash
import Safe

import Store.HexEncoding


import qualified Data.ByteString as Bytes

newtype WhirlpoolOrdered = WhirlpoolOrdered Whirlpool deriving (Eq)

-- TODO use deriving in upstream
instance Ord WhirlpoolOrdered where
  left <= right = (show left) <= (show right)

instance Show WhirlpoolOrdered where
  show (WhirlpoolOrdered x) = show x

newtype Hash = Hash Bytes.ByteString deriving (Eq, Ord)

instance Show Hash where
  show (Hash digest) = toString (bytesToHex $ {-Bytes.take 5-} digest)

-- TODO this function is not total as whirlpool doesnt work on arbitrary long data
currently_used_hash_algorithm :: Bytes.ByteString -> WhirlpoolOrdered
currently_used_hash_algorithm = WhirlpoolOrdered . hash

hashLengthInBytes :: Int
hashLengthInBytes = assertNote note check result
  where
    note = "something is off with the hash length"
    result = 64
    check = True
      && 64 <= result -- enouth entropy
      && result == lengthOfExample

    lengthOfExample = id
      $ Prelude.length
      $ Bytes.unpack
      $ calculateHash
      $ Bytes.pack []

calculateHash :: ByteString -> ByteString
calculateHash food = id
  $ extractRight
  $ hexToBytes
  $ BSU.fromString
  $ show
  $ currently_used_hash_algorithm
  $ food
  where
    extractRight (Right x) = x
    extractRight (Left errorMessage) = error $ "got a wrongly encoded hash: " ++ errorMessage

used_hasher :: ByteString -> Hash
used_hasher = Hash . calculateHash
