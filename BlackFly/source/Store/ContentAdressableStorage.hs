module Store.ContentAdressableStorage where

-- import Import

import Data.Tree
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import GHC.Exts

import Trifle.TreeShow

import Store.Hash

import qualified Data.Map.Strict as Map
import qualified Control.Monad.State as State
import qualified Data.ByteString as Bytes


newtype ContentAdressableStorage = ContentAdressableStorage (Map.Map Hash Bytes.ByteString)

instance Show ContentAdressableStorage where
  show x = unlines $ (showTree_ContentAdressableStorage x) >>= drawSimpleForest

drawSimpleForest :: Tree String -> [String]
drawSimpleForest (Node x xs) = [x] ++ indentedLines
  where
    recursion = xs >>= drawSimpleForest
    indentedLines = map ("  " ++) recursion

newtype HashReceipt = HashReceipt Hash deriving (Eq, Ord, Show)

instance ShowTree ContentAdressableStorage where
  showTree = showTree_ContentAdressableStorage

showTree_ContentAdressableStorage :: ContentAdressableStorage -> Forest String
showTree_ContentAdressableStorage (ContentAdressableStorage hashMap) = result
  where
    result = stringList

    mappings = Map.toList hashMap
    sortedByHash = sortWith fst mappings
    stringList = map asTree sortedByHash

    asTree (hash, content) = Node (show hash) [Node (show content) []]


issueReceipt :: Hash -> HashReceipt
issueReceipt = HashReceipt


emptyStorage :: ContentAdressableStorage
emptyStorage = ContentAdressableStorage Map.empty

pawn
  :: Bytes.ByteString
  -> State.State ContentAdressableStorage HashReceipt
pawn content = do
  store <- State.get
  State.put $ insertIntoStorage hash content store
  return newReceipt
  where
    hash = used_hasher content
    newReceipt = issueReceipt hash

-- TODO i forgot to check that the size does not exceed maxBound::Int which is undefined behaviour
insertIntoStorage
  :: Hash
  -> Bytes.ByteString
  -> ContentAdressableStorage
  -> ContentAdressableStorage
insertIntoStorage hash content (ContentAdressableStorage store)
  = ContentAdressableStorage
  $ Map.insertWith const hash content store
  -- TODO check if (\x y -> y) would be faster than const

lookupFromStorage :: Hash -> ContentAdressableStorage -> Maybe Bytes.ByteString
lookupFromStorage hash (ContentAdressableStorage store) = Map.lookup hash store

data Error_ContentAdressableStorage = ThereIsNoDataInContentAdressableStorageRegisteredWithTheHash_x Hash
  deriving (Show)

redeem :: HashReceipt -> State.State ContentAdressableStorage (Either Error_ContentAdressableStorage Bytes.ByteString)
redeem (HashReceipt hash) = do
  store <- State.get
  return $ enrichErrorCase $ lookupFromStorage hash store
  where
    enrichErrorCase (Just x) = Right x
    enrichErrorCase Nothing = Left $ ThereIsNoDataInContentAdressableStorageRegisteredWithTheHash_x hash

test_inAndOut :: ExceptT Error_ContentAdressableStorage (State.State ContentAdressableStorage) Bytes.ByteString
test_inAndOut = do
  receipt <- lift $ pawn (Bytes.pack [0..255])
  let _fakeReceipt = HashReceipt $ used_hasher $ Bytes.pack []
  ExceptT $ redeem receipt
--   ExceptT $ redeem fakeRceipt

test_runInAndOut :: Either Error_ContentAdressableStorage Bytes.ByteString
test_runInAndOut = State.evalState (runExceptT test_inAndOut) emptyStorage
