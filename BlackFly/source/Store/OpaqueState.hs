module Store.OpaqueState where

import qualified Control.Monad.State as State

import Trifle.Rename

import Control.Monad.Trans.Except

import Store.Fuchs
import Store.Tangle


newtype OpaqueState
  = OpaqueState [(VariableName, FuchsReceipt)]

data Error_RedeemOpaqueState
  = RedeemingFuchsForStateDidNotWork Error_FuchsStore
  | Fuchs_x_ReceiptDidNotRepresentAnOpaqueBunchOfBindings_ButItrepresented Fuchs
  deriving Show

redeem_as_State
  :: StateReceipt
  -> ExceptT Error_RedeemOpaqueState (State.State TangleStore) OpaqueState
redeem_as_State (StateReceipt stateReceipt) = do
  wannebe_state <- rethrow $ redeemFuchs_FuchsStore stateReceipt
  state_bindings <- except $ parse_as_state wannebe_state
  return $ OpaqueState state_bindings
  where
    rethrow = mapException RedeemingFuchsForStateDidNotWork

    parse_as_state (OpaqueBunchOfBindings bindings) = Right bindings
    parse_as_state other_case = Left $ Fuchs_x_ReceiptDidNotRepresentAnOpaqueBunchOfBindings_ButItrepresented other_case
