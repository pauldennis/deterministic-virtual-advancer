module Evaluate.EvaluationTest where

import Data.Either.Combinators
import Data.Maybe

import Trifle.Test
import Store.Fuchs
import Evaluate.Gas
import Evaluate.Evaluation


import ExampleFuchsProgramms.AsHaskellConstant.EvaluateExamples
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Compiler.CompileChain

example_test_by_name
  :: Show a
  => a
  -> FuchsProgram
example_test_by_name testnumber = id
  $ fromMaybe (error "example term not found")
  $ lookupInFuchsProgramState (constructVariableName $ "test" ++ show testnumber)
  $ fromRight (error "program does not compile")
  $ compile_fuchs_file
  $ evaluateexamples


evaluate_test :: Gas -> Int -> Either Error_Evaluating_FuchsProgram FuchsProgram
evaluate_test gas n = evaluateFuchsProgram emptyIntrinsicEnvironment gas $ example_test_by_name n


test_evaluation_little_examples :: Test
test_evaluation_little_examples = (,) "test_evaluation_little_examples" $ and [ True
  , isRight $ evaluate_test (Gas 1) 1
  , isRight $ evaluate_test (Gas 1) 2
  , isRight $ evaluate_test (Gas 2) 3
  , isRight $ evaluate_test (Gas 3) 4
  , isRight $ evaluate_test (Gas 3) 5
  , isLeft $ evaluate_test UnlimitedGas 6
  , isRight $ evaluate_test (Gas 9) 7
  , isRight $ evaluate_test (Gas 3) 8
  , isRight $ evaluate_test (Gas 6) 9
  , isRight $ evaluate_test (Gas 5) 10

  , isRight $ evaluate_test (Gas 3) 100010
  , isRight $ evaluate_test (Gas 5) 100011
  , isRight $ evaluate_test (Gas 7) 100012
  , isRight $ evaluate_test (Gas 9) 100013
  , isRight $ evaluate_test (Gas 11) 100014
  , isRight $ evaluate_test (Gas 13) 100015
  ]

