module Evaluate.Evaluation where

-- import Debug.Trace

import Numeric.Natural
import Data.List
import Data.Either.Combinators

import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Evaluate.Gas
import Evaluate.IntrinsicSignature
import Evaluate.Environment

import Store.Fuchs




-- TODO rename to EvaluationStateCombo
data EvalStateCombo
  = EvalStateCombo Environment FuchsProgram
  deriving Show

getCurrentGraphToBeEvauated :: EvalStateCombo -> FuchsProgram
getCurrentGraphToBeEvauated (EvalStateCombo _ x) = x


data Error_Evaluating_FuchsProgram
  = Error_Program_x_CannotBeApplied_to_arguments_x_ FuchsProgram [FuchsProgram]
  | Error_TooMuchArguments_in_Apply FuchsProgram
  | Error_ScrutineeOfCaseIsNotAConstruction FuchsProgram
  | Error_CouldNotFindVariableInEnvironment FuchsProgram Environment
  | Error_CouldNotRecognizeIntrinsic FuchsProgram VariableName
  | Error_RanOutOfGas EvalStateCombo
  | Error_PatternMatchFail_ConstructorNameMatchesButTheArityDoesNotMatch Natural Natural ConstructorName [VariableName] [FuchsProgram] FuchsProgram
  deriving Show

evaluateFuchsProgram
  :: Intrinsics
  -> Gas
  -> FuchsProgram
  -> Either Error_Evaluating_FuchsProgram FuchsProgram
evaluateFuchsProgram intrinsics gas fuchsProgram = lastResult
  where
    last_intermediate_result
      = evaluateFuchsProgramToWeakHeadNormalForm_Callback
          intrinsics
          gas
          (EvalStateCombo emptyEnvironment fuchsProgram)

    lastResult = fmap getCurrentGraphToBeEvauated last_intermediate_result




---





evaluateFuchsProgramToWeakHeadNormalForm_Callback
  :: Intrinsics
  -> Gas
  -> EvalStateCombo
  -> Either
       (Error_Evaluating_FuchsProgram)
       (EvalStateCombo)
evaluateFuchsProgramToWeakHeadNormalForm_Callback
  _intrinsics
  (Gas 0)
  evalStateCombo
  = Left $ Error_RanOutOfGas evalStateCombo
evaluateFuchsProgramToWeakHeadNormalForm_Callback
  intrinsics
  gas
  evalStateCombo@(EvalStateCombo (Environment bindings) currentGraph)
  = result

  where
    result = id
      $
        let trace _ x = x in
        trace stateState
      $ do
          evaluateFuchsProgramToWeakHeadNormalForm intrinsics (burnOneGas gas) evalStateCombo

    oneStep = evaluateFuchsProgramToWeakHeadNormalForm
      intrinsics
      (Gas 0)
      evalStateCombo

    getThunk (Right (EvalStateCombo _ x)) = x
    getThunk (Left (Error_RanOutOfGas (EvalStateCombo _ x))) = x
    getThunk (Left (Error_Program_x_CannotBeApplied_to_arguments_x_ x _)) = x
    getThunk (Left (Error_TooMuchArguments_in_Apply x)) = x
    getThunk (Left (Error_ScrutineeOfCaseIsNotAConstruction x)) = x
    getThunk (Left (Error_CouldNotFindVariableInEnvironment x _)) = x
    getThunk (Left (Error_CouldNotRecognizeIntrinsic x _)) = x
    getThunk (Left (Error_PatternMatchFail_ConstructorNameMatchesButTheArityDoesNotMatch _ _ _ _ _ x)) = x

    stateState = ""
      ++ "######### BEGIN STATE ##########################################################################################################" ++ "\n"
      ++ "Environment: " ++ "\n"
      ++ (bindings >>= printEnvironment)
      ++ "#########" ++ "\n"
      ++ "current graph: " ++ uglyPrintProgram currentGraph ++ "\n"
      ++ "#########" ++ "\n"
      ++ "extracted result: " ++ "\n"
      ++ uglyPrintProgram (getThunk oneStep) ++ "\n"
      ++ "######### END STATE" ++ "\n"

    printEnvironment (VariableName bytes, programm) = ""
      ++ show bytes ++ " := " ++ "\n"
      ++ unlines (map ("  "++) (uglyLineShowProgram programm))
      ++ "\n"







-- | TODO what are the implications for incorrect global variables in lambda abstractions?
evaluateFuchsProgramToWeakHeadNormalForm
  :: Intrinsics
  -> Gas
  -> EvalStateCombo
  -> Either Error_Evaluating_FuchsProgram EvalStateCombo

-- an construct is already in weak head normal form
evaluateFuchsProgramToWeakHeadNormalForm
  _intrinsics
  _gas
  combo@(EvalStateCombo
    _localEnvironment
    (SaturatedConstruction _ _)
  )
  = return combo

-- for an apply we need to feed arguments inorder to find out what we get out of it. Should there be not enouth arguments we return the unsatisfied lambda as. If we get a saturated lambda we return the body
evaluateFuchsProgramToWeakHeadNormalForm
  intrinsics
  gas
  (EvalStateCombo
    localEnvironment
    (Apply fuchsProgram [])
  )
  = evaluateFuchsProgramToWeakHeadNormalForm_Callback intrinsics gas (EvalStateCombo localEnvironment fuchsProgram)
  --TODO should remove this case? Do performance analysis?
evaluateFuchsProgramToWeakHeadNormalForm
  intrinsics
  gas
  (EvalStateCombo
    localEnvironment
    object@(Apply fuchsProgram arguments)
  )
  = do
      let food_count = genericLength arguments

      -- the program might not be in weak head normal form.
      -- we want to decide what to do with the fuchsProgram
      -- so we need something to switch-case on
      maybeLamda
        <- evaluateFuchsProgramToWeakHeadNormalForm_Callback
            intrinsics
            gas
            (EvalStateCombo localEnvironment fuchsProgram)
      --TODO localEnvironment correct here?

      case getCurrentGraphToBeEvauated maybeLamda of
        Lambda _free_variableNames lambda_variableNames lambda_body
          -> do
              let appetite = genericLength lambda_variableNames :: Natural

              if appetite < food_count
                then Left $ Error_TooMuchArguments_in_Apply object
                else return ()

              if appetite == food_count
                then do
                  let argumentEnvironment
                        = environmentZip lambda_variableNames arguments
                  let subEnvironment
                        = argumentEnvironment `dominatesOver` localEnvironment
                  evaluateFuchsProgramToWeakHeadNormalForm_Callback intrinsics gas (EvalStateCombo subEnvironment lambda_body)
                else error $ "todo missing currying implementation" ++ show object

        StrictIntrinsic magic_variable_name
          -> do
              reducedGas
                <- mapLeft (undefined)
                $ burn_gas food_count
                $ gas

              let forcing one_argument_of_apply
                    = force_callback
                        intrinsics
                        reducedGas
                        (EvalStateCombo localEnvironment one_argument_of_apply)

              forced_arguments <- mapM forcing arguments

              let Intrinsics _environment spell_book = intrinsics

              let magic_intrinsic = lookup magic_variable_name spell_book :: Maybe Intrinsic

              case magic_intrinsic of
                Just (Intrinsic _arity (MagicSpell magic_spell)) -> do
                  let magic_result = magic_spell reducedGas forced_arguments
                  Right $ EvalStateCombo localEnvironment magic_result
                Nothing -> Left $ Error_CouldNotRecognizeIntrinsic object magic_variable_name

        notLambda -> Left $ Error_Program_x_CannotBeApplied_to_arguments_x_ notLambda arguments


evaluateFuchsProgramToWeakHeadNormalForm
  _intrinsics
  _gas
  combo@(EvalStateCombo
    _localEnvironment
    (Lambda _free_variables _bound_variables _body)
  )
  = return combo

evaluateFuchsProgramToWeakHeadNormalForm
  intrinsics
  gas
  (EvalStateCombo
    localEnvironment
    object@(Case scrutinee cases defaultPath)
  )
  = do
      maybe_construction <- evaluateFuchsProgramToWeakHeadNormalForm_Callback intrinsics gas (EvalStateCombo localEnvironment scrutinee)

      (constructorName, fullmeal)
        <- assumeItIsSaturatedConstruction
        $ getCurrentGraphToBeEvauated
        $ maybe_construction

      let casesLookupTable
            = map (\(goal,backpack,path)->(goal,(backpack,path))) cases
      let maybeChoosenPath = lookup constructorName casesLookupTable

      case maybeChoosenPath of
           Nothing
             -> evaluateFuchsProgramToWeakHeadNormalForm_Callback intrinsics gas (EvalStateCombo localEnvironment defaultPath)
           Just (variableNames, choosenPath)
             -> do
                  checkForArityInPatternMatch constructorName variableNames fullmeal

                  let newBindings = environmentZip variableNames fullmeal
                  let subBindings = newBindings `dominatesOver` localEnvironment
                  evaluateFuchsProgramToWeakHeadNormalForm_Callback intrinsics gas (EvalStateCombo subBindings choosenPath)
      where
        assumeItIsSaturatedConstruction (SaturatedConstruction constructorName fullmeal)
          = Right (constructorName, fullmeal)
        assumeItIsSaturatedConstruction notConstruction
          = Left $ Error_ScrutineeOfCaseIsNotAConstruction notConstruction

        checkForArityInPatternMatch constructorName variableNames fullmeal = result
          where
            length_new_variables = genericLength variableNames :: Natural
            length_provided_expressions_from_scrutinized_scrutinee = genericLength fullmeal :: Natural

            result = if length_new_variables == length_provided_expressions_from_scrutinized_scrutinee
                        then Right ()
                        else Left $ Error_PatternMatchFail_ConstructorNameMatchesButTheArityDoesNotMatch length_new_variables length_provided_expressions_from_scrutinized_scrutinee constructorName variableNames fullmeal object


evaluateFuchsProgramToWeakHeadNormalForm
  intrinsics@(Intrinsics intrinsicEnvironment _magicResultGenerator)
  gas
  (EvalStateCombo
    localEnvironment
    object@(Variable variableName)
  )
  = case summon of
      Just foundProgramm
        -> evaluateFuchsProgramToWeakHeadNormalForm_Callback
              intrinsics
              gas
              (EvalStateCombo localEnvironment foundProgramm)
      Nothing
        -> Left $ Error_CouldNotFindVariableInEnvironment object environment
      where
        summon = lookupInEnvironment environment variableName

        environment = intrinsicEnvironment `dominatesOver` localEnvironment


evaluateFuchsProgramToWeakHeadNormalForm
  intrinsics
  gas
  (EvalStateCombo
    localEnvironment
    (RecursiveLet bindings body)
  )
  = do
      let newBindings = Environment bindings
      let subBindings = newBindings `dominatesOver` localEnvironment

      evaluateFuchsProgramToWeakHeadNormalForm_Callback
        intrinsics
        gas
        (EvalStateCombo subBindings body)


evaluateFuchsProgramToWeakHeadNormalForm
  _intrinsics
  _gas
  combo@(EvalStateCombo
    _localEnvironment
    (Trap _errorCase)
  )
  = Right combo


evaluateFuchsProgramToWeakHeadNormalForm
  _intrinsics
  _gas
  combo@(EvalStateCombo
    _localEnvironment
    (OpaqueNativeObject _bindings)
  )
  = Right combo

evaluateFuchsProgramToWeakHeadNormalForm
  _intrinsics
  _gas
  combo@(EvalStateCombo
    _localEnvironment
    (StrictIntrinsic _magic_variable_name)
  )
  = Right combo


evaluateFuchsProgramToWeakHeadNormalForm
  _intrinsics
  _gas
  (EvalStateCombo
    _localEnvironment
    object
  )
  = error
  $ "not considered case in evaluateFuchsProgramToWeakHeadNormalForm: "
  ++ show object


---




data MagicSpell
  = MagicSpell (Gas -> [FuchsProgram] -> FuchsProgram)

instance Show MagicSpell where
  show (MagicSpell _) = "MagicSpell"

data Intrinsic
  = Intrinsic Arity MagicSpell
  deriving Show

data Intrinsics
  = Intrinsics Environment [(VariableName, Intrinsic)]
  deriving Show


emptyIntrinsicEnvironment :: Intrinsics
emptyIntrinsicEnvironment = Intrinsics emptyEnvironment []




---




-- TODO swap names of force and force_callback? do for other eval function as well
-- | begin calling with force_callback, do not call force
force_callback
  :: Intrinsics
  -> Gas
  -> EvalStateCombo
  -> Either Error_Evaluating_FuchsProgram FuchsProgram
force_callback intrinsics gas evalStateCombo = do
  -- first we do a normal weak head evaluation

  -- here we burn gas
  let reduced_gas = burnOneGas gas

  second_evalStateCombo
    <- evaluateFuchsProgramToWeakHeadNormalForm_Callback
        intrinsics
        reduced_gas
        evalStateCombo

  -- TODO why is second_evalStateCombo used?
  result <- force intrinsics (burnOneGas gas) second_evalStateCombo
  return result


force
  :: Intrinsics
  -> Gas
  -> EvalStateCombo
  -> Either Error_Evaluating_FuchsProgram FuchsProgram
force
  intrinsics
  gas
  (EvalStateCombo
    evalualtion_environment
    (SaturatedConstruction constructor arguments)
  )
  = do
      forced_arguments <- mapM sub_force_same_environment arguments

      let forcedGraph
            = SaturatedConstruction
                constructor
                forced_arguments

      return forcedGraph
  where
    sub_force_same_environment
      = sub_force intrinsics gas evalualtion_environment




-- TODO why is variable not evaluated?
force
  intrinsics
  gas
  combo@(EvalStateCombo
    _evaluation_environment
    (Variable _)
  )
  = force_callback intrinsics gas combo

force
  _intrinsics
  _gas
  (EvalStateCombo
    _evaluation_environment
    currentGraph@(Lambda _globals _bounded _body)
  )
  = return currentGraph

force
  _intrinsics
  _gas
  (EvalStateCombo
    _evaluation_environment
    currentGraph@(OpaqueNativeObject _bindings)
  )
  = return currentGraph

force
  _intrinsics
  _gas
  (EvalStateCombo
    _evaluation_environment
    currentGraph@(StrictIntrinsic _variable_name)
  )
  = return currentGraph

force
  intrinsics
  gas
  (EvalStateCombo
    evalualtion_environment
    (Trap payload_expression)
  )
  = do
    forced_graph <- sub_force_same_environment payload_expression
    return $ Trap forced_graph
  where
    sub_force_same_environment
      = sub_force intrinsics gas evalualtion_environment


force
  intrinsics
  gas
  (EvalStateCombo environment currentGraph)
  = error $ "not considered case in force: " ++ show (currentGraph, environment, gas, intrinsics)



-- no burning gas here because callback is doing that
sub_force
  :: Intrinsics
  -> Gas
  -> Environment
  -> FuchsProgram
  -> Either Error_Evaluating_FuchsProgram FuchsProgram
sub_force intrinsics gas evalualtion_environment fuchsProgram
  = force_callback intrinsics gas
  $ EvalStateCombo evalualtion_environment fuchsProgram

