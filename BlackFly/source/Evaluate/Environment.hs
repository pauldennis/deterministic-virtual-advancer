module Evaluate.Environment where


import Numeric.Natural
import Data.List
import Safe --TODO compile out assertNote

import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State

import Store.Fuchs


newtype Environment
  = Environment [(,) VariableName FuchsProgram]
  deriving Show

emptyEnvironment :: Environment
emptyEnvironment = Environment []

environmentZip
  :: [VariableName]
  -> [FuchsProgram]
  -> Environment
environmentZip variableNames fuchsPrograms
  = assertNote note check result
  where
    note = show ("lengths suposed to be equal", length_1, length_2)

    length_1 = genericLength variableNames :: Natural
    length_2 = genericLength fuchsPrograms

    check = length_1 == length_2
    result = Environment $ zip variableNames fuchsPrograms

dominatesOver
  :: Environment
  -> Environment
  -> Environment
dominatesOver (Environment newBindings) (Environment alreadyExistingBindings)
  = Environment
  $ newBindings ++ alreadyExistingBindings

lookupInEnvironment
  :: Environment
  -> VariableName
  -> Maybe FuchsProgram
lookupInEnvironment (Environment bindings) variableName
  = lookup variableName bindings
