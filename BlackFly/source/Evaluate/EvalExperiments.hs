module New.EvalExperiments where

-- https://github.com/Kindelia/HVM/blob/master/HOW.md#lambda-duplication

doubleTheWork
  = let f = ((\x -> \y -> ((,) (timeConsumingWork x) y)) 1000000000)
    in ((,) (f 10) (f 20))

timeConsumingWork n = length [1..n]

