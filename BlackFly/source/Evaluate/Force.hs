module Evaluate.Force where

import Control.Monad.Trans.Except
import qualified Control.Monad.State as State

import Trifle.Rename

import Compiler.Backend.Compiler_FuchsProgram_to_FuchsReceipt
import LowLevelFuchsAndStateRepresentation.FuchsProgram_and_State
import Evaluate.Gas
import Evaluate.Environment

import Store.Tangle
import Store.Fuchs

import Evaluate.Evaluation


strict_evaluation_of_FuchsReceipt
  :: Intrinsics
  -> Gas
  -> FuchsReceipt
  -> ExceptT Error_Evaluating_FuchsReceipt (State.State TangleStore) FuchsReceipt
strict_evaluation_of_FuchsReceipt intrinsics gas
  = embedPureEvaluationInStore (evaluateFuchsProgram intrinsics gas)



-- put this function here, to save as space in Evaluate.hs
-- TODO put back to proper place
data Error_Evaluating_FuchsReceipt
  = Error_WhileDestoringProgram (Error_FuchsStore)
  | Error_WhileEvaluatingFuchsProgram (Error_Evaluating_FuchsProgram)
  deriving Show

embedPureEvaluationInStore
  :: (FuchsProgram -> Either Error_Evaluating_FuchsProgram FuchsProgram)
  -> FuchsReceipt
  -> ExceptT
      Error_Evaluating_FuchsReceipt
      (State.State TangleStore)
      Store.Fuchs.FuchsReceipt
embedPureEvaluationInStore evaluate old_fuchsReceipt = do
  fullProgrammInMemory
    <- mapException Error_WhileDestoringProgram
    $ destoreProgram
    $ old_fuchsReceipt

  headNormalFormProgram
    <- mapException Error_WhileEvaluatingFuchsProgram
    $ ExceptT $ return --TODO library function?
    $ evaluate
    $ fullProgrammInMemory

  new_fuchsReceipt
    <- State.lift
    $ storeProgram headNormalFormProgram
  return new_fuchsReceipt




force_evaluation_of_FuchsReceipt
  :: Intrinsics
  -> Gas
  -> FuchsReceipt
  -> ExceptT Error_Evaluating_FuchsReceipt (State.State TangleStore) FuchsReceipt
force_evaluation_of_FuchsReceipt intrinsics gas
  = embedPureEvaluationInStore
      (forceEvaluateFuchsProgram intrinsics gas)



forceEvaluateFuchsProgram
  :: Intrinsics
  -> Gas
  -> FuchsProgram
  -> Either Error_Evaluating_FuchsProgram FuchsProgram
forceEvaluateFuchsProgram intrinsics gas fuchsProgram
  = lastResult
  where
    last_intermediate_result
      = force_callback --begin with callback, because this first evaluated to weak head normal form
          intrinsics
          gas
          (EvalStateCombo emptyEnvironment fuchsProgram)

    lastResult = last_intermediate_result
