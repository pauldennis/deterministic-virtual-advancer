module Evaluate.Gas where

import Numeric.Natural


data Gas
  = Gas Natural
  | UnlimitedGas
  deriving Show


burnOneGas :: Gas -> Gas
burnOneGas (Gas gas) = Gas $ gas - 1 --TODO use decrement, TODO wrap in maybe monad
burnOneGas UnlimitedGas = UnlimitedGas


data Error_CannotBurn_x_Gas_because_only_got_x
  = Error_CannotBurn_x_Gas_because_only_got_x Natural Gas

burn_gas
  :: Natural
  -> Gas
  -> Either
      Error_CannotBurn_x_Gas_because_only_got_x
      Gas
burn_gas _ UnlimitedGas = return UnlimitedGas
burn_gas gas_planned_to_burn (Gas available_gas)
  = if gas_planned_to_burn <= available_gas
      then return $ Gas $ available_gas - gas_planned_to_burn
      else Left $ Error_CannotBurn_x_Gas_because_only_got_x gas_planned_to_burn (Gas available_gas)
