module Evaluate.IntrinsicSignature where

import Data.Either.Combinators
import Data.ByteString
import Data.ByteString.UTF8
import Data.ByteString.Lazy
import Data.List
import Data.Binary

import Numeric.Natural

import qualified Compiler.Frontend.UnicodeSubsets
import Store.Hash
import Store.Fuchs
import Compiler.Frontend.UsingTheParser
import Compiler.Frontend.FunctionName
import CleanParser.Token

-- import Evaluate.Environment

newtype Arity
  = Arity Natural
  deriving Show

newtype ExplanatoryText
  = ExplanatoryText String
  deriving Show

data IntrinsicSignatureIngredient
  = IntrinsicSignatureIngredient String ExplanatoryText Arity
  deriving Show

data IntrinsicSignature
  = IntrinsicSignature VariableName IntrinsicSignatureIngredient

getExpectedMagicVariableName :: IntrinsicSignature -> VariableName
getExpectedMagicVariableName
  (IntrinsicSignature variableName _)
  = variableName

-- TODO have some tooling to allow to browse the letter of builtins
makeIntrinsicSignature
  :: String
  -> ExplanatoryText
  -> Arity
  -> Either
      Error_ExpectedInvalidIntrinsicSignature
      IntrinsicSignature
makeIntrinsicSignature
  customary
  explanatoryText
  arity
  = do
      expectedVariableName
        <- make_out_expected_intrinsic_name raw_intrinsic_signature_ingredients
      let variableName = constructVariableName expectedVariableName
      return
        $ IntrinsicSignature
            variableName
            raw_intrinsic_signature_ingredients
  where
    raw_intrinsic_signature_ingredients
      = IntrinsicSignatureIngredient
          customary
          explanatoryText
          arity


-- TODO more compact name?
calculate_identification_hash
  :: IntrinsicSignatureIngredient
  -> String
calculate_identification_hash
  (IntrinsicSignatureIngredient customary (ExplanatoryText explanatoryText) (Arity arity))
  = result
  where
    result = identification_hash

    invalid_utf8_bytesequence = Data.ByteString.pack [maxBound]
    customary_as_bytes = fromString customary
    explanatoryText_as_bytes = fromString explanatoryText
    arity_as_bytes = encode arity

    invalid_utf8_sequence_combination
      = customary_as_bytes
      `Data.ByteString.append` invalid_utf8_bytesequence
      `Data.ByteString.append` explanatoryText_as_bytes
      `Data.ByteString.append` invalid_utf8_bytesequence
      `Data.ByteString.append` (toStrict arity_as_bytes)

    identification_hash
      = show
      $ currently_used_hash_algorithm
      $ invalid_utf8_sequence_combination

data Error_ExpectedInvalidIntrinsicSignature
  = Error_Customary_is_empty String
  | Error_ExplanatoryText_x_Did_not_start_with_x String String
  | Error_NotSupposedToBeginWithModificator String
  | Error_CouldNotParse_Customary_AsVariableName
      (Error_EatingFunctionNameDidNotWork NumberedToken)
  | Error_Compiled_End_Result_Is_Not_A_Valid_Variable_Name
      (Error_EatingFunctionNameDidNotWork NumberedToken)
  | Error_FileHasRemnant String
  | Error_ExplanationTest_x_DoesNotContainCustomary_x_InTicks_See_x_ String String String
  | Error_YouAreSupposedToWriteSomeNonAutomatedTexthere String
  | Error_the_explanation_text_x_is_supposed_to_mention_the_customary_x_ String String
  | Error_YouAreSupposedToWriteSomeNonAutomatedTextHere String
  | Error_the_explanation_text_x_is_supposed_to_contain_the_arity_x_ Arity
  deriving Show

make_out_expected_intrinsic_name
  :: IntrinsicSignatureIngredient
  -> Either Error_ExpectedInvalidIntrinsicSignature String
make_out_expected_intrinsic_name
  intrinsic@(IntrinsicSignatureIngredient customary (ExplanatoryText explanatoryText) arity@(Arity numberOfArguments))
  = do
      case customary of
        [] -> Left $ Error_Customary_is_empty $ explanatoryText
        _ -> Right ()

      do
        let prefix = "Dear Intrinsic User,\n\n"
        if Data.List.isPrefixOf prefix explanatoryText
          then Right ()
          else Left
            $ Error_ExplanatoryText_x_Did_not_start_with_x explanatoryText
            $ prefix

      if Data.List.last explanatoryText == '\n'
        then Right ()
        else Left $ Error_FileHasRemnant
              $ Data.List.reverse
              $ Data.List.takeWhile (/= '\n')
              $ Data.List.reverse
              $ explanatoryText

      if Data.List.isInfixOf customary explanatoryText
        then Right ()
        else Left
          $ Error_the_explanation_text_x_is_supposed_to_mention_the_customary_x_ explanatoryText customary

      if Data.List.isInfixOf (show numberOfArguments) explanatoryText
        then Right ()
        else Left
          $ Error_the_explanation_text_x_is_supposed_to_contain_the_arity_x_ arity

      id $ let customary_in_ticks = "`" ++ customary ++ "`"
        in if Data.List.isInfixOf customary_in_ticks explanatoryText
          then Right ()
          else Left
            $ Error_ExplanationTest_x_DoesNotContainCustomary_x_InTicks_See_x_ explanatoryText customary customary_in_ticks

      if explanatoryText /= ("Dear Intrinsic User,\n\n"++"`"++customary++"`"++(show numberOfArguments)++"\n")
        then Right ()
        else Left
          $ Error_YouAreSupposedToWriteSomeNonAutomatedTextHere explanatoryText

      if explanatoryText /= ("Dear Intrinsic User,\n\n"++"`"++customary++"`\n"++(show numberOfArguments)++"\n")
        then Right ()
        else Left
          $ Error_YouAreSupposedToWriteSomeNonAutomatedTextHere explanatoryText

      if explanatoryText /= ("Dear Intrinsic User,\n\n"++"`"++customary++"`\n"++(show numberOfArguments)++"\n"++[Data.List.head customary]++"\n")
        then Right ()
        else Left
          $ Error_YouAreSupposedToWriteSomeNonAutomatedTextHere explanatoryText

      if explanatoryText /= ("Dear Intrinsic User,\n\n"++"`"++customary++"`"++(show numberOfArguments)++[Data.List.head customary]++"\n")
        then Right ()
        else Left
          $ Error_YouAreSupposedToWriteSomeNonAutomatedTextHere explanatoryText

      if explanatoryText /= ("Dear Intrinsic User,\n\n"++"`"++customary++"`"++[Data.List.head customary]++"\n")
        then Right ()
        else Left
          $ Error_YouAreSupposedToWriteSomeNonAutomatedTextHere explanatoryText



      -- TODO this is actually incorrect. This predicat is actually supposed to be parsed in the frontend
      _
        <- mapLeft Error_NotSupposedToBeginWithModificator
        $ if not $ Data.List.elem Compiler.Frontend.UnicodeSubsets.inverted_question_mark customary
            then Right undefined
            else Left customary

      _
        <- mapLeft Error_CouldNotParse_Customary_AsVariableName
        $ parse_to_FunctionNameAsSyntax customary

      let hash
            = calculate_identification_hash
            $ intrinsic

      let expected_concatenation
            = [Compiler.Frontend.UnicodeSubsets.inverted_question_mark]
            ++ hash
            ++ [Compiler.Frontend.UnicodeSubsets.underscore]
            ++ customary

      _
        <- mapLeft Error_Compiled_End_Result_Is_Not_A_Valid_Variable_Name
        $ parse_to_FunctionNameAsSyntax expected_concatenation

      return expected_concatenation


test_intrinsic_names :: (String, Bool)
test_intrinsic_names
  = (,) "test_intrinsic_names"
  $ and
  $ Prelude.tail [undefined
  , isRight
      $ make_out_expected_intrinsic_name
      (IntrinsicSignatureIngredient "test" (ExplanatoryText "Dear Intrinsic User,\n\n`test`\n(Arity 0)\n") (Arity 0))
  , isLeft
      $ make_out_expected_intrinsic_name
      (IntrinsicSignatureIngredient "test" (ExplanatoryText "test") (Arity 0))
  , isLeft
      $ make_out_expected_intrinsic_name
      (IntrinsicSignatureIngredient "¿" (ExplanatoryText "Dear Intrinsic User,\n\n`test`\n(Arity 0)\n") (Arity 0))
  , isLeft
      $ make_out_expected_intrinsic_name
      (IntrinsicSignatureIngredient "" (ExplanatoryText "Dear Intrinsic User,\n\nempty test string\n") (Arity 0))
  , isRight
      $ make_out_expected_intrinsic_name
      (IntrinsicSignatureIngredient "_s" (ExplanatoryText "Dear Intrinsic User,\n\n`_s` test string\n0\n") (Arity 0))
  ]
