module ExampleFuchsProgramms.AsHaskellConstant.AllExamples where


import ExampleFuchsProgramms.AsHaskellConstant.Anatomy
import ExampleFuchsProgramms.AsHaskellConstant.ConnectivityDriver
import ExampleFuchsProgramms.AsHaskellConstant.EvaluateExamples
import ExampleFuchsProgramms.AsHaskellConstant.ToggleFlag
import ExampleFuchsProgramms.AsHaskellConstant.UntypedUpdateChannel
import ExampleFuchsProgramms.AsHaskellConstant.UpdateChannel


allExamplesNames :: [String]
allExamplesNames =
  [ "Anatomy"
  , "ConnectivityDriver"
  , "EvaluateExamples"
  , "ToggleFlag"
  , "UntypedUpdateChannel"
  , "UpdateChannel"
  ]


allExamples :: [String]
allExamples =
  [ anatomy
  , connectivitydriver
  , evaluateexamples
  , toggleflag
  , untypedupdatechannel
  , updatechannel
  ]
