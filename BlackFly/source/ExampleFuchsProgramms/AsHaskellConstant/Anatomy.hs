module ExampleFuchsProgramms.AsHaskellConstant.Anatomy where


anatomy :: String
anatomy = ""
  ++ "" ++ "\n"
  ++ "lambda := argument1 argument2 argument3 |-> lambda_body" ++ "\n"
  ++ "" ++ "\n"
  ++ "switchcase := switch scrutinee cases" ++ "\n"
  ++ "                Constructor backpack1 backpack2 backpack3 |-> path" ++ "\n"
  ++ "" ++ "\n"
  ++ "application := function argument1 argument2 argument3" ++ "\n"
  ++ "" ++ "\n"
  ++ "letbinding := let introducedVariable := exisitingTerm in let_binding_body" ++ "\n"
