import System.Directory
import Data.Char
import Data.List

inputFolder = "./fuchs/"
outputFolder = "./AsHaskellConstant/"
moduleHierarchyBoilerplate = "ExampleFuchsProgramms.AsHaskellConstant."
allAllExamplesModuleName = "AllExamples"


fmap2 = fmap . fmap

main = do
  print "convert fuchs files haskell modules"

  entries <- fmap sort $ listDirectory inputFolder

  if elem (map toLower allAllExamplesModuleName) (fmap2 toLower entries)
     then error $ "dont want to have file with the reserved name: " ++ show allAllExamplesModuleName
     else return ()

  mapM_ writeAshaskellModule entries

  writeFile (outputFolder ++ allAllExamplesModuleName ++ ".hs" ) (combineInOneModuleContent entries)

writeAshaskellModule fileName = do
  fileContent <- readFile (inputFolder ++ fileName)

  let serializedContent = asHaskellModule fileName fileContent

  writeFile (outputFolder ++ fileName ++ ".hs") serializedContent

asHaskellModule fileName fileContent = result
  where
    result = ""
      ++ header
      ++ unlines serializedLines
      ++ ""

    header = ""
      ++ "module "++moduleHierarchyBoilerplate++fileName++" where" ++"\n"
      ++ "\n"
      ++ "\n"
      ++ (map toLower fileName) ++ " :: String" ++ "\n"
      ++ (map toLower fileName) ++ " = \"\"" ++ "\n"

    serializedLines
      = id
      $ map (++ " ++ " ++ show "\n" )
      $ map ("  ++ "++)
      $ map show
      $ lines
      $ fileContent

combineInOneModuleContent fileNames = result
  where
    result = ""
      ++ "module "++moduleHierarchyBoilerplate++allAllExamplesModuleName++" where" ++"\n"
      ++ "\n"
      ++ "\n"
      ++ (fileNames >>= importModule)
      ++ "\n"
      ++ "\n"
      ++ "allExamplesNames :: [String]\n"
      ++ "allExamplesNames =\n  [ \""++serializedFilenesAsStrings++"\"\n  ]\n"
      ++ "\n"
      ++ "\n"
      ++ "allExamples :: [String]\n"
      ++ "allExamples =\n  [ "++serializedFilenesLowerCase++"\n  ]\n"

    serializedFilenesLowerCase = intercalate "\n  , " (fmap2 toLower fileNames)

    serializedFilenesAsStrings = intercalate "\"\n  , \"" fileNames

    importModule filename = result
      where
        result = "import " ++ moduleHierarchyBoilerplate ++ filename ++"\n"
