module CleanParser.LexerLike where

import Data.Either

import CleanParser.Core
import CleanParser.ActualUsingTheParser
import CleanParser.LowLevelBuildingBlocks

data Error_CompleteParseError errorCase token output
  = Error_CompleteParserDidNotWork errorCase
  | Error_CompleteParserDidNotEatEverything_got_x_but_x_remains output [token]
  deriving Show

data CompleteParseResult output
  = CompleteParseResult output
  deriving Show

completeParse
  :: Parser token errorCase output
  -> Parser token (Error_CompleteParseError errorCase token output) (CompleteParseResult output)
completeParse parser = fmap CompleteParseResult $ do
  output <- fmapOnParserError Error_CompleteParserDidNotWork parser
  fmapOnParserError (refineError output) (checkForEndOfFile output)
  where
    refineError output (Error_UnexpectedContinuationOfFile remnant)
      = Error_CompleteParserDidNotEatEverything_got_x_but_x_remains output remnant

test_completeParse :: (String, Bool)
test_completeParse = (,) "complete parse" $ and [ True
  , isRight $ runParser (completeParse (succeedWith ())) ""
  , isLeft $ runParser (completeParse (failWith ())) ""
  , isLeft $ runParser (completeParse (succeedWith ())) "-"
  , isRight $ runParser (completeParse eatOneToken) "-"
  ]
