module CleanParser.RenameInterfaceBehaviour where

import Control.Applicative

import CleanParser.Core

doTwoParser :: (Parser t f o) -> (Parser t f p) -> (Parser t f (o,p))
doTwoParser = liftA2 (,)

parseAllInSequence :: [Parser t f o] -> Parser t f [o]
parseAllInSequence = sequenceA

addParser :: [Parser t f [o]] -> Parser t f [o]
addParser = fmap concat . sequenceA
