module CleanParser.Token where

data NumberedToken = Token Integer Char
  deriving Show

class Show a => CharacterRepresentable a where
  getCharacter :: a -> Char

instance CharacterRepresentable NumberedToken where
  getCharacter (Token _ x) = x

instance CharacterRepresentable Char where
  getCharacter = id

doesTokenhaveAttribute :: Char -> NumberedToken -> Bool
doesTokenhaveAttribute char token = char == getCharacter token
