module CleanParser.Core where

import Safe.Partial
import GHC.Stack

import Control.Applicative
import Data.Either.Combinators

newtype Input token = Input [token]

newtype Remnant token = Remnant [token]
  deriving (Show)

newtype Parser token falsity output
  = Parser (Input token -> Either falsity (Remnant token, output))

unwrapParser
  :: Parser token falsity output
  -> Input token
  -> Either falsity (Remnant token, output)
unwrapParser (Parser parserFunction) = parserFunction

getRemnant :: (Remnant token, output) -> Remnant token
getRemnant = fst

getOutput :: (Remnant token, output) -> output
getOutput = snd

getOutputComplete :: Partial => (Remnant token, output) -> output
getOutputComplete (Remnant [], output) = output
getOutputComplete (Remnant _, _output) = withFrozenCallStack $ error "the parser did not eat everything"


--TODO check all laws
instance Functor (Parser token falsity) where
  fmap f (Parser parser) = Parser $ fmap3 f parser
    where
      fmap3 = fmap . fmap . fmap

fmapOnParserError
  :: (falsity1 -> falsity2)
  -> (Parser token falsity1 output)
  -> (Parser token falsity2 output)
fmapOnParserError f (Parser parser) = Parser result
  where
    result input = mapLeft f $ parser input

instance Applicative (Parser token falsity) where
  pure thing = Parser $ \(Input queue) -> Right (Remnant queue, thing)
  (Parser bigParser) <*> (Parser parser) = Parser result
    where
      result input = do--Either
        --TODO find out proper order of the next two lines:
        (Remnant afterGettingFunction, temporaryFunction)
          <- bigParser input
        (remnant, tmpOutput)
          <- parser $ Input afterGettingFunction
        return (remnant, temporaryFunction tmpOutput)

instance Monad (Parser token falsity) where
  (Parser parser) >>= f = Parser result
    where
      result inputTokenStream = do--Either
        temporary <- parser inputTokenStream

        let temporaryResult = getOutput temporary
        let Remnant temporaryStringRest = getRemnant temporary

        let parameterizedParser = f temporaryResult

        unwrapParser parameterizedParser (Input temporaryStringRest)

--TODO implement elegantly
instance (Monoid falsity) => Alternative (Parser token falsity) where
  empty = Parser $ const $ Left mempty
  (Parser parser1) <|> (Parser parser2) = Parser result
    where
      result = mapLeft (uncurry (<>)) . first
      first input = firstRight (parser1 input) (parser2 input)

      firstRight :: (Either a1 b) -> (Either a2 b) -> (Either (a1, a2) b)
      firstRight (Right x) _ = Right x
      firstRight _ (Right y) = Right y
      firstRight (Left x) (Left y) = Left (x,y)
