module CleanParser.UnicodeAware where

import Data.Either

import CleanParser.Core
import CleanParser.Basic
import CleanParser.RenameInterfaceBehaviour
import CleanParser.Token
import CleanParser.LowLevelBuildingBlocks
import CleanParser.ActualUsingTheParser


data EatExpectedCharacterError token
  = CannotEatTokenWithCharacter_x_BecauseEndOfFile Char
  | UnsuccessfulTriedToMatchChararecter_x_WithToken_x_ Char token
  deriving (Show)

eatExpectedCharacter
  :: CharacterRepresentable token
  => Char
  -> Parser token (EatExpectedCharacterError token) token
eatExpectedCharacter char
  = fmapOnParserError refineError
  $ eatExpectedToken $ (char ==) . getCharacter
  where
    refineError CannotEatExpectedTokenBecauseEndOfFile
      = CannotEatTokenWithCharacter_x_BecauseEndOfFile char
    refineError (TokenNotExpected token)
      = UnsuccessfulTriedToMatchChararecter_x_WithToken_x_ char token


data EatNotExpectedCharacterError token
  = CannotNotMatchTokenWithCharacter_x_BecauseEndOfFile Char
  | Match_x_with_x_WasNotAllowed Char token
  deriving (Show)

eatExpectedNotCharacter
  :: CharacterRepresentable token
  => Char
  -> Parser token (EatNotExpectedCharacterError token) token
eatExpectedNotCharacter char
  = fmapOnParserError refineError
  $ eatExpectedToken $ (char /=) . getCharacter
  where
    refineError CannotEatExpectedTokenBecauseEndOfFile
      = CannotNotMatchTokenWithCharacter_x_BecauseEndOfFile char
    refineError (TokenNotExpected token)
      = Match_x_with_x_WasNotAllowed char token

eatString
  :: CharacterRepresentable token
  => String
  -> Parser token (EatExpectedCharacterError token) [token]
eatString string = result
  where
    result = parseAllInSequence $ map eatExpectedCharacter string


eatOneCharOfString
  :: CharacterRepresentable a
  => [Char]
  -> Parser a (Error_EatTokenWithAttribute a Char) a
eatOneCharOfString
  = eatTokenWithOneOfSomeAttributes
  $ ((==) . getCharacter)


data Error_LineParser contentParserError lineEndingError
  = ErrorInLineContentParser contentParserError
  | ErrorInLineEnding lineEndingError
  deriving Show

data TheNewLineSymbol token
  = TheNewLineSymbol token
  deriving Show

-- a record can have newlines in it
data Record lineContent newlineSymbol
  = Record lineContent (TheNewLineSymbol newlineSymbol)
  deriving Show

eatLine
  :: CharacterRepresentable token
  => Parser token errorCase output
  -> Parser token (Error_LineParser errorCase (EatExpectedCharacterError token)) (Record output token)
eatLine lineContentParser = do
  lineContent
    <- fmapOnParserError ErrorInLineContentParser lineContentParser
  newlineSymbol
    <- fmapOnParserError ErrorInLineEnding $ eatExpectedCharacter '\n'
  return $ Record lineContent (TheNewLineSymbol newlineSymbol)

test_eatLine :: (String, Bool)
test_eatLine = (,) "line parser" $ and [ True
  , isLeft $ runParser parser ""
  , isLeft $ runParser parser "#"
  , isLeft $ runParser parser "##"
  , isLeft $ runParser parser "###"
  , isRight $ runParser parser "\n"
  , isRight $ runParser parser "#\n"
  , isRight $ runParser parser "##\n"
  , isRight $ runParser parser "###\n"
  ]
  where
    parser = (eatLine (greedilyRepeatParser (eatExpectedCharacter '#')))
