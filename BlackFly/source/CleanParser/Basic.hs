module CleanParser.Basic where

import Control.Applicative
import Control.Exception
import Data.List
import Safe

import CleanParser.Core
import CleanParser.Error
import CleanParser.RenameInterfaceBehaviour
import CleanParser.LowLevelBuildingBlocks

failIfWith
  :: Bool
  -> falsity
  -> Parser token falsity ()
failIfWith condition errorMessage
  = if condition
       then failWith errorMessage
       else return ()

parserAssert
  :: Bool
  -> falsity
  -> Parser token falsity ()
parserAssert condition errorMessage
  = if condition
       then return ()
       else failWith errorMessage

firstSuccessfulParser
  :: (Foldable t, Monoid falsity)
  => t (Parser token falsity a)
  -> Parser token falsity a
firstSuccessfulParser = foldr (<|>) $ failWith mempty

-- because I can
firstWorkingParser'
  :: (Foldable t, Monoid (m a1), Monad m, Functor t)
  => t (Parser token a1 a2)
  -> Parser token (m a1) a2
firstWorkingParser' parsers
  = firstSuccessfulParser
  $ fmap (fmapOnParserError return)
  $ parsers

-- to save the innocent: easier types
firstWorkingParser :: [Parser t f o] -> Parser t [f] o
firstWorkingParser = firstWorkingParser'

-- TODO maybe do this directly and move this function in the LowLevelBuildingBlocks
eitherAorB
  :: Parser t f1 o
  -> Parser t f2 o
  -> Parser t (f1, f2) o
eitherAorB parserA parserB
  = fmapOnParserError (\[Left x, Right y] -> (x,y))
  $ firstWorkingParser
  [ fmapOnParserError Left parserA
  , fmapOnParserError Right parserB
  ]

data EatAndCheckOutputError error output
  = UnexpectedParserErrorWhileEatingForReview error
  | UnexpectedOutput output
  deriving (Eq, Show)

-- | do a parser that yields output but fail if predicate is false
eatAndReviewOutput
  :: (output -> Bool)
  -> Parser token falsity output
  -> Parser token (EatAndCheckOutputError falsity output) output
eatAndReviewOutput predicate parser = do
  output <- wrapErrorCase parser
  if predicate output
     then return output
     else failWith $ UnexpectedOutput output
  where
    wrapErrorCase
      = fmapOnParserError UnexpectedParserErrorWhileEatingForReview


data EatExpectedTokenError token
  = CannotEatExpectedTokenBecauseEndOfFile
  | TokenNotExpected token
  deriving (Show)

eatExpectedToken
  :: (o -> Bool)
  -> Parser o (EatExpectedTokenError o) o
eatExpectedToken isTokenExpected
  = fmapOnParserError refineError
  $ eatAndReviewOutput isTokenExpected eatOneToken
  where
    refineError (UnexpectedParserErrorWhileEatingForReview ExpectedATokenButGotEndOfFile) = CannotEatExpectedTokenBecauseEndOfFile
    refineError (UnexpectedOutput token) = TokenNotExpected token


data EatExpectedNotTokenError token
  = CannotEatExpectedNotTokenBecauseEndOfFile
  | NotTokenDidIndeedAppear token
  deriving (Show)

eatExpectedNotToken
  :: (a -> Bool) -> Parser a (EatExpectedNotTokenError a) a
eatExpectedNotToken isTokenExpected
  = fmapOnParserError replaceError
  $ eatExpectedToken (not . isTokenExpected)
  where
    replaceError CannotEatExpectedTokenBecauseEndOfFile = CannotEatExpectedNotTokenBecauseEndOfFile
    replaceError (TokenNotExpected token) = NotTokenDidIndeedAppear token

eatExpectedTokenString
  :: (character -> o -> Bool)
  -> [character]
  -> Parser o (EatExpectedTokenError o) [o]
eatExpectedTokenString hasAttribute string
  = parseAllInSequence
  $ map (eatExpectedToken . hasAttribute)
  $ string


data Error_EatTokenWithAttribute token attribute
  = ListOfAttributesIsEmptyAndEndOfFileAsWell
  | ListOfAttributesIsEmptyThereforeCannotMatchToken token
  | CannotFindTokenWithOneOfTheAttributes_x_BecauseEndOfFile [attribute]
  | TheFoundToken_x_DoesFullfillAnyOfTheAllowedProperties_x_ token [attribute]
  deriving (Show)

eatTokenWithOneOfSomeAttributes
  :: (Eq attribute, Show attribute)
  => (token -> attribute  -> Bool)
  -> [attribute]
  -> Parser token (Error_EatTokenWithAttribute token attribute) token
eatTokenWithOneOfSomeAttributes
  doesTokenHaveAttribute
  possibleAttributes
  = assertNote note check
  $ usedParser
  where
    note
      | not check1 = "no attributes provided"
      | not check2 = "possible attributes are not nubbed" ++ show possibleAttributes
      | otherwise = undefined

    check :: Bool
    check = True
      && check1
      && check2

    check1 = (not . null) possibleAttributes
    check2 = nub possibleAttributes == possibleAttributes

    parsers = fmap parser possibleAttributes

    parser
      = id
      . fmapOnParserError (\x -> [x]) --TODO use firstWorkingParser'
      . eatExpectedToken
      . flip doesTokenHaveAttribute

    usedParser
      = case possibleAttributes of
             [] -> tryToGiveRicherErrorMessage
             _ -> fmapOnParserError refineErrorMessage
                    $ firstSuccessfulParser parsers

    tryToGiveRicherErrorMessage = do
      token <- fmapOnParserError refine eatOneToken
      assert checkThatNoAttributesAreProvided
        $ failWith
        $ ListOfAttributesIsEmptyThereforeCannotMatchToken token
      where
        checkThatNoAttributesAreProvided = null possibleAttributes

    refine ExpectedATokenButGotEndOfFile
      = ListOfAttributesIsEmptyAndEndOfFileAsWell

    --generalisation opportunity:
    refineErrorMessage (errorCase:_)
      = case errorCase of
             TokenNotExpected token -> TheFoundToken_x_DoesFullfillAnyOfTheAllowedProperties_x_ token possibleAttributes
             CannotEatExpectedTokenBecauseEndOfFile -> CannotFindTokenWithOneOfTheAttributes_x_BecauseEndOfFile possibleAttributes
    refineErrorMessage _ = undefined

newtype TheAtLeastOnceWittness output
  = TheAtLeastOnceWittness output
  deriving Show

data GreedilyRepeatParserResultAtLeastOnce output wouldBeError
  = GreedilyRepeatParserResultAtLeastOnce
      (TheAtLeastOnceWittness output)
      (GreedilyRepeatOutput [output])
      (TheErrorThatWouldHaveHappenedWhenContinuing wouldBeError)
  deriving Show

greedilyRepeatParserAtLeastOnce
  :: Parser token wouldBeError output
  -> Parser token wouldBeError
      (GreedilyRepeatParserResultAtLeastOnce output wouldBeError)
greedilyRepeatParserAtLeastOnce parser = do
  firstResult <- parser
  GreedilyRepeatParserResult
    (GreedilyRepeatOutput optionalRemainingResults)
    (TheErrorThatWouldHaveHappenedWhenContinuing errorIfWouldHaveContinued)
    <- absurdError $ greedilyRepeatParser parser
  return $ GreedilyRepeatParserResultAtLeastOnce
    (TheAtLeastOnceWittness firstResult)
    (GreedilyRepeatOutput optionalRemainingResults)
    (TheErrorThatWouldHaveHappenedWhenContinuing errorIfWouldHaveContinued)


newtype Delimiter a = Delimiter a
  deriving Show

newtype OutputBetweenDelimiter a = OutputBetweenDelimiter a
  deriving Show

data DelimiteredOutput comma item = DelimiteredOutput (Delimiter comma) (OutputBetweenDelimiter item)
  deriving Show

data ErrorCaseAndError_WhenWouldContinue commaError itemError
  = DelemiterError_WhenWouldContinue commaError
  | ItemError_WhenWouldContinue itemError
  deriving Show

data DelimitedGreedilyRepeatParserAtLeastOnceResult
  comma
  item
  commaError
  itemError
  = DelimitedGreedilyRepeatParserAtLeastOnceResult
      (TheAtLeastOnceWittness item)
      (GreedilyRepeatOutput [DelimiteredOutput comma item])
      (ErrorCaseAndError_WhenWouldContinue commaError itemError)
  deriving Show

newtype Error_DelimitedGreedilyRepeatParserAtLeastOnce a
  = ParserCouldNotBeInvokedAtLeastOnce a
  deriving Show

data Error_InEitherCommaOrItemParser commaError itemError
  = ErrorInCommaParser commaError
  | ErrorInItemParser itemError

-- | parse for example thinks like "1, 23, 34, 34, 0", "23", but not ""
delimitedGreedilyRepeatParserAtLeastOnce
  :: Parser token itemError item
  -> Parser token commaError comma
  -> Parser
      token
      (Error_DelimitedGreedilyRepeatParserAtLeastOnce itemError)
      (DelimitedGreedilyRepeatParserAtLeastOnceResult
        comma
        item
        commaError
        itemError
      )
delimitedGreedilyRepeatParserAtLeastOnce itemParser delimiter = do
  firstResult
    <- fmapOnParserError ParserCouldNotBeInvokedAtLeastOnce itemParser
  GreedilyRepeatParserResult
    (GreedilyRepeatOutput optionalRemainingResults)
    (TheErrorThatWouldHaveHappenedWhenContinuing errorIfWouldHaveContinued)
    <- repeatedDelimiteredParser

  let refinedError = translateError errorIfWouldHaveContinued

  return $ DelimitedGreedilyRepeatParserAtLeastOnceResult
    (TheAtLeastOnceWittness firstResult)
    (GreedilyRepeatOutput $ fmap enrichOutput optionalRemainingResults)
    (refinedError)
  where
    repeatedDelimiteredParser
      = absurdError
      $ greedilyRepeatParser
      $ doTwoParser
      (fmapOnParserError ErrorInCommaParser delimiter)
      (fmapOnParserError ErrorInItemParser itemParser)

    enrichOutput (comma, item) = DelimiteredOutput (Delimiter comma) (OutputBetweenDelimiter item)

    -- TODO could be removed. should it?
    translateError (ErrorInCommaParser x)
      = DelemiterError_WhenWouldContinue x
    translateError (ErrorInItemParser x)
      = ItemError_WhenWouldContinue x

