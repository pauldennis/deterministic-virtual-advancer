module CleanParser.LowLevelBuildingBlocks where

{-
  here we still use internal details like Left, Right, Input and Remnant
-}

import Data.Void
import Data.Either

import CleanParser.Core
import CleanParser.ActualUsingTheParser

succeedWith :: output -> Parser token falsity output
succeedWith message = return message


failWith :: falsity -> Parser token falsity output
failWith errorMassage = Parser $ \_ -> Left errorMassage


-- | this parser never fails
discardUntilEndOfFile :: output -> Parser token Void output
discardUntilEndOfFile output = Parser $ const $ Right (Remnant [], output)


data Error_UnexpectedContinuationOfFile token
  = Error_UnexpectedContinuationOfFile [token]
  deriving (Eq, Show)

checkForEndOfFile
  :: output
  -> Parser token (Error_UnexpectedContinuationOfFile token) output
checkForEndOfFile outputIfSuccessful = Parser $ parser
  where
    parser (Input endOfFile@[])
      = Right (Remnant endOfFile, outputIfSuccessful)

    parser (Input leftoverTokens)
      = Left $ Error_UnexpectedContinuationOfFile leftoverTokens

data Error_OptionalParse wouldHappenError output
  = ParserWorkedWith output
  | ParserWouldNotWork wouldHappenError

optionalParse
  :: Parser token errorCase output
  -> Parser token Void (Error_OptionalParse errorCase output)
optionalParse (Parser optionalParser)
  = Parser
  $ \(Input input) -> Right
  $ case optionalParser (Input input) of
         Right (remnant, result) -> (remnant, ParserWorkedWith result)
         Left noParseError -> (Remnant input, ParserWouldNotWork noParseError)

test_optionalParse :: (String, Bool)
test_optionalParse = (,) "optinal parse" $ and [ True
  , isRight $ runParser (optionalParse eatOneToken) ""
  , isRight $ runParser (optionalParse eatOneToken) "1"
  , isRight $ runParser (optionalParse eatOneToken) "22"
  , isRight $ runParser (optionalParse eatOneToken) "333"
  ]


data Error_EatOneToken
   = ExpectedATokenButGotEndOfFile
      deriving (Eq, Show)

-- | output and token are the same type
eatOneToken :: Parser token Error_EatOneToken token
eatOneToken = Parser $ parser
  where
    parser (Input (token:remainderTokens)) = Right (Remnant remainderTokens, token)
    parser (Input []) = Left $ ExpectedATokenButGotEndOfFile

newtype GreedilyRepeatOutput output
  = GreedilyRepeatOutput output
  deriving Show

newtype TheErrorThatWouldHaveHappenedWhenContinuing wouldBeError
  = TheErrorThatWouldHaveHappenedWhenContinuing wouldBeError
  deriving Show

data GreedilyRepeatParserResult output wouldBeError
  = GreedilyRepeatParserResult
      (GreedilyRepeatOutput [output])
      (TheErrorThatWouldHaveHappenedWhenContinuing wouldBeError)
  deriving Show

outputsOfGreedilyRepeatParserResult
  :: GreedilyRepeatParserResult output wouldBeError
  -> [output]
outputsOfGreedilyRepeatParserResult (GreedilyRepeatParserResult (GreedilyRepeatOutput outputs) _) = outputs

whyHadToBeGreedilyRepeatParserStopped
  :: GreedilyRepeatParserResult output wouldBeError
  -> wouldBeError
whyHadToBeGreedilyRepeatParserStopped (GreedilyRepeatParserResult _ (TheErrorThatWouldHaveHappenedWhenContinuing x)) = x

-- | the provided parser is supposed to never successfull eat the empty word. otherwise an infinite loop appears
-- | the parser parses the token stream lazily. That means infinit streams of tokens can be parsed productivly
greedilyRepeatParser
  :: Parser token falsity output
  -> Parser token Void (GreedilyRepeatParserResult output falsity)
greedilyRepeatParser (Parser parser) = Parser $ result
  where
    result (Input input) = help $ parser (Input input)
      where
        help (Left errorCase)
          = Right
              ( Remnant input
              , GreedilyRepeatParserResult
                  (GreedilyRepeatOutput [])
                  (TheErrorThatWouldHaveHappenedWhenContinuing errorCase)
              )
        help (Right (Remnant intermediateRemnant, output))
          = Right $ combine
              output
              (recursion intermediateRemnant)

  --recursion :: [t] -> ([t], [o])
    recursion tokens
      = fromRight undefined
      $ manuallyRun tokens (greedilyRepeatParser (Parser parser))

    combine output lazyResult
      = (lastRenmnent, GreedilyRepeatParserResult (GreedilyRepeatOutput $ output : actualPayload) errorIfWouldContinue)
      where
        -- patternmatching of lazyPatternMatching happens later in function. otherwise performance bug
        lastRenmnent = fst lazyResult
        recursiveOutput = snd lazyResult

        actualPayload = (\(GreedilyRepeatParserResult (GreedilyRepeatOutput x) _)->x) recursiveOutput
        errorIfWouldContinue = (\(GreedilyRepeatParserResult _ y)->y) recursiveOutput

    manuallyRun string (Parser function) = function $ Input string

test_greedilyRepeatParser_works_lazy :: (String, Bool)
test_greedilyRepeatParser_works_lazy
  = (,) "greedilyRepeatParser eats lazily"
  $ ("#" ==)
  $ take 1
  $ (\(GreedilyRepeatOutput x)->x)
  $ (\(GreedilyRepeatParserResult x _)->x)
  $ getOutput
  $ (\(Right x) -> x)
  $ runParser (greedilyRepeatParser eatOneToken)
  $ repeat '#' --NOTE: the list is infinit

-- | do not use
strictlyRepeatTest
  :: Parser token errorCase output
  -> Parser token Void [Either errorCase output]
strictlyRepeatTest parser = do
  maybeResult <- optionalParse parser
  case maybeResult of
       ParserWorkedWith result -> do
         recursiveResults <- strictlyRepeatTest parser
         return $ (Right result) : recursiveResults
       ParserWouldNotWork errorCase -> return [Left errorCase]


data PositiveLookaheadError parserError
  = PositiveLookaheadError parserError
  deriving (Eq, Show)

-- | only tell if parsing would work and what the output would be, but not actually eating anything
positiveLookahead
  :: Parser t error output
  -> Parser t (PositiveLookaheadError error) output
positiveLookahead (Parser parser)
  = fmapOnParserError PositiveLookaheadError
  $ Parser result
  where
    result (Input input) = do--Either
        successfullOutput <- parser (Input input)
        return (Remnant input, getOutput successfullOutput)


data NegativeLookaheadError parserContinuation
  = NegativeLookaheadError parserContinuation
  deriving (Eq, Show)

negativeLookahead
  :: Parser t o output
  -> Parser t (NegativeLookaheadError (Remnant t, output)) o
negativeLookahead (Parser parser)
  = fmapOnParserError NegativeLookaheadError
  $ Parser result
  where
    result (Input input) = case parser (Input input) of
                        (Left errorCase) -> return (Remnant input, errorCase)
                        (Right success) -> Left success


-- | inefficient. do not use. whole stream might get forced. Detect this manually with each parser
detectIfParserAteAnything
  :: Eq token
  => Parser token falsity output
  -> Parser token falsity (Bool, output)
detectIfParserAteAnything (Parser parser) = Parser resultParser
  where
    resultParser (Input input)
      = case parser (Input input) of
             Right (Remnant remnant, output) -> Right result
               where
                 result = (Remnant remnant, (inefficientCompare, output))
                 inefficientCompare = remnant /= input
             Left errorMassage -> Left errorMassage

