module CleanParser.Error where

import Safe.Partial
import GHC.Stack

import Data.Void
import CleanParser.Core

rethrow
  :: (falsity1 -> falsity2)
  -> Parser token falsity1 output
  -> Parser token falsity2 output
rethrow = fmapOnParserError

wrapError
  :: (falsity1 -> falsity2)
  -> Parser token falsity1 output
  -> Parser token falsity2 output
wrapError = fmapOnParserError

absurdError
  :: Parser token Void output
  -> Parser token falsity output
absurdError = fmapOnParserError absurd

deleteError
  :: Parser token _falsity output
  -> Parser token () output
deleteError = fmapOnParserError $ const ()

assumeNoError
  :: Partial
  => String
  -> Parser token falsity1 output
  -> Parser token falsity2 output
assumeNoError = withFrozenCallStack $ fmapOnParserError . error
