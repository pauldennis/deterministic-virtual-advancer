module CleanParser.ActualUsingTheParser where

import Safe.Partial
import GHC.Stack

import Data.Either.Combinators

import CleanParser.Core
import CleanParser.Token

-- | Take an parser that wants to eat an Stream of tokens. It might complain with an error of typ falsity. After digesting it outputs output. The token stream that gets feed is the second argument namely a list of tokens.
runParser
  :: Parser token falsity output
  -> [token]
  -> Either falsity (Remnant token, output)
runParser parser string = unwrapParser parser $ Input string


parse
  :: Parser token falsity output
  -> [token]
  -> Either falsity output
parse parser string = mapRight getOutput $ runParser parser string


feedToParser
  :: [token]
  -> Parser token falsity output
  -> Either falsity (Remnant token, output)
feedToParser = flip runParser


runParserWithNumberedTokens
  :: Parser NumberedToken falsity output
  -> String
  -> Either falsity (Remnant NumberedToken, output)
runParserWithNumberedTokens parser string
  = runParser parser
  $ zipWith Token [1..]
  $ string


-- TODO maybe remove Complete and have Partial on other functions
parserWithNumberedTokensComplete
  :: Parser NumberedToken falsity output
  -> String
  -> Either falsity output
parserWithNumberedTokensComplete parser string
  = mapRight getOutputComplete
  $ runParserWithNumberedTokens parser string


parserWithNumberedTokensCompleteAssumeSuccess
  :: Partial
  => Parser NumberedToken _falsity output
  -> String
  -> output
parserWithNumberedTokensCompleteAssumeSuccess parser string
  = withFrozenCallStack
  $ fromRight (error "parser did unexpectedly not work")
  $ mapRight getOutputComplete
  $ runParserWithNumberedTokens parser string
