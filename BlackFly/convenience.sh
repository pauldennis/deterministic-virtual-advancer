set -e

pushd source/ExampleFuchsProgramms
  rm ./AsHaskellConstant/* || true
  runhaskell wrap_in_haskell.hs
popd
