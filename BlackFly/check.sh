set -e

nix-shell --run ./convenience.sh

nix-build --pure --show-trace
