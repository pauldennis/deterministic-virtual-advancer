{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, stdenv ? pkgs.stdenv

, ghc ? import ./glasgowHaskellCompiler.nix {}

}:


stdenv.mkDerivation {
  name = "BlackFly";
  builder = ./builder.sh;

  source = ./source;

  buildInputs = [
    ghc
  ];

  # excruciating unicode and whatnot
  LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive";
  LANG="en_GB.UTF8";

}
